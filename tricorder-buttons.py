#!/usr/bin/env python

import RPi.GPIO as GPIO
import uinput
import time
import signal
import datetime as dt
import sys
import os
import psutil
import subprocess

GPIO.setmode(GPIO.BCM)
GPIO.setup(16, GPIO.IN)     # Left Button
GPIO.setup(26, GPIO.IN)     # Center Button
GPIO.setup(20, GPIO.IN)     # Right Button
GPIO.setup(19, GPIO.IN)     # Head Switch

programLogFile = "/home/pi/TOSTricorder/tricorder-buttons.log"
scriptName = __file__
verbose = 1
uiScript = "tricordergui.py"
uiScriptPath = "/usr/bin/python3 /home/pi/TOSTricorder/"
clickSoundFile = "/home/pi/TOSTricorder/click.wav"

# events = (uinput.KEY_A, uinput.KEY_B, uinput.KEY_C)
events = (uinput.KEY_TAB, uinput.KEY_SPACE, uinput.KEY_LEFTSHIFT, uinput.KEY_LEFTALT, uinput.KEY_T)
device = uinput.Device(events)
headSwLastState = False


def isUiRunning():
    global uiScript
    for pid in psutil.pids():
        try:
            p = psutil.Process(pid)
            if p.name() == "python3" and len(p.cmdline()) > 1 and uiScript in p.cmdline()[1]:
                return True
                break
        except Exception as e:
            write_log("ERROR", "Process ID Not Found: ({0})".format(e), True)

    return False


def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


# KEY_ESC
# KEY_SPACE
# device.emit_combo([uinput.KEY_LEFTSHIFT, uinput.KEY_TAB])

def main(button):
    global uiScript, uiScriptPath

    button_pressed = False

    if not isUiRunning():
        write_log("INFORMATION", "UI is not running")
        if button == 1:
            # Launch UI
            write_log("INFORMATION", "Starting UI")
            device.emit_combo([uinput.KEY_LEFTALT, uinput.KEY_T])
            button_pressed = True
        elif button == 2:
            # Wake UI / Placeholder
            write_log("INFORMATION", "Placeholder")
            device.emit_click(uinput.KEY_SPACE)
            button_pressed = True
        elif button == 3:
            # Shutdown
            # device.emit_combo([uinput.KEY_ALT, uinput.KEY_S])
            write_log("INFORMATION", "Shutdown")
            os.system("sudo shutdown -h now")
            device.destroy()
            sys.exit()

        if button_pressed:
            time.sleep(.5)

        return

    if button == 1:
        # print("Button 1")
        device.emit_combo([uinput.KEY_LEFTSHIFT, uinput.KEY_TAB])
        write_log("INFORMATION", "Button 1 Clicked")
        button_pressed = True
    elif button == 2:
        # print("Button 2")
        device.emit_click(uinput.KEY_SPACE)
        write_log("INFORMATION", "Button 2 Clicked")
        button_pressed = True
    elif button == 3:
        # print("Button 3")
        device.emit_click(uinput.KEY_TAB)
        write_log("INFORMATION", "Button 3 Clicked")
        button_pressed = True

    if button_pressed:
        time.sleep(.5)


if __name__ == '__main__':
    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    write_log("INFORMATION", "Started")

    while True:
        if killer.kill_now:
            break

        if GPIO.input(16):
            write_log("INFORMATION", "Button 1 Pressed", True)
            main(1)
        elif GPIO.input(26):
            write_log("INFORMATION", "Button 2 Pressed", True)
            main(2)
        elif GPIO.input(20):
            write_log("INFORMATION", "Button 3 Pressed", True)
            main(3)

        if GPIO.input(19):
            if headSwLastState == False:
                write_log("INFORMATION", "Opened Head Unit", True)
                # try:
                #     subprocess.call('aplay -D sysdefault ' + clickSoundFile, shell=True)
                # except Exception as e:
                #     write_log("ERROR", "Failed to play click sound: ({0})".format(e), True)
                headSwLastState = True
        else:
            if headSwLastState == True:
                write_log("INFORMATION", "Closed Head Unit", True)
                # try:
                #     subprocess.call('aplay -D sysdefault ' + clickSoundFile, shell=True)
                # except Exception as e:
                #     write_log("ERROR", "Failed to play click sound: ({0})".format(e), True)
                headSwLastState = False

        time.sleep(.02)  # Poll every 20ms (otherwise CPU load gets too high)

device.destroy()
write_log("INFORMATION", "Ended")
