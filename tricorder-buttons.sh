#!/bin/sh

# systemd  service

start() {
	# Archive existing log file
  if [ -f /home/pi/TOSTricorder/tricorder-buttons.log ]; then
  	if [ -f /home/pi/TOSTricorder/tricorder-buttons.log.bak ]; then
  		rm /home/pi/TOSTricorder/tricorder-buttons.log.bak
  	fi
  	cp /home/pi/TOSTricorder/tricorder-buttons.log /home/pi/TOSTricorder/tricorder-buttons.log.bak
  	rm /home/pi/TOSTricorder/tricorder-buttons.log
  fi
  echo "Tricorder-Buttons Service Starting"
	/usr/bin/python /home/pi/TOSTricorder/tricorder-buttons.py
}

stop() {
  echo "Tricorder-Buttons Service Stopping"
  kill $(pgrep tricorder-buttons.sh)
  echo "Tricorder-Buttons Service Stopped"
}

case "$1" in
   start)
      start
   ;;
   stop)
      stop
   ;;
   restart)
      stop
      start
   ;;
   status)
      if [ pgrep tricorder-buttons.sh > 0 ]; then
      	echo "Tricorder-Buttons Service is Running"
      else
      	echo "Tricorder-Buttons Service is Stopped"
      fi
   ;;
   *)
      echo "Usage: $0 {start|stop|restart|status}"
esac