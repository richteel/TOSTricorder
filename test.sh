#!/bin/bash

CWDIR=$(pwd)

# Check that the user ran as sudo
if [ "$EUID" -ne 0 ]
  then echo "Please run using sudo ./$0 (You may use 'sudo !!' now)"
  exit
fi

# Default Parameters
P_UPDATE="n"
P_COUNTRY="us"
P_LANG="en"
P_TZ="America/New_York"
P_HOSTNAME="tricorder001"

usage() { echo "Usage: $0 [-u <y/n|$P_UPDATE>] [-c <2 char country code|$P_COUNTRY>] [-l <2 char language code|$P_LANG>] [-t <string timezone|$P_TZ>] [-h <string hostname|$P_HOSTNAME>]" 1>&2; exit 1; }


while getopts ":u:c:l:t:h:" o; do
    case "${o}" in
        u)
            P_UPDATE=${OPTARG}
            ((P_UPDATE == "y" || P_UPDATE == "n")) || usage
            ;;
        c)
            P_COUNTRY=${OPTARG}
            ;;
        l)
            P_LANG=${OPTARG}
            ;;
        t)
            P_TZ=${OPTARG}
            ;;
        h)
            P_HOSTNAME=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

P_UPDATE="${P_UPDATE,,}"
P_COUNTRY="${P_COUNTRY,,}"
P_LANG="${P_LANG,,}"

echo "Update = $P_UPDATE"
echo "Country = $P_COUNTRY"
echo "Language = $P_LANG"
echo "Timezone = $P_TZ"
echo "Hostname = $P_HOSTNAME"
echo

# **************************************************
CHECK="Boot Option To Autologin: "
L_LINK="/etc/systemd/system/getty.target.wants/getty@tty1.service"
L_FILE="/etc/systemd/system/autologin@.service"
if [ ! -L "${L_LINK}" ] || [ "$(readlink -- "${L_LINK}")" != "${L_FILE}" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi
unset L_LINK
unset L_FILE

CHECK="Boot Option To Console: "
L_LINK="/etc/systemd/system/default.target"
L_FILE="/lib/systemd/system/multi-user.target"
if [ ! -L "${L_LINK}" ] || [ "$(readlink -- "${L_LINK}")" != "${L_FILE}" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi
unset L_LINK
unset L_FILE

CHECK="Keyboard Layout: "
MATCH=$(cat /etc/default/keyboard | egrep "^XKBLAYOUT=\"${P_COUNTRY}\"")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="System Locales: "
SEARCH="^${P_LANG}_${P_COUNTRY^^}.UTF-8 UTF-8"
MATCH=$(cat /etc/locale.gen | egrep "${SEARCH}")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH
unset SEARCH

CHECK="Default Locale: "
SEARCH="^LANG=${P_LANG}_${P_COUNTRY^^}.UTF-8"
MATCH=$(cat /etc/default/locale | egrep "${SEARCH}")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH
unset SEARCH

CHECK="System Timezone: "
MATCH1=$(cat /etc/timezone | egrep "^${P_TZ}")
MATCH2=$(diff -a -q /etc/localtime "/usr/share/zoneinfo/${P_TZ}")
if [ -z "$MATCH1" ] || [ ! -z "$MATCH2" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH1})"
fi
unset MATCH

CHECK="WiFi Country: "
MATCH=$(cat /etc/wpa_supplicant/wpa_supplicant.conf | egrep "country=${P_COUNTRY^^}")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="Hostname: "
MATCH=$(cat /etc/hostname | egrep "^${P_HOSTNAME}$")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="SPI enabled: "
MATCH=$(cat /boot/config.txt | egrep "^dtparam=spi=on")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="I2C enabled: "
MATCH=$(cat /boot/config.txt | egrep "^dtparam=i2c_arm=on")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="Remote Desktop Server: "
if [ ! -f /usr/sbin/xrdp ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi

CHECK="NEOPIXEL support enabled: "
if [ ! -f /usr/bin/scons ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi

CHECK="Audio driver installed: "
if [ ! -f "/etc/asound.conf" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi

CHECK="Audio enabled: "
MATCH=$(cat /boot/config.txt | egrep "^dtparam=audio=on")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="Audio default set to CM108: "
echo "Test not implemented"

CHECK="Camera enabled: "
MATCH1=$(cat /boot/config.txt | egrep "start_x=1")
MATCH2=$(cat /boot/config.txt | egrep "gpu_mem=128")
if [ -z "$MATCH1" ] || [ -z "$MATCH2" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH1} & ${MATCH2})"
fi
unset MATCH1
unset MATCH2

CHECK="Serial enabled: "
MATCH=$(cat /boot/config.txt | egrep "enable_uart=1")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="Display enabled: "
MATCH=$(cat /boot/config.txt | egrep "^dtoverlay=adafruit22,rotate=270,speed=16000000,fps=15")
if [ -z "$MATCH" ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS (${MATCH})"
fi
unset MATCH

CHECK="fbcp installed: "
if [ ! -f /usr/local/bin/fbcp ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi

CHECK="Display Enabled on Boot: "
if [ ! -f /etc/modules-load.d/fbtft.conf ] || [ ! -f /etc/modprobe.d/fbtft.conf ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi

CHECK="USB Audio set to default audio device: "
if [ ! -f /etc/modprobe.d/alsa-base.conf ]; then
	echo "${CHECK}FAIL"
  FAILEDCHECK="Yes"
else
	echo "${CHECK}PASS"
fi

echo
echo "-------------------------------------------------------------"
if [ "${FAILEDCHECK}" == "Yes" ]; then
	echo "At least one test failed.\nRun setup to attempt to fix failed tests."
else
	echo "All tests passed"
fi
echo "-------------------------------------------------------------"
