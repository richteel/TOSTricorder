#!/usr/bin/env python

import signal
import sys
import time
import datetime as dt
import os
import picamera
import psutil
import sqlite3
import math

# Feeds
#    Air-Pressure       0x77    BMP180
#    Air-Temperature1   0x77
#    Air-Temperature2   0x44    SHT31-D
#    Air-Humidity       0x44

programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
photosPath = "/home/pi/TOSTricorder/photos/"
databaseFile = "/home/pi/TOSTricorder/tricorder.db"
sqlGetDataGps = "SELECT LocationDataId, DateTime_UTC, DateTime_Local, Latitude, Longitude, Elevation, Created FROM LocationData WHERE Created >= datetime('now', '-15 Minute') ORDER BY Created DESC LIMIT 1;"
scriptName = __file__
wait_in_seconds_good_read = 60 * 5
wait_in_seconds_bad_read = 60 * 1
msg_format_camera = "Camera-Image {}\n"
required_free_disk_space_per = 0.1

verbose = 1


def get_gps_info():
    global sqlGetDataGps, databaseFile

    retVal = None
    conn = sqlite3.connect(databaseFile)
    c = conn.cursor()
    try:
        c.execute(sqlGetDataGps)
        firstrow = c.fetchone()
        if firstrow is not None and len(firstrow) == 7:
            # retVal = {'Latitude': firstrow['Latitude'], 'Longitude': firstrow['Longitude']}
            retVal = {'Latitude': firstrow[3], 'Longitude': firstrow[4]}

    except sqlite3.Error as e:
        retVal = None
        write_log("ERROR", "Error saving data to the database (" + e.args[0] + ")", True)

    conn.close()
    return retVal


def check_disk_space():
    global required_free_disk_space_per, photosPath

    disk_total = psutil.disk_usage(photosPath).total  #
    disk_free = psutil.disk_usage(photosPath).free  # 9,582,764,032
    disk_free_percent = float(disk_free) / float(disk_total)  #

    if disk_free_percent >= required_free_disk_space_per:
        return True

    write_log("WARNING", "Disk space is at {0:0.00f}%. Photos will not be saved.".format(disk_free_percent * 100.0))
    return False


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def write2fifo(msg):
    global pipePath
    if not os.path.exists(pipePath):
        return

    with open(pipePath, 'w') as f:
        f.write(msg)
        f.close()


def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


if __name__ == '__main__':
    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    write_log("INFORMATION", "Started")

    while True:
        if killer.kill_now:
            break

        wait_time = wait_in_seconds_bad_read

        if not os.path.exists(photosPath):
            os.mkdir(photosPath, 0o666)

        lat = 0
        lon = 0

        gpsData = get_gps_info()

        with picamera.PiCamera() as camera:
            if not gpsData is None:
                # Set GPS latitude
                lat = gpsData['Latitude']
                camera.exif_tags['GPS.GPSLatitudeRef'] = 'N' if lat > 0 else 'S'
                alat = math.fabs(lat)
                dlat = int(alat)
                mlat = int(60 * (alat - dlat))
                slat = int(6000 * (60 * (alat - dlat) - mlat))
                camera.exif_tags['GPS.GPSLatitude'] = '%d/1,%d/1,%d/100' % (dlat, mlat, slat)

                # Set GPS longitude
                lon = gpsData['Longitude']
                camera.exif_tags['GPS.GPSLongitudeRef'] = 'E' if lon > 0 else 'W'
                alon = math.fabs(lon)
                dlon = int(alon)
                mlon = int(60 * (alon - dlon))
                slon = int(6000 * (60 * (alon - dlon) - mlon))
                camera.exif_tags['GPS.GPSLongitude'] = '%d/1,%d/1,%d/100' % (dlon, mlon, slon)

            if os.path.exists(photosPath):
                if (check_disk_space()):
                    camera.rotation = 90
                    camera.exif_tags['IFD0.Copyright'] = 'Copyright (c) 2016 TeelSys LLC'
                    fileName = "img_%s.jpg" % dt.datetime.now().isoformat()
                    photoFullFileName = photosPath + fileName
                    thumbFullFileName = photosPath + 'thumb_' + fileName
                    camera.capture(photoFullFileName)
                    # camera.capture(thumbFullFileName, resize = (320, 240))
                    os.chmod(photoFullFileName, 0o755)
                    # os.chmod(thumbFullFileName, 0o644)
                    write2fifo(msg_format_camera.format(photoFullFileName))

        """
        if read_bmp180():
            wait_time = wait_in_seconds_good_read
            write_log("DEBUG", "Wrote BMP180 Data to FIFO", True)
        else:
            write_log("WARNING", "Failed to obtain data from BMP180")
        """

        time.sleep(wait_time)  # set to wait time in seconds

write_log("INFORMATION", "Ended")
