#!/usr/bin/env python

import signal
import sys
import os
import time
from neopixel import *
import datetime as dt


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/neopixels.fifo"
scriptName = __file__

# LED strip configuration:
LED_COUNT = 3  # Number of LED pixels.
LED_PIN = 13  # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 4  # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 128  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)

# Global Settings
refreshMS = 40
# 125 = 8 times per second
# 40 = 25 times per second
updateMS = 250
brightness = 128
verbose = 1
pixel0R = 0
pixel0G = 0
pixel0B = 0
pixel1R = 0
pixel1G = 0
pixel1B = 0
pixel2R = 0
pixel2G = 0
pixel2B = 0


# **********************************************************
# ***                     FUNCTIONS                      ***
# **********************************************************
def archiveLog(filename):
    if os.path.isfile(filename):
        os.rename(filename, filename + ".bak")


def isValidColor(r, g, b):
    retVal = True

    if not isValidInt(r):
        retVal = False
    elif not isValidInt(g):
        retVal = False
    elif not isValidInt(b):
        retVal = False

    return retVal


def isValidInt(strVal, minVal=0, maxVal=255):
    try:
        q = int(strVal)
        if minVal <= q <= maxVal:
            return True
        else:
            return False
    except ValueError:
        return False

    return retVal


def processLine(line):
    global programLogFile, pixel0R, pixel0G, pixel0B, pixel1R, pixel1G, pixel1B, pixel2R, pixel2G, pixel2B, brightness, verbose, refreshMS, updateMS

    data = line.lower()
    words = data.split(" ")
    err = False

    if words[0] == "p1":
        if not len(words) == 4:
            err = True
        elif not isValidColor(words[1], words[2], words[3]):
            err = True
        else:
            pixel0R = int(words[1])
            pixel0G = int(words[2])
            pixel0B = int(words[3])
    elif words[0] == "p2":
        if not len(words) == 4:
            err = True
        elif not isValidColor(words[1], words[2], words[3]):
            err = True
        else:
            pixel1R = int(words[1])
            pixel1G = int(words[2])
            pixel1B = int(words[3])
    elif words[0] == "p3":
        if not len(words) == 4:
            err = True
        elif not isValidColor(words[1], words[2], words[3]):
            err = True
        else:
            pixel2R = int(words[1])
            pixel2G = int(words[2])
            pixel2B = int(words[3])
    elif words[0] == "brightness":
        if not len(words) == 2:
            err = True
        elif not isValidInt(words[1]):
            err = True
        else:
            brightness = int(words[1])
    elif words[0] == "verbose":
        if not len(words) == 2:
            err = True
        elif not isValidInt(words[1], 0, 1):
            err = True
        else:
            verbose = int(words[1])
    elif words[0] == "refreshms":
        if not len(words) == 2:
            err = True
        elif not isValidInt(words[1], 30, 1000):
            err = True
        else:
            refreshMS = int(words[1])
    elif words[0] == "updatems":
        if not len(words) == 2:
            err = True
        elif not isValidInt(words[1], 30, 1000):
            err = True
        else:
            updateMS = int(words[1])
    else:
        err = True

    if err:
        writeLog("WARNING", "Command not recognized, incorrect number of arguments, or arguments out of range: " + line,
                 True)


def readPipe(pipe):
    data = os.read(pipe, 1024)
    if not data:
        return

    data = data.strip()

    if len(data) == 0:
        return

    lines = data.split(os.linesep)

    for line in lines:
        writeLog("DEBUG", "Processing Line: " + line, True)
        processLine(line)


def timeStamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def writeLog(messageType, message, echoMessage = False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and messageType.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        writeLogFileHeader(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timeStamp(), s2=scriptName, s3=messageType, s4=message) + "\n")
        f.close()

    if echoMessage:
        print(fmt.format(s1=timeStamp(), s2=scriptName, s3=messageType, s4=message))


def writeLogFileHeader(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


# **********************************************************
# ***             Main program logic follows             ***
# **********************************************************
if __name__ == '__main__':
    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    killer = GracefulKiller()
    pipe = -1

    # Archive the log file if it exists
    # archiveLog(programLogFile)

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, 1)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    if os.path.exists(pipePath):
        os.unlink(pipePath)

    if not os.path.exists(pipePath):
        os.mkfifo(pipePath)
        os.chmod(pipePath, 0o666)

    time.sleep(1)
    if os.path.exists(pipePath):
        pipe = os.open(pipePath, os.O_RDONLY | os.O_NONBLOCK)

    nextUpdateTime = dt.datetime.now()
    nextUpdateTime = nextUpdateTime + dt.timedelta(milliseconds=updateMS)
    writeLog("INFORMATION", "Started")

    while True:
        if killer.kill_now:
            break
        if dt.datetime.now() > nextUpdateTime:
            if pipe <= 0 and os.path.exists(pipePath):
                pipe = os.open(pipePath, os.O_RDONLY | os.O_NONBLOCK)

            if pipe > 0:
                readPipe(pipe)
                # writeLog("DEBUG", "Read Pipe")

            nextUpdateTime = dt.datetime.now()
            nextUpdateTime = nextUpdateTime + dt.timedelta(milliseconds=updateMS)
        # writeLog("DEBUG", "Read Configuration")

        strip.setBrightness(brightness)
        strip.setPixelColor(0, Color(pixel0R, pixel0G, pixel0B))
        strip.setPixelColor(1, Color(pixel1R, pixel1G, pixel1B))
        strip.setPixelColor(2, Color(pixel2R, pixel2G, pixel2B))
        strip.show()
        # writeLog("DEBUG", "Updated Pixels")
        time.sleep(refreshMS / 1000.0) # set to wait time in seconds

if pipe > 0:
    os.close(pipe)

if os.path.exists(pipePath):
    os.unlink(pipePath)

writeLog("INFORMATION", "Ended")
