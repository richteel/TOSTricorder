#!/usr/bin/env python

import signal
import sys
import time
import datetime as dt
import os
import Adafruit_BMP.BMP085 as BMP085
import sht31

# Feeds
#    Air-Pressure       0x77    BMP180
#    Air-Temperature1   0x77
#    Air-Temperature2   0x44    SHT31-D
#    Air-Humidity       0x44


programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
scriptName = __file__
wait_in_seconds_good_read = 60 * 5
wait_in_seconds_bad_read = 60 * 1
msg_format_pressure = "Air-Pressure {}"
msg_format_temperature1 = "Air-Temperature1 {}"
msg_format_temperature2 = "Air-Temperature2 {}"
msg_format_humidity = "Air-Humidity {}"

verbose = 1

sensor_bmp180 = BMP085.BMP085()


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True

def read_bmp180():
    try:
        write2fifo(msg_format_pressure.format(sensor_bmp180.read_pressure()))
        write2fifo(msg_format_temperature1.format(sensor_bmp180.read_temperature()))
        return True
    except Exception as e:
        write_log("DEBUG", "Error while attempting to read from BMP180: ({0})".format(e), True)
        return False


def read_sht31():
    try:
        with sht31.SHT31(1) as sht31d:
            temperature, humidity = sht31d.get_temp_and_humidity()
            write2fifo(msg_format_temperature2.format(temperature))
            write2fifo(msg_format_humidity.format(humidity))
            return True
    except Exception as e:
        write_log("DEBUG", "Error while attempting to read from SHT31-D: ({0})".format(e), True)
        return False


def write2fifo(msg):
    global pipePath
    if not os.path.exists(pipePath):
        return

    with open(pipePath, 'w') as f:
        f.write(msg + os.linesep)
        f.close()


def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message = False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


if __name__ == '__main__':
    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")


    write_log("INFORMATION", "Started")
    while True:
        if killer.kill_now:
            break

        wait_time = wait_in_seconds_bad_read
        if read_bmp180():
            wait_time = wait_in_seconds_good_read
            write_log("DEBUG", "Wrote BMP180 Data to FIFO", True)
        else:
            write_log("WARNING", "Failed to obtain data from BMP180")

        
        if read_sht31():
            wait_time = wait_in_seconds_good_read
            write_log("DEBUG", "Wrote SHT31-D Data to FIFO", True)
        else:
            write_log("WARNING", "Failed to obtain data from SHT31-D")

        time.sleep(wait_time)  # set to wait time in seconds

write_log("INFORMATION", "Ended")
