BEGIN TRANSACTION;


	-- Describe LOCATIONDATA
	CREATE TABLE IF NOT EXISTS [LocationData] (
		"LocationDataId"		integer NOT NULL,
		"DateTime_UTC"		datetime NOT NULL,
		"DateTime_Local"		datetime NOT NULL,
		"Latitude"		float,
		"Longitude"		float,
		"Elevation"		float,
		"Created"		datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
	    PRIMARY KEY ([LocationDataId])
	);

	-- Describe IOTSITES
	CREATE TABLE IF NOT EXISTS [IotSites] (
		"IotSiteId"		integer NOT NULL,
		"DisplayName"		varchar(128) NOT NULL COLLATE NOCASE,
	    UNIQUE([DisplayName]),
	    PRIMARY KEY ([IotSiteId])
	);

	-- Describe FEEDDATATYPES
	CREATE TABLE IF NOT EXISTS [FeedDataTypes] (
		"FeedDataTypeId"		integer NOT NULL,
		"DisplayName"		varchar(128) NOT NULL COLLATE NOCASE,
			UNIQUE([DisplayName]),
	    PRIMARY KEY ([FeedDataTypeId])
	);

	-- Describe SENSORS
	CREATE TABLE IF NOT EXISTS [Sensors] (
		"SensorId"		integer NOT NULL,
		"DisplayName"		nvarchar(128) NOT NULL COLLATE NOCASE,
		"IsActive"		bit NOT NULL DEFAULT 1,
			UNIQUE([DisplayName]),
	    PRIMARY KEY ([SensorId])
	);

	-- Describe FEEDS
	CREATE TABLE IF NOT EXISTS [Feeds] (
		"FeedId"		integer NOT NULL,
		"SensorId"		numeric NOT NULL,
		"DisplayName"		varchar(128) NOT NULL COLLATE NOCASE,
		"FeedDataTypeId"		numeric NOT NULL,
		"IsActive"		bit NOT NULL DEFAULT 1,
			UNIQUE([DisplayName]),
	    PRIMARY KEY ([FeedId])
	,
	    FOREIGN KEY ([SensorId])
	        REFERENCES [Sensors]([SensorId]),
	    FOREIGN KEY ([FeedDataTypeId])
	        REFERENCES [FeedDataTypes]([FeedDataTypeId])
	);

	-- Describe FEEDDATA
	CREATE TABLE IF NOT EXISTS [FeedData] (
		"FeedDataId"		integer NOT NULL,
		"FeedId"		numeric NOT NULL,
		"Value"		nvarchar NOT NULL COLLATE NOCASE,
		"Created"		datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
		"Latitude"		float,
		"Longitude"		float,
		"Elevation"		float,
	    PRIMARY KEY ([FeedDataId])
	,
	    FOREIGN KEY ([FeedId])
	        REFERENCES [Feeds]([FeedId])
	);

	-- Describe FEEDDATAIOTSITE
	CREATE TABLE IF NOT EXISTS [FeedDataIotSite] (
		"FeedDataIotSiteId"		integer NOT NULL,
		"FeedDataId"		numeric NOT NULL,
		"IotSiteId"		numeric NOT NULL,
		"Sent"		datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
	    PRIMARY KEY ([FeedDataIotSiteId])
	,
	    FOREIGN KEY ([FeedDataId])
	        REFERENCES [FeedData]([FeedDataId]),
	    FOREIGN KEY ([IotSiteId])
	        REFERENCES [IotSites]([IotSiteId])
	);
	
	INSERT INTO [IotSites] ([DisplayName]) VALUES('Adafruit');
	INSERT INTO [IotSites] ([DisplayName]) VALUES('Microsoft');
	INSERT INTO [FeedDataTypes] ([DisplayName]) VALUES('Bool');
	INSERT INTO [FeedDataTypes] ([DisplayName]) VALUES('Float');
	INSERT INTO [FeedDataTypes] ([DisplayName]) VALUES('Int');
	INSERT INTO [FeedDataTypes] ([DisplayName]) VALUES('Path');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('Camera');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('Microphone');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('GPS');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('Geiger Counter');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('10-DOF');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('Temperature & Humidity Sensor');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('Digital Light Sensor');
	INSERT INTO [Sensors] ([DisplayName]) VALUES('UV Index Sensor');
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Camera-Image', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Camera' AND [FeedDataTypes].[DisplayName]='Path';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Sound-Level', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Microphone' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Radiation-Counts-per-minute', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Geiger Counter' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Radiation-Dose', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Geiger Counter' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Radiation-Dose-Error', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Geiger Counter' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Accelerometer-X', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Accelerometer-Y', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Accelerometer-Z', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Compass-X', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Compass-Y', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Compass-Z', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Gyroscope-X', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Gyroscope-Y', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Gyroscope-Z', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Air-Pressure', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Air-Temperature1', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='10-DOF' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Air-Temperature2', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Temperature & Humidity Sensor' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Air-Humidity', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Temperature & Humidity Sensor' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Light-IR', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Digital Light Sensor' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Light-Visible', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='Digital Light Sensor' AND [FeedDataTypes].[DisplayName]='Float';
	INSERT INTO [Feeds] ([SensorId], [DisplayName], [FeedDataTypeId]) SELECT [SensorId], 'Light-UV_Index', [FeedDataTypeId] FROM [Sensors], [FeedDataTypes] WHERE [Sensors].[DisplayName]='UV Index Sensor' AND [FeedDataTypes].[DisplayName]='Float';


	
COMMIT;
