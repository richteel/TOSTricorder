#!/usr/bin/env python3

import signal
import gpsd
import sys
import time
import datetime as dt
import subprocess
import os


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
scriptName = __file__
wait_in_seconds_good_read = 60 * 5
wait_in_seconds_bad_read = 60 * 1
msg_format = "gps {} {} {} {}"

packet = None
gpsd_connected = False
verbose = 1


def start_gps():
    write_log("INFORMATION", "Starting gpsd")
    # Set the TTY Properties
    subprocess.call(
        ["stty", "-F", "/dev/ttyS0", "9600", "cs8", "-parenb", "-inpck", "-cstopb", "-hupcl", "brkint", "ignpar",
         "-icrnl", "-opost", "-onlcr", "-isig", "-icanon", "-echo"])
    subprocess.call(["gpsd", "/dev/ttyS0", "-F", "/var/run/gpsd.sock"])
    write_log("INFORMATION", "gpsd Started")
    time.sleep(5)


def connect2gpsd():
    write_log("INFORMATION", "Initializing connection to gpsd")
    global gpsd_connected
    # Connect to the local gpsd
    try:
        gpsd.connect()
        gpsd_connected = True
        return gpsd_connected
    except ConnectionRefusedError as e:
        write_log("WARNING", "Error occurred while disconnecting from gpsd ({})".format(e.strerror))
    except Exception as e:
        write_log("WARNING", "Error occurred while connecting to gpsd ({})".format(e))
        gpsd_connected = False
        return gpsd_connected


def disconnect_gpsd():
    global gpsd_connected
    try:
        gpsd.disconnect()
    except Exception as e:
        write_log("WARNING", "Error occurred while disconnecting from gpsd ({})".format(e))

    gpsd_connected = False


def read_gps(max_attempts=5):
    if not gpsd_connected:
        return False

    global packet
    read_complete = False
    read_attempts = 0

    packet = None

    while read_attempts < max_attempts and not read_complete:
        # Get gps position
        try:
            packet = gpsd.get_current()
            if packet.mode >= 2:
                read_complete = True
        except:
            read_complete = False

        read_attempts += 1
        time.sleep(0.25)  # Delay for 250 milliseconds

    return read_complete


def write2fifo(msg):
    global pipePath
    if not os.path.exists(pipePath):
        return

    with open(pipePath, 'w') as f:
        f.write(msg + os.linesep)
        f.close()


def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


if __name__ == '__main__':
    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    write_log("INFORMATION", "Started")
    while True:
        if killer.kill_now:
            break

        wait_time = wait_in_seconds_bad_read
        if not gpsd_connected:
            if not connect2gpsd():
                start_gps()
                connect2gpsd()

        if gpsd_connected:
            if read_gps(10):
                # Save the data
                if packet.mode == 2:
                    write2fifo(msg_format.format(packet.time_iso, packet.lat, packet.lon, 0.0))
                    wait_time = wait_in_seconds_good_read
                    write_log("DEBUG", "Wrote GPS Data to FIFO", True)
                elif packet.mode == 3:
                    write2fifo(msg_format.format(packet.time_iso, packet.lat, packet.lon, packet.alt))
                    wait_time = wait_in_seconds_good_read
                    write_log("DEBUG", "Wrote GPS Data to FIFO", True)
                else:
                    write_log("DEBUG", "Did not write GPS Data to FIFO as the mode was less than 2", True)

            else:
                write_log("WARNING", "Failed to obtain data from gpsd")

            disconnect_gpsd()

        time.sleep(wait_time)  # set to wait time in seconds

write_log("INFORMATION", "Ended")
