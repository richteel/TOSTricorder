#!/usr/bin/env python3

import os
import fcntl
import sys
import tkinter as tk
import subprocess as sub
import re
import volume

TITLE_FONT = ('Times New Roman', 12, 'bold')
NORMAL_FONT = ('Times New Roman', 12, 'normal')
vIncrement = 1
STREAM_URL = 'http://ice1.somafm.com/u80s-128-mp3'

class RadioPage(tk.Frame):

    def __init__(self, parent, controller):
        self.player = None
        self.__title = "Radio"
        self.v = volume.Volume()
        self.myParent = parent
        self.myController = controller
        tk.Frame.__init__(self, parent)

        # Create GUI Widgets
        self.cmdPlayStop = tk.Button(self, text='Play', font=TITLE_FONT, bg='silver')
        self.volFrame = tk.Frame(self)
        self.sldVolume = tk.Scale(self.volFrame, from_=0, to=100, orient=tk.HORIZONTAL, resolution=1)
        self.cmdVolDown = tk.Button(self.volFrame, text='-', font=TITLE_FONT, bg='silver')
        self.cmdVolUp = tk.Button(self.volFrame, text='+', font=TITLE_FONT, bg='silver')
        self.lblStreamUrl = tk.Label(self, text='Stream URL', font=NORMAL_FONT, anchor='n', justify='left', bg='SeaGreen1', wraplength=315)
        self.lblStreamTitle = tk.Label(self, text='Stream Title', font=NORMAL_FONT, anchor='n', justify='left', bg='beige', wraplength=315)

        # Add event handlers
        self.cmdPlayStop["command"] = self.startRadio
        self.cmdVolDown["command"] = self.cmdVolDown_onClick
        self.cmdVolUp["command"] = self.cmdVolUp_onClick
        self.sldVolume["command"] = self.sldVolume_onChange

        # Add GUI Widgets to parent
        self.cmdVolDown.pack(side = tk.LEFT)
        self.sldVolume.pack(side = tk.LEFT)
        self.cmdVolUp.pack(side = tk.RIGHT)

        self.lblStreamUrl.pack(fill="x")
        self.cmdPlayStop.pack(fill="x")
        self.volFrame.pack()
        self.lblStreamTitle.pack(fill="both", expand=True)

        # self.updateInfo()
        self.sldVolume.set(self.getVolume())

    def __enter__(self):
        return self

    def __del__(self):
        self.endProg()

    def __exit__(self, exc_type, exc_value, traceback):
        self.endProg()

    def endProg(self):
        print("GoodBye")
        if self.player!=None:
            self.player.terminate()

    def cmdVolDown_onClick(self):
        vol = self.sldVolume.get()
        if vol < vIncrement:
            vol = 0
        else:
            vol = vol - vIncrement
        self.sldVolume.set(vol)

    def cmdVolUp_onClick(self):
        vol = self.sldVolume.get()
        if vol > 100 - vIncrement:
            vol = 100
        else:
            vol = vol + vIncrement
        self.sldVolume.set(vol)
        

    def sldVolume_onChange(self, event):
        self.v.set_volume(self.sldVolume.get())
        

    def make_nonblocking(self, output):
        fd = output.fileno()
        fcntl.fcntl(fd, fcntl.F_SETFL, fcntl.fcntl(fd, fcntl.F_GETFL) | os.O_NONBLOCK)
        try:
            return output.read()
        except:
            return ''

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, val):
        self.__title = val

    # @property
    # def player(self):
    #     return self.__player
    #
    # @player.setter
    # def player(self, val):
    #     self.__player = val

    def getStreamUrl(self, subject):
        match = re.search(r"(?<=StreamUrl=([']))(?:(?=(\\?))\2.)*?(?=\1;)", subject.decode('utf-8'))
        if match:
            result = match.group()
        else:
            result = ""
        return result

    def getStreamTitle(self, subject):
        match = re.search(r"(?<=StreamTitle=([']))(?:(?=(\\?))\2.)*?(?=\1;)", subject.decode('utf-8'))
        if match:
            result = match.group()
        else:
            result = ""
        return result

    def startRadio(self):
        ON_POSIX = 'posix' in sys.builtin_module_names

        if self.player==None:
            self.player = sub.Popen(['mpg123', STREAM_URL], stdout=sub.PIPE, stderr=sub.PIPE, bufsize=1, close_fds=ON_POSIX)
            
            self.cmdPlayStop.config(text="Stop")
            self.after(500, self.updateStatus)
        else:
            self.player.terminate()
            self.player = None
            self.cmdPlayStop["text"]="Play"

    def updateStatus(self):
        if self.player!=None:
            line = self.make_nonblocking(self.player.stdout)
            if not line:
                line = self.make_nonblocking(self.player.stderr)
            if line and line != '':
                streamtitle = self.getStreamTitle(line)
                streamurl = self.getStreamUrl(line)
                if streamtitle and streamtitle != '':
                    self.lblStreamTitle['text'] = streamtitle
                if streamurl and streamurl != '':
                    self.lblStreamUrl['text'] = streamurl
            self.after(500, self.updateStatus)

    def getVolume(self):
        return self.v.volume


