TOSTricorder - Star Trek (The Original Series) Tricorder
--------------------------------------------------------

This project is about stuffing a Raspberry Pi 3 into a Diamond Select Toys Star Trek: The Original Series Tricorder with a bunch of sensors, a display, and a few switches.

This repository contains the Linux program source code for the project. More details for the project may be found in the documentation folder as well as the Electrical Engineer's Logbook blog at [www.teelsys.com](http://www.teelsys.com).

![Diamond Select Toys Star Trek: The Original Series Tricorder](https://gitlab.com/richteel/TOSTricorder/raw/4867821469b07760c835d418a724ad539ec696f3/Ticorder.jpg)

Installation Instructions

1. Download the latest Raspbian Image from [Raspberry Pi - Downloads](https://www.raspberrypi.org/downloads/raspbian/)
	+ Version as of this document is Raspbian Jessie 2016-05-27
2. Install the Raspbian Image to an SD Card based on the instructions at [Raspberry Pi - Installing Operating System Images](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)
3. Insert the SD Card in the Raspberry Pi, connect a HDMI monitor and keyboard
4. Power on the Raspberry Pi
5. Setup connection to wifi
	+ If you plan to use SSH for the following steps, use ifconfig to find the ip address
6. Open a terminal window and run the following commands
	+ git clone https://gitlab.com/richteel/TOSTricorder.git
	+ cd TOSTricorder
	+ sudo ./setup.sh -u y
	+ *Raspberry Pi will reboot*
	+ cd TOSTricorder
	+ sudo ./setup.sh
7. Edit the /home/pi/TOSTricorder/iot.config file by running
	+ nano /home/pi/TOSTricorder/iot.config
	+ Change <Your Adafruit.IO key> to your key
	+ For now, you may ignore the UserName setting. This may be used later when implementing MQTT.
	+ Save the file <Ctrl>+x, Y, <Enter>
8. Reboot

If you have already installed the software, then run the following commands to update it.

	cd ~
	cd TOSTricorder
	git pull https://gitlab.com/richteel/TOSTricorder.git
	sudo ./setup.sh

# Parameters for setup.sh #
Usage: setup.sh [-u &lt;y/n&gt;] [-c &lt;2 char country code&gt;] [-l &lt;2 char language code&gt;] [-t &lt;string timezone&gt;] [-h &lt;string hostname&gt;]

All parameters are optional

- u
	- **Description:** Run Updates on Operating System and Installed Software
	- **Values:** y (yes) or n (no)
	- **Default:** n
- c
	- **Description:** Country Code used for localization
	- **Values:** Valid country codes are contained in /etc/locale.gen. The combination of Language and Country must exist in the file.
	- **Default:** us
- l
	- **Description:** Language Code used for localization
	- **Values:** Valid language codes are contained in /etc/locale.gen. The combination of Language and Country must exist in the file.
	- **Default:** en
- t
	- **Description:** Timezone
	- **Values:** Valid timezones are found in /usr/share/zoneinfo/.
	- **Default:** America/New_York
- h
	- **Description:** Hostname
	- **Values:** Valid hostnames only contain upper and lower case letters, numbers and the hyphen "-" character. Hostnames must start and end with a letter or a number. Best practice is to keep hostnames to 15 characters or less even though they may contain up to 63 characters.
	- **Default:** tricorder001

# Hardware #

## Sensors ##

|#|Sensor|Vendor|Part Number|Communication|
|---|---|---|---|---|
|1|Camera|Adafruit|[3100](https://www.adafruit.com/products/3100)|CSi interface|
|2|Microphone|Adafruit|[2716](https://www.adafruit.com/products/2716)|USB*|
|3|GPS|Adafruit|[746](https://www.adafruit.com/products/746)|Serial (14 & 15)|
|4|Geiger Counter|Radiation-Watch|[Type 5](http://www.radiation-watch.org/p/english.html)|GPIO (22 & 23)|
|5|gyroscope, compass, accelerometer, pressure, & temperature|Adafruit|[1604](https://www.adafruit.com/products/1604)|I2C (0x19, 0x1E, 0x6B, 0x77)|
|6|temperature & humidity|Adafruit|[2857](https://www.adafruit.com/products/2857)|I2C (0x44)|
|7|Light & IR|Adafruit|[1980](https://www.adafruit.com/products/1980)|I2C (0x29)|
|8|UV|Adafruit|[2899](https://www.adafruit.com/products/2899)|I2C (0x29)|
