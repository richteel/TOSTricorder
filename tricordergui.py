#!/usr/bin/env python3

import os
import tkinter as tk
from tkinter import messagebox
import subprocess
import psutil
import sqlite3
import socket
import datetime as dt
import traceback
import sys

import RadioPage as radioPg

TITLE_FONT = ('Times New Roman', 12, 'bold')
NORMAL_FONT = ('Times New Roman', 12, 'normal')
STATUS_FONT = ('Times New Roman', 10, 'normal')
databaseFile = "/home/pi/TOSTricorder/tricorder.db"
sqlGetDataFeed = """SELECT F.DisplayName, FD.Value, FD.Created, FD.Latitude, FD.Longitude, FD.Elevation, FDS.Sent, ITS.DisplayName
	FROM Feeds F
	INNER JOIN FeedData FD
	ON F.FeedId=FD.FeedId
	LEFT OUTER JOIN FeedDataIotSite FDS
	ON FD.FeedDataId=FDS.FeedDataId
	LEFT OUTER JOIN IotSites ITS
	ON FDS.IotSiteId=ITS.IotSiteId
	WHERE F.DisplayName=?
	ORDER BY FD.Created DESC, FDS.Sent DESC  LIMIT 1;"""
programLogFile = "/home/pi/TOSTricorder/tricordergui.log"
verbose = 1
scriptName = __file__


def log_tkinter_error(self, *args):
    # err = traceback.format_exception(*args)
    var = traceback.format_exc()
    write_log("ERROR", var, True)


class TricorderForm(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        # Set the size
        self.geometry('318x202+0+0')

        # Add hotkeys
        # self.bind('<Left>', self.previous_page_e)
        # self.bind('<Right>', self.next_page_e)

        # Show Splash
        self.sp = SplashScreen(self, self)
        self.wm_title(self.sp.title)
        self.sp.pack(fill="both", expand=True)

        self.pageIdx = 0
        self.lastPageIdx = -1

        # Show the spash screen until the main form is ready to be displayed
        self.update()

        self.showMain()

    def loadUi(self):
        # Create GUI Widgets
        self.bottomFrame = tk.Frame(self, bg='SkyBlue2')
        self.cmdPrevious = tk.Button(self.bottomFrame, width=6, text='<', font=TITLE_FONT, bg='silver')
        self.cmdNext = tk.Button(self.bottomFrame, width=6, text='>', font=TITLE_FONT, bg='silver')
        self.lblPageNumber = tk.Label(self.bottomFrame, text='Page of Pages', font=NORMAL_FONT)
        self.lblPageNumber["bg"] = self.bottomFrame["bg"]
        self.contentFrame = tk.Frame(self, bg='Pink')

        # Add event handlers
        self.cmdPrevious["command"] = self.previous_page
        self.cmdNext["command"] = self.next_page

        # Add GUI Widgets to parent
        self.bottomFrame.pack(side="bottom", fill="x")
        self.cmdPrevious.pack(side="left")
        self.cmdNext.pack(side="right")
        self.lblPageNumber.pack(fill="both", expand=True)
        self.contentFrame.pack(fill="both", expand=True)

        # Set the tab order
        new_order = (self.cmdPrevious, self.cmdNext)
        for widget in new_order:
            widget.lift()

        # Create Pages
        airPage = InfoPage(self.contentFrame, self)
        airPage.title = "Air"
        airPage.databaseFile = databaseFile
        airPage.sensorInfo = [{'feed': 'Air-Pressure', 'displayname': 'Pressure', 'units': 'hPa'},
                              {'feed': 'Air-Temperature1', 'displayname': 'Temp1', 'units': 'C'},
                              {'feed': 'Air-Temperature2', 'displayname': 'Temp2', 'units': 'C'},
                              {'feed': 'Air-Humidity', 'displayname': 'Humidity', 'units': '%'}]

        lightPage = InfoPage(self.contentFrame, self)
        lightPage.title = "Light"
        lightPage.databaseFile = databaseFile
        lightPage.sensorInfo = [{'feed': 'Light-IR', 'displayname': 'IR', 'units': ''},
                                {'feed': 'Light-Visible', 'displayname': 'Visible', 'units': ''},
                                {'feed': 'Light-UV_Index', 'displayname': 'UV', 'units': ''}]

        radiationPage = InfoPage(self.contentFrame, self)
        radiationPage.title = "Radiation"
        radiationPage.databaseFile = databaseFile
        radiationPage.sensorInfo = [{'feed': 'Radiation-Counts-per-minute', 'displayname': 'Count', 'units': 'cpm'},
                                    {'feed': 'Radiation-Dose', 'displayname': 'Dose', 'units': 'uSv/h'},
                                    {'feed': 'Radiation-Dose-Error', 'displayname': 'Dose Error', 'units': ''}]

        accelerometerPage = InfoPage(self.contentFrame, self)
        accelerometerPage.title = "Accelerometer"
        accelerometerPage.databaseFile = databaseFile
        accelerometerPage.sensorInfo = [{'feed': 'Accelerometer-X', 'displayname': 'X', 'units': ''},
                                        {'feed': 'Accelerometer-Y', 'displayname': 'Y', 'units': ''},
                                        {'feed': 'Accelerometer-Z', 'displayname': 'Z', 'units': ''}]

        compassPage = InfoPage(self.contentFrame, self)
        compassPage.title = "Compass"
        compassPage.databaseFile = databaseFile
        compassPage.sensorInfo = [{'feed': 'Compass-X', 'displayname': 'X', 'units': ''},
                                  {'feed': 'Compass-Y', 'displayname': 'Y', 'units': ''},
                                  {'feed': 'Compass-Z', 'displayname': 'Z', 'units': ''}]

        gyroscopePage = InfoPage(self.contentFrame, self)
        gyroscopePage.title = "Gyroscope"
        gyroscopePage.databaseFile = databaseFile
        gyroscopePage.sensorInfo = [{'feed': 'Gyroscope-X', 'displayname': 'X', 'units': ''},
                                    {'feed': 'Gyroscope-Y', 'displayname': 'Y', 'units': ''},
                                    {'feed': 'Gyroscope-Z', 'displayname': 'Z', 'units': ''}]

        systemStatusPage = SystemStatusPage(self.contentFrame, self)
        systemStatusPage.title = "System Status"

        processesStatusPage = ProcessesStatusPage(self.contentFrame, self)
        processesStatusPage.title = "Process and Module Status"

        utilityPage = UtilityPage(self.contentFrame, self)
        utilityPage.title = "Utilities"

        radioPage = radioPg.RadioPage(self.contentFrame, self)
        radioPage.title = "Radio"

        self.pages = (
            airPage, lightPage, radiationPage, accelerometerPage, compassPage, gyroscopePage, 
            radioPage, 
            systemStatusPage, processesStatusPage, utilityPage)

    def showMain(self):
        self.loadUi()
        self.sp.close()
        self.change_page()

        # Set entry focus to the next button
        self.cmdNext.focus_set()

    def show_frame(self, cont):
        if self.lastPageIdx >= 0:
            self.pages[self.lastPageIdx].pack_forget()

        self.lastPageIdx = self.pageIdx
        frame = self.pages[cont]
        # frame.tkraise()
        self.wm_title(self.pages[cont].title)
        frame.pack(fill="both", expand=True)

    def change_page(self):
        self.lblPageNumber['text'] = "{} of {}".format(self.pageIdx + 1, len(self.pages))
        self.show_frame(self.pageIdx)

    def previous_page_e(self, event):
        self.previous_page()

    def previous_page(self):
        if (self.pageIdx == 0) and (len(self.pages) > 0):
            self.pageIdx = len(self.pages) - 1
        else:
            self.pageIdx = self.pageIdx - 1

        self.change_page()

    def next_page_e(self, event):
        self.next_page()

    def next_page(self):
        if (self.pageIdx < len(self.pages) - 1):
            self.pageIdx += 1
        else:
            self.pageIdx = 0

        self.change_page()


class InfoPage(tk.Frame):
    def __init__(self, parent, controller):
        self.__title = "Tricorder-Info"
        self.__sensorInfo = []
        self.myParent = parent
        self.myController = controller
        tk.Frame.__init__(self, parent)

        # Create GUI Widgets
        self.lblInformation = tk.Label(self, text='Info', font=NORMAL_FONT, anchor='nw', justify='left', bg='beige')

        # Add event handlers

        # Add GUI Widgets to parent
        self.lblInformation.pack(fill="both", expand=True)

        # self.updateInfo()

    @property
    def databaseFile(self):
        return self.__databaseFile

    @databaseFile.setter
    def databaseFile(self, val):
        self.__databaseFile = val

    @property
    def sensorInfo(self):
        return self.__sensorInfo

    @sensorInfo.setter
    def sensorInfo(self, val):
        self.__sensorInfo = val
        self.updateInfo()

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, val):
        self.__title = val

    def updateInfo(self):
        strDisplay = ''
        dte = ''
        lat = ''
        lng = ''
        elv = ''
        snt = ''
        iot = ''
        val = ''

        if not os.path.isfile(self.__databaseFile):
            strDisplay = "No data present"
        else:
            conn = sqlite3.connect('file:' + self.__databaseFile + '?mode=ro', uri=True)
            c = conn.cursor()

            for dic in self.__sensorInfo:
                if len(strDisplay) > 0:
                    strDisplay = strDisplay + '\n'

                val = ''

                sqlParms = (dic['feed'],)
                try:
                    c.execute(sqlGetDataFeed, sqlParms)
                    all_rows = c.fetchall()
                    if all_rows is not None and len(all_rows) > 0 and len(all_rows[0]) > 0:
                        val = all_rows[0][1]
                        dte = all_rows[0][2]
                        lat = all_rows[0][3] if all_rows[0][3] is not None else lat
                        lng = all_rows[0][4] if all_rows[0][4] is not None else lng
                        elv = all_rows[0][5] if all_rows[0][5] is not None else elv
                        snt = all_rows[0][6] if all_rows[0][6] is not None else snt
                        iot = all_rows[0][7] if all_rows[0][7] is not None else iot
                except Exception as ex:
                    write_log("ERROR", "Failed to execute query to get datafeed. ({})".format(ex), True)

                strDisplay = strDisplay + '{}: {} {}'.format(dic['displayname'], val, dic['units'])

            conn.close()

        capInfo = "\n\nCaptured: {}".format(dte)
        locInfo = "\nLocation: {}, {} {} M".format(lat, lng, elv)
        iotInfo = "\nUploaded: {} to {}".format(snt, iot)
        self.lblInformation['text'] = strDisplay + capInfo + locInfo + iotInfo

        # Update the information every minute
        self.after(60000, self.updateInfo)


class ProcessesStatusPage(tk.Frame):
    def __init__(self, parent, controller):
        self.__title = "Tricorder-Utility"
        self.myParent = parent
        self.myController = controller
        tk.Frame.__init__(self, parent, bg='tan')

        # Create GUI Widgets
        # row0Frame = tk.Frame(self)
        # row1Frame = tk.Frame(self, bg='brown', height=10)
        # row2Frame = tk.Frame(self, bg='red', height=10)
        # row3Frame = tk.Frame(self, bg='orange', height=10)
        # frMid0 = tk.Frame(row0Frame, bg='yellow')
        self.lblTricorder = tk.Label(self, text="Tricorder Serv", font=STATUS_FONT)
        self.lblNeopixels = tk.Label(self, text="Neopixels Serv", font=STATUS_FONT)
        self.lblDatabase = tk.Label(self, text="Database Serv", font=STATUS_FONT)
        self.lblGPS = tk.Label(self, text="GPS Serv", font=STATUS_FONT)
        self.lblAir = tk.Label(self, text="Air Monitoring Serv", font=STATUS_FONT)
        self.lblRadiation = tk.Label(self, text="Radiation Monitoring Serv", font=STATUS_FONT)
        self.lblLight = tk.Label(self, text="Light Monitoring Serv", font=STATUS_FONT)
        self.lblInertia = tk.Label(self, text="Inertia Monitoring Serv", font=STATUS_FONT)
        self.lblCamera = tk.Label(self, text="Camera Monitoring Serv", font=STATUS_FONT)
        self.lblGPSD = tk.Label(self, text="GPSD Serv", font=STATUS_FONT)
        self.lblIot = tk.Label(self, text="IOT Serv", font=STATUS_FONT)
        self.lblButtons = tk.Label(self, text="Tricorder Buttons Serv", font=STATUS_FONT)
        self.lblUinput = tk.Label(self, text="UInput Module", font=STATUS_FONT)

        # Add event handlers

        # Add GUI Widgets to parent

        self.lblTricorder.grid(row=0, column=0, padx=2, pady=1)
        self.lblNeopixels.grid(row=0, column=1, padx=2, pady=1)
        self.lblDatabase.grid(row=1, column=0, padx=2, pady=1)
        self.lblGPS.grid(row=1, column=1, padx=2, pady=1)
        self.lblAir.grid(row=2, column=0, padx=2, pady=1)
        self.lblRadiation.grid(row=2, column=1, padx=2, pady=1)
        self.lblLight.grid(row=3, column=0, padx=2, pady=1)
        self.lblInertia.grid(row=3, column=1, padx=2, pady=1)
        self.lblCamera.grid(row=4, column=0, padx=2, pady=1)
        self.lblGPSD.grid(row=4, column=1, padx=2, pady=1)
        self.lblIot.grid(row=5, column=0, padx=2, pady=1)
        self.lblButtons.grid(row=5, column=1, padx=2, pady=1)
        self.lblUinput.grid(row=6, column=0, padx=2, pady=1)

        self.processList = ['Name, scriptName']

        self.updateStatus()

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, title):
        self.__title = title

    def updateStatus(self):
        self.updateStatusLabel(self.lblTricorder, 'tricorder.sh', 'tricorder.sh', updateList=True)
        self.updateStatusLabel(self.lblNeopixels, 'python', 'neopixels.py')
        self.updateStatusLabel(self.lblDatabase, 'python3', 'dbwritesensordata.py')
        self.updateStatusLabel(self.lblGPS, 'python3', 'dataGps.py')
        self.updateStatusLabel(self.lblAir, 'python', 'dataAir.py')
        self.updateStatusLabel(self.lblRadiation, 'python', 'dataRadiation.py')
        self.updateStatusLabel(self.lblLight, 'python', 'dataLight.py')
        self.updateStatusLabel(self.lblInertia, 'python', 'dataInertia.py')
        self.updateStatusLabel(self.lblCamera, 'python', 'dataCamera.py')
        self.updateStatusLabel(self.lblGPSD, 'gpsd', 'ttyS0')
        self.updateStatusLabel(self.lblIot, 'python3', 'iotClient.py')
        self.updateStatusLabel(self.lblButtons, 'python', 'tricorder-buttons.py')
        self.updateStatusLabel(self.lblUinput, 'uinput', isModule=True)

        # Update the processes status every minute
        self.after(60000, self.updateStatus)

    def updateStatusLabel(self, statusLabel, processName, scriptName='', isModule=False, updateList=False):
        if isModule:
            if self.isModuleLoaded(processName):
                statusLabel['bg'] = 'green'
            else:
                statusLabel['bg'] = 'red'
        else:
            if self.isProcessRunning(processName, scriptName, updateList):
                statusLabel['bg'] = 'green'
            else:
                statusLabel['bg'] = 'red'

    def isModuleLoaded(self, moduleName):
        c = subprocess.Popen("lsmod", stdout=subprocess.PIPE)
        gr = subprocess.Popen(["grep", moduleName + ' '], stdin=c.stdout, stdout=subprocess.PIPE)
        if len(gr.communicate()[0]) > 0:
            return True

        return False

    def isProcessRunning(self, processName, scriptName, updateList=False):
        if updateList or len(self.processList) == 0:
            self.processList = []
            for pid in psutil.pids():
                try:
                    p = psutil.Process(pid)
                    pName = p.name()
                    sName = p.cmdline()[1] if len(p.cmdline()) > 1 else ''
                    sName = sName.split("/")[len(sName.split("/")) - 1]
                    s = "{}\t{}".format(pName, sName)
                    self.processList.append(s)
                    # print(self.processList)
                except Exception as ex:
                    write_log("ERROR", "Error while checking process - {},{} ({})".format(processName, scriptName, ex))

        if len(scriptName) == 0:
            if any(scriptName in s for s in self.processList):
                return True
        else:
            sTest = "{}\t{}".format(processName, scriptName)
            if sTest in self.processList:
                return True

        # for pid in psutil.pids():
        #     p = psutil.Process(pid)
        #     if p.name() == processName:
        #         if len(scriptName) == 0:
        #             return True
        #         elif len(p.cmdline()) > 1 and scriptName in p.cmdline()[1]:
        #             return True

        return False


class SplashScreen(tk.Frame):
    def __init__(self, parent, controller):
        self.__title = "Tricorder"
        self.myParent = parent
        self.myController = controller
        tk.Frame.__init__(self, parent)

        # Create GUI Widgets
        self.background_image = tk.PhotoImage(file='/home/pi/TOSTricorder/sc_logo_small.png')

        lblModel = tk.Label(self, text='Tricorder Type I', font=TITLE_FONT, anchor='n', justify='center')
        lblBackground = tk.Label(self, anchor='w', image=self.background_image, bg='#07549a')
        lblInformation = tk.Label(self, font=NORMAL_FONT, justify='center', bg='white')
        lblInformation['text'] = "Serial #: 0001\nShip: Enterprise"

        # Add event handlers

        # Add GUI Widgets to parent
        lblModel.pack(side='top', fill="x", expand=True)
        lblBackground.pack(side='left')
        lblInformation.pack(fill='both', expand=True)

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, title):
        self.__title = title

    def close(self):
        self.pack_forget()
        self.destroy()

    def show(self, seconds2Show):
        # self.root.deiconify()  # Show the window
        self.root.after(1000 * seconds2Show, self.close)  # Schedule self.hide in show_int seconds


class SystemStatusPage(tk.Frame):
    def __init__(self, parent, controller):
        self.__title = "Tricorder-Info"
        self.myParent = parent
        self.myController = controller
        tk.Frame.__init__(self, parent)

        # Create GUI Widgets
        self.lblInformation = tk.Label(self, text='Info', font=STATUS_FONT, anchor='nw', justify='left', bg='tan')

        # Add event handlers

        # Add GUI Widgets to parent
        self.lblInformation.pack(fill="both", expand=True)

        self.updateStatus()

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, title):
        self.__title = title

    def updateStatus(self):
        # CPU informatiom
        CPU_temp = self.getCPUtemperature()
        # CPU_usage = self.getCPUuse()
        CPU_usage = psutil.cpu_percent(interval=1)

        # RAM information
        # Output is in kb, here I convert it in Mb for readability
        RAM_stats = self.getRAMinfo()
        RAM_total = round(int(RAM_stats[0]) / 1000, 1)
        RAM_used = round(int(RAM_stats[1]) / 1000, 1)
        RAM_free = round(int(RAM_stats[2]) / 1000, 1)

        # Disk information
        DISK_stats = self.getDiskSpace()
        DISK_total = DISK_stats[0]
        DISK_used = DISK_stats[1]
        DISK_free = DISK_stats[2]
        DISK_perc = DISK_stats[3]

        # Network Info
        #ipAddress = self.getNetworkInfo()['ip']
        ipAddress = self.getNetworkInfo2()

        sRam = "RAM (total): {}\nRAM (used): {}\nRAM (free): {}\n".format(RAM_total, RAM_used, RAM_free)
        sDisk = "Disk (Size): {}\nDisk (Used): {} ({})\nDisk (Avail): {}\n".format(DISK_total, DISK_used, DISK_perc,
                                                                                   DISK_free)
        sCpu = "CPU Usage: {}\nCPU Temp (C): {}\n".format(CPU_usage, CPU_temp)
        sNetwork = "IP Address: {}\n".format(ipAddress)

        self.lblInformation['text'] = "{}{}{}{}".format(sRam, sDisk, sCpu, sNetwork)
        # Update system information every minute
        self.after(60000, self.updateStatus)

    # Return CPU temperature as a character string
    def getCPUtemperature(self):
        res = os.popen('vcgencmd measure_temp').readline()
        return (res.replace("temp=", "").replace("'C\n", ""))

    # Return RAM information (unit=kb) in a list
    # Index 0: total RAM
    # Index 1: used RAM
    # Index 2: free RAM
    def getRAMinfo(self):
        p = os.popen('free')
        i = 0
        while 1:
            i = i + 1
            line = p.readline()
            if i == 2:
                return (line.split()[1:4])

    # Return % of CPU used by user as a character string
    def getCPUuse(self):
        return (str(os.popen("top -n1 | awk '/Cpu\(s\):/ {print $2}'").readline().strip()))

    # Return information about disk space as a list (unit included)
    # Index 0: total disk space
    # Index 1: used disk space
    # Index 2: remaining disk space
    # Index 3: percentage of disk used
    def getDiskSpace(self):
        p = os.popen("df -h /")
        i = 0
        while 1:
            i = i + 1
            line = p.readline()
            if i == 2:
                return (line.split()[1:5])

    def check_internet(host="8.8.8.8", port=53, timeout=3):
        """
        Host: 8.8.8.8 (google-public-dns-a.google.com)
        OpenPort: 53/tcp
        Service: domain (DNS/TCP)
        """
        try:
            socket.setdefaulttimeout(timeout)
            socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
            return True
        except Exception as ex:
            return False

    def getNetworkInfo(self):
        ipaddr = ''
        gateway = ''
        host = ''

        if self.check_internet():
            gw = os.popen("ip -4 route show default").read().split()
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect((gw[2], 0))
            ipaddr = s.getsockname()[0]
            gateway = gw[2]
            host = socket.gethostname()

        return {'ip': ipaddr, 'gw': gateway, 'host': host}

    def getNetworkInfo2(self):
        #retVal = (str(os.popen("hostname -I").readline().strip()))
        retVal = subprocess.check_output(["hostname", "-I"]).decode('ascii').strip()
        return retVal


class UtilityPage(tk.Frame):
    def __init__(self, parent, controller):
        self.__title = "Tricorder-Utility"
        self.myParent = parent
        self.myController = controller
        tk.Frame.__init__(self, parent, bg='tan')

        # Create GUI Widgets
        row0Frame = tk.Frame(self, bg='tan')
        row1Frame = tk.Frame(self, bg='brown', height=10)
        row2Frame = tk.Frame(self, bg='red', height=10)
        row3Frame = tk.Frame(self, bg='orange', height=10)
        # frMid0 = tk.Frame(row0Frame, bg='yellow')
        cmdQuit = tk.Button(row0Frame, text='Exit App', font=TITLE_FONT, bg='silver')
        cmdReboot = tk.Button(row0Frame, text='Reboot', font=TITLE_FONT, bg='silver')
        cmdShutdown = tk.Button(row0Frame, text='Shutdown', font=TITLE_FONT, bg='silver')

        # Add event handlers
        cmdQuit["command"] = self.quit_app
        cmdReboot["command"] = self.reboot
        cmdShutdown["command"] = self.shutdown

        # Add GUI Widgets to parent
        row0Frame.grid(row=0, column=0, padx=0, pady=0, sticky="we")
        # row1Frame.grid(row=1, column=0, padx=1, pady=1)
        # row2Frame.grid(row=2, column=0, padx=1, pady=1)
        # row3Frame.grid(row=3, column=0, padx=1, pady=1)
        cmdQuit.grid(row=0, column=0, padx=9, pady=2)
        cmdReboot.grid(row=0, column=1, padx=9, pady=2)
        cmdShutdown.grid(row=0, column=2, padx=10, pady=2)

        # Set the tab order
        new_order = (cmdQuit, cmdReboot, cmdShutdown)
        for widget in new_order:
            widget.lift()

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, title):
        self.__title = title

    def quit_app(self):
        result = messagebox.askyesno("Confirm", "Do you wish to exit?", parent=app)
        if result:
            app.destroy()

    def reboot(self):
        result = messagebox.askyesno("Confirm", "Do you wish to reboot?", parent=app)
        if result:
            os.system("sudo reboot -h now")

    def shutdown(self):
        result = messagebox.askyesno("Confirm", "Do you wish to shutdown?", parent=app)
        if result:
            os.system("sudo shutdown -h now")

# Log Handling
def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]

def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))

def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()

# Delete the previous archived log file if it exists
if os.path.isfile(programLogFile + '.bak'):
    os.remove(programLogFile + '.bak')
# If the log file exists, archive it
if os.path.isfile(programLogFile):
    os.rename(programLogFile, programLogFile + '.bak')

write_log("INFORMATION", "Started", True)
tk.Tk.report_callback_exception = log_tkinter_error

app = TricorderForm()
app.config(cursor="none")
app.mainloop()

write_log("INFORMATION", "Closed", True)
