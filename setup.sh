#!/bin/bash

CWDIR=$(pwd)

# Check that the user ran as sudo
if [ "$EUID" -ne 0 ]
	then echo "Please run using sudo ./$0 (You may use 'sudo !!' now)"
	exit
fi

# Default Parameters
P_UPDATE="n"
P_UPDATE_SERVICES="n"
P_UPDATE_ALL='n'
P_COUNTRY="us"
P_LANG="en"
P_TZ="America/New_York"
P_HOSTNAME="tricorder001"

usage() { echo "Usage: $0 [-u <y/n|$P_UPDATE>] [-s <y/n|$P_UPDATE_SERVICES>] [-a <y/n|$P_UPDATE_ALL>] [-c <2 char country code|$P_COUNTRY>] [-l <2 char language code|$P_LANG>] [-t <string timezone|$P_TZ>] [-h <string hostname|$P_HOSTNAME>]" 1>&2; exit 1; }


while getopts ":u:a:s:c:l:t:h:" o; do
	case "${o}" in
		u)
			P_UPDATE=${OPTARG}
			((P_UPDATE == "y" || P_UPDATE == "n")) || usage
			;;
		s)
			P_UPDATE_SERVICES=${OPTARG}
			((P_UPDATE_SERVICES == "y" || P_UPDATE_SERVICES == "n")) || usage
			;;
		a)
			P_UPDATE_ALL=${OPTARG}
			((P_UPDATE_ALL == "y" || P_UPDATE_ALL == "n")) || usage
			;;
		c)
			P_COUNTRY=${OPTARG}
			;;
		l)
			P_LANG=${OPTARG}
			;;
		t)
			P_TZ=${OPTARG}
			;;
		h)
			P_HOSTNAME=${OPTARG}
			;;
		*)
			usage
			;;
	esac
done

# To lowercase
P_UPDATE="${P_UPDATE,,}"
P_UPDATE_SERVICES="${P_UPDATE_SERVICES,,}"
P_UPDATE_ALL="${P_UPDATE_ALL,,}"
P_COUNTRY="${P_COUNTRY,,}"
P_LANG="${P_LANG,,}"

echo "Update = ${P_UPDATE}"
echo "Update Services = ${P_UPDATE_SERVICES}"
echo "Update All = ${P_UPDATE_ALL}"
echo "Country = ${P_COUNTRY}"
echo "Language = ${P_LANG}"
echo "Timezone = ${P_TZ}"
echo "Hostname = ${P_HOSTNAME}"

# ********** FUNCTIONS **********
set_config_var() {
lua - "$1" "$2" "$3" <<EOF > "$3.bak"
local key=assert(arg[1])
local value=assert(arg[2])
local fn=assert(arg[3])
local file=assert(io.open(fn))
local made_change=false
for line in file:lines() do
	if line:match("^#?%s*"..key.."=.*$") then
	line=key.."="..value
	made_change=true
	end
	print(line)
end

if not made_change then
	print(key.."="..value)
end
EOF
mv "$3.bak" "$3"
}

clear_config_var() {
lua - "$1" "$2" <<EOF > "$2.bak"
local key=assert(arg[1])
local fn=assert(arg[2])
local file=assert(io.open(fn))
for line in file:lines() do
	if line:match("^%s*"..key.."=.*$") then
	line="#"..line
	end
	print(line)
end
EOF
mv "$2.bak" "$2"
}

get_config_var() {
lua - "$1" "$2" <<EOF
local key=assert(arg[1])
local fn=assert(arg[2])
local file=assert(io.open(fn))
local found=false
for line in file:lines() do
	local val = line:match("^%s*"..key.."=(.*)$")
	if (val ~= nil) then
	print(val)
	found=true
	break
	end
end
if not found then
	 print(0)
end
EOF
}


# ********** MAIN **********
# --------- UPDATES --------
if [ $P_UPDATE == "y" ]; then
	echo
	echo "*********************************"
	echo "********** GET UPDATES **********"
	echo "*********************************"
	apt-get --assume-yes update
	# rpi-update
	echo
	echo "Done"
	CHANGEMADE="Yes"
	
	#apt-get install git-core
		#wget https://raw.github.com/Hexxeh/rpi-update/master/rpi-update -O /usr/bin/rpi-update
		#chmod +x /usr/bin/rpi-update
		#BRANCH=next rpi-update
	
	echo
	echo Please reboot and run setup again
	echo
	sleep 5s # Waits 5 seconds.
	reboot now
	exit
fi


# -----Python Libraries ----
P_COUNTER=0
###########################################################################
P_TITLE="Adafruit_IO Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/Adafruit_IO/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	pip install adafruit-io
	pip3 install adafruit-io

	# Add custom code to client to save location data
	sudo cp /home/pi/TOSTricorder/client.py /usr/local/lib/python3.4/dist-packages/Adafruit_IO/client.py

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="BMP180 Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/Adafruit_BMP-1.5.1-py2.7.egg ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	wget https://github.com/adafruit/Adafruit_Python_BMP/archive/master.zip
	unzip master.zip
	rm master.zip
	cd Adafruit_Python_BMP-master
	python ./setup.py install
	mkdir /usr/local/lib/python2.7/dist-packages/Adafruit_BMP
	cd ..
	cp ./Adafruit_Python_BMP-master/Adafruit_BMP/* /usr/local/lib/python2.7/dist-packages/Adafruit_BMP/
	rm -r Adafruit_Python_BMP-master
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="ConfigParser (Python3) Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python3.4/dist-packages/configparser-3.5.0-nspkg.pth ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	pip3 install ConfigParser

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="GPS Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python3.4/dist-packages/gpsd_py3-0.2.0-py3.4.egg ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	# sudo pip3 install gpsd-py3
	wget https://github.com/richteelbah/gpsd-py3/archive/master.zip
	unzip master.zip
	rm master.zip
	cd gpsd-py3-master
	python3 ./setup.py install
	cd ..
	rm -r gpsd-py3-master
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="L3GD20H Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /home/pi/TOSTricorder/L3GD20/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	wget https://github.com/mpolaczyk/Gyro-L3GD20-Python/archive/master.zip
	unzip master.zip
	rm master.zip
	mv Gyro-L3GD20-Python-master /home/pi/TOSTricorder/L3GD20

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="LSM303 Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/Adafruit_LSM303-1.0.0-py2.7.egg ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	wget https://github.com/adafruit/Adafruit_Python_LSM303/archive/master.zip
	unzip master.zip
	rm master.zip
	cd Adafruit_Python_LSM303-master
	python ./setup.py install
	cd ..
	rm -r Adafruit_Python_LSM303-master
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="Neopixel Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/bin/scons ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install build-essential python-dev git scons swig
	wget https://github.com/jgarff/rpi_ws281x/archive/master.zip
	unzip master.zip
	rm master.zip
	cd rpi_ws281x-master
	scons
	cd python
	python ./setup.py install
	cd ../..
	rm -r rpi_ws281x-master
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="NTP Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/lib/python3/dist-packages/ntplib.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	mkdir ntp
	cd ntp
	wget http://ftp.us.debian.org/debian/pool/main/n/ntplib/python3-ntplib_0.3.3-1_all.deb
	dpkg --install python3-ntplib_0.3.3-1_all.deb
	cd ..
	rm -r ntp
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="PiCamera Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/lib/python2.7/dist-packages/picamera/__init__.py ] || [ ! -f /usr/lib/python3/dist-packages/picamera/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install python-picamera
	apt-get --assume-yes install python3-picamera

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="PiPocketGeiger Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/PiPocketGeiger/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	pip install PiPocketGeiger

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="PSUtil Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/psutil-4.3.2-py2.7-linux-armv7l.egg/psutil/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	wget https://github.com/giampaolo/psutil/archive/master.zip
	unzip master.zip
	rm master.zip
	cd psutil-master
	python ./setup.py install
	cd ..
	rm -r psutil-master
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="PSUtil (Python 3) Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/lib/python3/dist-packages/psutil/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get install python3-psutil

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="SHT31 Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/sht31.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	wget https://github.com/jaques/sht21_python/archive/master.zip
	unzip master.zip
	rm master.zip
	mkdir /usr/local/lib/python2.7/dist-packages/sht
	cp ./sht21_python-master/sht31.py /usr/local/lib/python2.7/dist-packages/sht31.py
	rm -r sht21_python-master
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="TSL2591 Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/tsl2591-0.0.1-py2.7.egg/tsl2591/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install build-essential libssl-dev libffi-dev python-dev
	wget https://github.com/maxlklaxl/python-tsl2591/archive/master.zip
	unzip master.zip
	rm master.zip
	cd python-tsl2591-master
	python ./setup.py install
	cd ..
	rm -r python-tsl2591-master
	cd "$CWDIR"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="UInput Library"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/lib/python2.7/dist-packages/uinput/__init__.py ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install libudev-dev
	pip install python-uinput

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


# --------- Drivers --------
###########################################################################
P_TITLE="CM108 Audio Driver"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f "/home/pi/.asoundrc" ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	# wget https://raw.github.com/Hexxeh/rpi-update/master/rpi-update -O /usr/bin/rpi-update
	# chmod +x /usr/bin/rpi-update
	# yBRANCH=next rpi-update

	echo "pcm.!default	{" > /home/pi/.asoundrc
	echo "	type hw card 0" >>	/home/pi/.asoundrc
	echo "}" >>	/home/pi/.asoundrc
	echo "ctl.!default {" >>	/home/pi/.asoundrc
	echo "	type hw card 0" >>	/home/pi/.asoundrc
	echo "}" >>	/home/pi/.asoundrc

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


# -------- Programs --------
###########################################################################
P_TITLE="FBCP"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/local/bin/fbcp ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install cmake
	git clone https://github.com/tasanakorn/rpi-fbcp
	cd rpi-fbcp/
	mkdir build
	cd build/
	cmake ..
	make
	install fbcp /usr/local/bin/fbcp
	cd "$CWDIR"
	rm -r rpi-fbcp

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="GPSD Service"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/sbin/gpsd ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install gpsd gpsd-clients python-gps
	systemctl stop gpsd.socket
	systemctl disable gpsd.socket

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="MPG123 Audio Player"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f "/usr/bin/mpg123" ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get install mpg123

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="Remote Desktop Server"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/sbin/xrdp ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install xrdp

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="SQLite"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/bin/sqlite3 ] || [ "$P_UPDATE_ALL" == "y" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Installing ${P_TITLE}"
	echo "------------------------------------------------------"

	apt-get --assume-yes install sqlite3

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


# ------ Configuration -----
###########################################################################
P_TITLE="Disable avahi-daemon"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
systemctl disable avahi-daemon
systemctl disable org.freedesktop.Avahi


###########################################################################
P_TITLE="Auto Login Boot Option"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
L_LINK="/etc/systemd/system/getty.target.wants/getty@tty1.service"
L_FILE="/etc/systemd/system/autologin@.service"
if [ ! -L "${L_LINK}" ] || [ "$(readlink -- "${L_LINK}")" != "${L_FILE}" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	ln -fs /etc/systemd/system/autologin@.service /etc/systemd/system/getty.target.wants/getty@tty1.service

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset L_LINK
unset L_FILE


###########################################################################
P_TITLE="Buttons"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /etc/modules | egrep "^uinput")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	echo 'uinput' | sudo tee --append /etc/modules

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="Camera"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH1=$(cat /boot/config.txt | egrep "start_x=1")
MATCH2=$(cat /boot/config.txt | egrep "gpu_mem=128")
if [ -z "$MATCH1" ] || [ -z "$MATCH2" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	set_config_var start_x 1 /boot/config.txt
	CUR_GPU_MEM=$(get_config_var gpu_mem /boot/config.txt)
	if [ -z "$CUR_GPU_MEM" ] || [ "$CUR_GPU_MEM" -lt 128 ]; then
		set_config_var gpu_mem 128 /boot/config.txt
	fi
	sed /boot/config.txt -i -e "s/^startx/#startx/"
	sed /boot/config.txt -i -e "s/^fixup_file/#fixup_file/"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH1
unset MATCH2


###########################################################################
P_TITLE="Default Audio Device (USB)"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /etc/modprobe.d/alsa-base.conf ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	touch /etc/modprobe.d/alsa-base.conf
	echo "# This sets the index value of the cards but doesn't reorder." > /etc/modprobe.d/alsa-base.conf
	echo "options snd-usb-audio index=0" >> /etc/modprobe.d/alsa-base.conf
	echo "options snd-bcm2835 index=1" >> /etc/modprobe.d/alsa-base.conf
	echo "" >> /etc/modprobe.d/alsa-base.conf
	echo "# Does the reordering." >> /etc/modprobe.d/alsa-base.conf
	echo "options snd slots=snd-usb-audio,snd-bcm2835" >> /etc/modprobe.d/alsa-base.conf

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi


# Required before Default Locale
###########################################################################
P_TITLE="System Locales"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
SEARCH="^${P_LANG}_${P_COUNTRY^^}.UTF-8 UTF-8"
MATCH=$(cat /etc/locale.gen | egrep "${SEARCH}")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	sed -i "s/^# ${P_LANG}_${P_COUNTRY^^}.UTF-8 UTF-8/${P_LANG}_${P_COUNTRY^^}.UTF-8 UTF-8/g" /etc/locale.gen
	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH
unset SEARCH


###########################################################################
P_TITLE="Default Locale"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
SEARCH="^LANG=${P_LANG}_${P_COUNTRY^^}.UTF-8"
MATCH=$(cat /etc/default/locale | egrep "${SEARCH}")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	locale-gen "${P_LANG}_${P_COUNTRY^^}.UTF-8"
	update-locale LANG="${P_LANG}_${P_COUNTRY^^}.UTF-8"

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH
unset SEARCH


# https://github.com/notro/fbtft/wiki/Framebuffer-use
###########################################################################
P_TITLE="Display"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
echo
echo "------------------------------------------------------"
echo "${P_COUNTER} - Configuring ${P_TITLE}"
echo "------------------------------------------------------"

if [ ! -f /etc/modules-load.d/fbtft.conf ]; then
	echo "	- Modifying /etc/modules-load.d/fbtft.conf"

	touch /etc/modules-load.d/fbtft.conf
	echo "spi-bcm2835" > /etc/modules-load.d/fbtft.conf
	echo "fbtft_device" >> /etc/modules-load.d/fbtft.conf
	
	CHANGEMADE="Yes"
else
	echo "	- /etc/modules-load.d/fbtft.conf already modified"
fi

if [ ! -f /etc/modprobe.d/fbtft.conf ]; then
	echo "	- Modifying /etc/modprobe.d/fbtft.conf"

	touch /etc/modprobe.d/fbtft.conf
	echo "options fbtft_device name=adafruit22a debug=1 speed=8000000 rotate=270 txbuflen=16384" > /etc/modprobe.d/fbtft.conf

	CHANGEMADE="Yes"
else
	echo "	- /etc/modprobe.d/fbtft.conf already modified"
fi

MATCH=$(cat /boot/config.txt | egrep "^dtparam=spi=on")
if [ -z "$MATCH" ]; then
	echo "	- Modifying /boot/config.txt (Enable SPI)"

	set_config_var dtparam=spi on /boot/config.txt
	
	CHANGEMADE="Yes"
else
	echo "	- /boot/config.txt (Enable SPI) already modified"
fi
unset MATCH

echo
echo "Done"


###########################################################################
P_TITLE="Desktop on LCD"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/share/X11/xorg.conf.d/99-fbdev.conf ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	touch /usr/share/X11/xorg.conf.d/99-fbdev.conf
	echo 'Section "Device"' > /usr/share/X11/xorg.conf.d/99-fbdev.conf
	echo '	Identifier "myfb"' >> /usr/share/X11/xorg.conf.d/99-fbdev.conf
	echo '	Driver "fbdev"' >> /usr/share/X11/xorg.conf.d/99-fbdev.conf
	echo '	Option "fbdev" "/dev/fb1"' >> /usr/share/X11/xorg.conf.d/99-fbdev.conf
	echo 'EndSection' >> /usr/share/X11/xorg.conf.d/99-fbdev.conf

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi


###########################################################################
P_TITLE="Desktop Shortcuts"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /home/pi/Desktop/Tricorder.desktop ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	cp Tricorder.desktop /home/pi/Desktop/Tricorder.desktop
	cp lxterminal.desktop /home/pi/Desktop/lxterminal.desktop
	# Autostart UI
	cp Tricorder.desktop /etc/xdg/autostart/Tricorder.desktop

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi


###########################################################################
P_TITLE="Desktop Wallpaper"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /home/pi/.config/pcmanfm/LXDE-pi/desktop-items-0.conf | egrep "^wallpaper=/home/pi/TOSTricorder/backgrounds/science_logo.png")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	set_config_var wallpaper /home/pi/TOSTricorder/backgrounds/science_logo.png /home/pi/.config/pcmanfm/LXDE-pi/desktop-items-0.conf
	set_config_var autohide 1 /home/pi/.config/lxpanel/LXDE-pi/panels/panel

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="GPS"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /boot/cmdline.txt | egrep "dwc_otg.lpm_enable=0 console=tty1 ")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	# ORIGINAL CONTENTS: dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait
	echo 'dwc_otg.lpm_enable=0 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait' | sudo tee /boot/cmdline.txt

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="Hostname"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /etc/hostname | egrep "^${P_HOSTNAME}$")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

		# ORIGINAL: raspberrypi
	# Change the name in /etc/hostname to tricorder001
	echo $P_HOSTNAME > /etc/hostname
	echo "127.0.1.1	${P_HOSTNAME}" | sudo tee --append /etc/hosts

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="I2C (Enable)"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /boot/config.txt | egrep "^dtparam=i2c_arm=on")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	set_config_var dtparam=i2c_arm on /boot/config.txt

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="KeyBind to launch UI"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /home/pi/.config/openbox/lxde-pi-rc.xml | egrep "/usr/bin/python3 /home/pi/TOSTricorder/tricordergui.py")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	cp lxde-pi-rc.xml /home/pi/.config/openbox/lxde-pi-rc.xml

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="Keyboard Layout"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /etc/default/keyboard | egrep "^XKBLAYOUT=\"${P_COUNTRY}\"")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	sed -i "s/XKBLAYOUT=\".*\"/XKBLAYOUT=\"$P_COUNTRY\"/g" /etc/default/keyboard
	invoke-rc.d keyboard-setup start

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="Serial (Enable)"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /boot/config.txt | egrep "enable_uart=1")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	set_config_var enable_uart 1 /boot/config.txt
	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="Timezone"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH1=$(cat /etc/timezone | egrep "^${P_TZ}")
MATCH2=$(diff -a -q /etc/localtime "/usr/share/zoneinfo/${P_TZ}")
if [ -z "$MATCH1" ] || [ ! -z "$MATCH2" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	#TZE="${P_TZ//\//\\\/}"

	# sed -i 's/Etc\/UTC/${TZE}/g' /etc/timezone
	echo "${P_TZ}" >/etc/timezone
	cp /usr/share/zoneinfo/$P_TZ /etc/localtime

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="Tricorder UI (Autorun)"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /etc/xdg/lxsession/LXDE-pi/autostart | egrep "^@/usr/bin/python3 /home/pi/TOSTricorder/tricordergui.py")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	echo '@/usr/bin/python3 /home/pi/TOSTricorder/tricordergui.py' | sudo tee --append /etc/xdg/lxsession/LXDE-pi/autostart

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
P_TITLE="WiFi Country"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
MATCH=$(cat /etc/wpa_supplicant/wpa_supplicant.conf | egrep "country=${P_COUNTRY^^}")
if [ -z "$MATCH" ]; then
	echo
	echo "------------------------------------------------------"
	echo "${P_COUNTER} - Configuring ${P_TITLE}"
	echo "------------------------------------------------------"

	if [ -e /etc/wpa_supplicant/wpa_supplicant.conf ]; then
		if grep -q "^country=" /etc/wpa_supplicant/wpa_supplicant.conf ; then
			sed -i "s/^country=.*/country=${P_COUNTRY^^}/g" /etc/wpa_supplicant/wpa_supplicant.conf
		else
			sed -i "1i country=${P_COUNTRY^^}" /etc/wpa_supplicant/wpa_supplicant.conf
		fi
	else
		echo "country=${P_COUNTRY^^}" > /etc/wpa_supplicant/wpa_supplicant.conf
	fi

	echo
	echo "Done"
	CHANGEMADE="Yes"
else
	echo
	echo "${P_COUNTER} - ${P_TITLE} already configured"
fi
unset MATCH


###########################################################################
## Summary																 ##
###########################################################################
echo
echo "-------------------------------------------------------------"
if [ "${CHANGEMADE}" == "Yes" ]; then
	echo "You may need to reboot for changes to take effect."
else
	echo "No changes were made."
fi
echo "-------------------------------------------------------------"


./services.sh

