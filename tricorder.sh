#!/bin/sh

# systemd  service

start() {
	# Archive existing log file
  if [ -f /home/pi/TOSTricorder/tricorder.log ]; then
  	if [ -f /home/pi/TOSTricorder/tricorder.log.bak ]; then
  		rm /home/pi/TOSTricorder/tricorder.log.bak
  	fi
  	cp /home/pi/TOSTricorder/tricorder.log /home/pi/TOSTricorder/tricorder.log.bak
  	rm /home/pi/TOSTricorder/tricorder.log
  fi
  echo "Tricorder Service Starting"
	/usr/bin/python /home/pi/TOSTricorder/neopixels.py &
	/usr/bin/python3 /home/pi/TOSTricorder/dbwritesensordata.py &
	/usr/bin/python3 /home/pi/TOSTricorder/dataGps.py &
	/usr/bin/python /home/pi/TOSTricorder/dataAir.py &
	/usr/bin/python /home/pi/TOSTricorder/dataRadiation.py &
	/usr/bin/python /home/pi/TOSTricorder/dataLight.py &
	/usr/bin/python /home/pi/TOSTricorder/dataInertia.py &
	/usr/bin/python /home/pi/TOSTricorder/dataCamera.py &
	/usr/bin/python3 /home/pi/TOSTricorder/iotClient.py
  echo "Tricorder Service Started"
}

stop() {
  echo "Tricorder Service Stopping"
  kill $(pgrep tricorder.sh)
  echo "Tricorder Service Stopped"
}

case "$1" in
   start)
      start
   ;;
   stop)
      stop
   ;;
   restart)
      stop
      start
   ;;
   status)
      if [ pgrep tricorder.sh > 0 ]; then
      	echo "Tricorder Service is Running"
      else
      	echo "Tricorder Service is Stopped"
      fi
   ;;
   *)
      echo "Usage: $0 {start|stop|restart|status}"
esac