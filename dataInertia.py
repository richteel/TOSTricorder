#!/usr/bin/env python

import signal
import sys
import time
import datetime as dt
import os
import Adafruit_LSM303
from L3GD20.L3GD20 import L3GD20
# import L3GD20.L3GD20

# Feeds
#    Air-Pressure       0x77    BMP180
#    Air-Temperature1   0x77
#    Air-Temperature2   0x44    SHT31-D
#    Air-Humidity       0x44

programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
scriptName = __file__
wait_in_seconds_good_read = 60 * 5
wait_in_seconds_bad_read = 60 * 1
msg_format_accel_x = "Accelerometer-X {}\n"  # I2C 0x1E
msg_format_accel_y = "Accelerometer-Y {}\n"  # I2C 0x1E
msg_format_accel_z = "Accelerometer-Z {}\n"  # I2C 0x1E
msg_format_compass_x = "Compass-X {}\n"  # I2C 0x19
msg_format_compass_y = "Compass-Y {}\n"  # I2C 0x19
msg_format_compass_z = "Compass-Z {}\n"  # I2C 0x19
msg_format_gyro_x = "Gyroscope-X {}\n"  # I2C 0x6B
msg_format_gyro_y = "Gyroscope-Y {}\n"  # I2C 0x6B
msg_format_gyro_z = "Gyroscope-Z {}\n"  # I2C 0x6B

verbose = 1


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def write2fifo(msg):
    global pipePath
    if not os.path.exists(pipePath):
        return

    with open(pipePath, 'w') as f:
        f.write(msg)
        f.close()


def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


if __name__ == '__main__':
    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    write_log("INFORMATION", "Started")
    lsm303 = Adafruit_LSM303.LSM303()
    s = L3GD20(busId=1, slaveAddr=0x6b, ifLog=False, ifWriteBlock=False)
    # Preconfiguration
    s.Set_PowerMode("Normal")
    s.Set_FullScale_Value("250dps")
    s.Set_AxisX_Enabled(True)
    s.Set_AxisY_Enabled(True)
    s.Set_AxisZ_Enabled(True)

    # Print current configuration
    s.Init()
    s.Calibrate()

    # Calculate angle
    dt = 0.02
    x = 0
    y = 0
    z = 0

    while True:
        if killer.kill_now:
            break

        wait_time = wait_in_seconds_bad_read

        # Read the X, Y, Z axis acceleration values and print them.
        accel, mag = lsm303.read()
        # Grab the X, Y, Z components from the reading and print them out.
        accel_x, accel_y, accel_z = accel
        mag_x, mag_z, mag_y = mag
        # print('Accel X={0}, Accel Y={1}, Accel Z={2}, Mag X={3}, Mag Y={4}, Mag Z={5}'.format(
        #     accel_x, accel_y, accel_z, mag_x, mag_y, mag_z))
        write2fifo(msg_format_accel_x.format(accel_x))
        write2fifo(msg_format_accel_y.format(accel_y))
        write2fifo(msg_format_accel_z.format(accel_z))
        write2fifo(msg_format_compass_x.format(mag_x))
        write2fifo(msg_format_compass_y.format(mag_y))
        write2fifo(msg_format_compass_z.format(mag_z))

        dxyz = s.Get_CalOut_Value()
        x += dxyz[0] * dt;
        y += dxyz[1] * dt;
        z += dxyz[2] * dt;
        write2fifo(msg_format_gyro_x.format(x))
        write2fifo(msg_format_gyro_y.format(y))
        write2fifo(msg_format_gyro_z.format(z))

        """
        if read_bmp180():
            wait_time = wait_in_seconds_good_read
            write_log("DEBUG", "Wrote BMP180 Data to FIFO", True)
        else:
            write_log("WARNING", "Failed to obtain data from BMP180")
        """

        time.sleep(wait_time)  # set to wait time in seconds

write_log("INFORMATION", "Ended")
