#!/usr/bin/env python

import signal
import sys
import time
import datetime as dt
import os
from PiPocketGeiger import RadiationWatch

# import subprocess

# Feeds
#    Air-Pressure       0x77    BMP180
#    Air-Temperature1   0x77
#    Air-Temperature2   0x44    SHT31-D
#    Air-Humidity       0x44

programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
pipePathGeiger = "/tmp/geiger.fifo"
scriptName = __file__
wait_in_seconds_good_read = 60 * 5
wait_in_seconds_bad_read = 60 * 1
msg_format_cpm = "Radiation-Counts-per-minute {}\n"
msg_format_uSvh = "Radiation-Dose {}\n"
msg_format_uSvhError = "Radiation-Dose-Error {}\n"

verbose = 1


def onNoise():
    print("Vibration! Stop moving!")

    global pipePathGeiger
    if not os.path.exists(pipePathGeiger):
        return

    with open(pipePathGeiger, 'w') as f:
        f.write("N\n")
        f.close()


def onRadiation():
    print("Ray appeared!")
    # Play a classic geiger click sound.
    # mpg123 /home/pi/TOSTricorder/click.wav
    # subprocess.Popen(['aplay', '-D', 'sysdefault', '/home/pi/TOSTricorder/click.wav'])
    global pipePathGeiger
    if not os.path.exists(pipePathGeiger):
        return

    with open(pipePathGeiger, 'w') as f:
        f.write("R\n")
        f.close()


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def write2fifo(msg):
    global pipePath
    if not os.path.exists(pipePath):
        return

    with open(pipePath, 'w') as f:
        f.write(msg)
        f.close()


def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


if __name__ == '__main__':
    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    write_log("INFORMATION", "Started")
    with RadiationWatch(22, 23) as radiationWatch:
        radiationWatch.registerRadiationCallback(onRadiation)
        radiationWatch.registerNoiseCallback(onNoise)
        while True:
            if killer.kill_now:
                break

            wait_time = wait_in_seconds_bad_read

            readings = radiationWatch.status()

            write2fifo(msg_format_cpm.format(readings["cpm"]))
            write2fifo(msg_format_uSvh.format(readings["uSvh"]))
            write2fifo(msg_format_uSvhError.format(readings["uSvhError"]))

            """
            if read_bmp180():
                wait_time = wait_in_seconds_good_read
                write_log("DEBUG", "Wrote BMP180 Data to FIFO", True)
            else:
                write_log("WARNING", "Failed to obtain data from BMP180")
            """

            time.sleep(wait_time)  # set to wait time in seconds

write_log("INFORMATION", "Ended")
