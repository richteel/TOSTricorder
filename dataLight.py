#!/usr/bin/env python

import signal
import sys
import time
import datetime as dt
import os
import tsl2591
import smbus

# Feeds
#    Light-IR           0x29        TSL2591
#    Light-Visible      0x29
#    Light-UV_Index     0x38 & 0x39

programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
scriptName = __file__
wait_in_seconds_good_read = 60 * 5
wait_in_seconds_bad_read = 60 * 1
msg_format_ir = "Light-IR {}\n"
msg_format_visible = "Light-Visible {}\n"
msg_format_uv_idx = "Light-UV_Index {}\n"

verbose = 1


def readUv():
    bus = smbus.SMBus(1)
    bus.write_byte(0x38, 0x02)
    time.sleep(0.5)
    data0 = bus.read_byte_data(0x38, 0x73)
    data1 = bus.read_byte_data(0x38, 0x71)
    uvlight = data0 * 256 + data1
    return uvlight


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def write2fifo(msg):
    global pipePath
    if not os.path.exists(pipePath):
        return

    with open(pipePath, 'w') as f:
        f.write(msg)
        f.close()


def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


if __name__ == '__main__':
    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    write_log("INFORMATION", "Started")
    tsl = tsl2591.Tsl2591()  # initialize
    while True:
        if killer.kill_now:
            break

        wait_time = wait_in_seconds_bad_read

        full, ir = tsl.get_full_luminosity()  # read raw values (full spectrum and ir spectrum)
        lux = tsl.calculate_lux(full, ir)  # convert raw values to lux

        write2fifo(msg_format_ir.format(ir))
        write2fifo(msg_format_visible.format(lux - ir))
        write2fifo(msg_format_uv_idx.format(readUv()))

        """
        if read_bmp180():
            wait_time = wait_in_seconds_good_read
            write_log("DEBUG", "Wrote BMP180 Data to FIFO", True)
        else:
            write_log("WARNING", "Failed to obtain data from BMP180")
        """

        time.sleep(wait_time)  # set to wait time in seconds

write_log("INFORMATION", "Ended")
