#!/usr/bin/env python3

# Import Adafruit IO REST client.
from Adafruit_IO import Client
from Adafruit_IO import MQTTClient
import configparser
from optparse import OptionParser
import signal
import datetime as dt
import sys
import os
import time
import sqlite3
import socket
import base64
from PIL import Image

# *** GLOBAL VARIABLES ***
# Set to your Adafruit IO key.
ADAFRUIT_IO_KEY = ""
ADAFRUIT_IO_USERNAME = ""
scriptName = __file__
globalLogFile = "/home/pi/thl_log.txt"
programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
databaseFile = "/home/pi/TOSTricorder/tricorder.db"
configFile = "/home/pi/TOSTricorder/iot.config"
pipePath = "/tmp/dbwritesensordata.fifo"
photosPath = "/home/pi/TOSTricorder/photos/"
verbose = 1
feeds = []
sqlGetDataFeed = """SELECT FD.FeedDataId, FD.Value, FD.Created, FD.Latitude, FD.Longitude, FD.Elevation, FDS.Sent, ITS.DisplayName
	FROM Feeds F
	INNER JOIN FeedData FD
	ON F.FeedId=FD.FeedId
	LEFT OUTER JOIN FeedDataIotSite FDS
	ON FD.FeedDataId=FDS.FeedDataId
	LEFT OUTER JOIN IotSites ITS
	ON FDS.IotSiteId=ITS.IotSiteId
	WHERE F.DisplayName=?
	ORDER BY FD.Created DESC, FDS.Sent DESC  LIMIT 1;"""


def CreateThumbNail(imgFile):
    global photosPath

    l = len(imgFile) - len(photosPath)
    k = imgFile[-l:]
    retVal = photosPath + 'thumb_' + k

    print(imgFile)

    with open(imgFile, 'rb') as fd_img:
        img = Image.open(fd_img)
        # size = 320, 240
        size = 365, 205
        img.thumbnail(size)
        img.save(retVal, img.format)
        fd_img.close()

    return retVal


def img2Base64(imgFile):
    retVal = None

    with open(imgFile, "rb") as f:
        retVal = base64.b64encode(f.read()).decode()

    return retVal


def SendData2AdafruitIO(dict):
    global aio

    if not check_internet():
        write_log("WARNING", "No internet connection. Data not sent to Adafruit.IO", True)
        return False

    feedId = dict["feedName"].lower()

    fifoMsg = 'iot adafruit {}\n'.format(dict['feedDataId'])

    if not write2fifo(fifoMsg):
        return False

    try:
        str = dict['feedDataVal']

        if dict['feedName'] == 'Camera-Image':
            thumbFile = CreateThumbNail(dict['feedDataVal'])
            # str = img2Base64(dict['feedDataVal'])
            str = img2Base64(thumbFile)

        if dict['lat'] is None or dict['lng'] is None or dict['elv'] is None:
            aio.send(feedId, str)
        else:
            aio.sendLoc(feedId, str, dict['lat'], dict['lng'], dict['elv'])
    except Exception as ex:
        write_log("ERROR", "Failed to send data to Adafruit.IO {} ({})".format(dict, ex), True)

    return True


def UpdateIotFeeds():
    conn = sqlite3.connect('file:' + databaseFile + '?mode=ro', uri=True, timeout=10)
    try:
        c = conn.cursor()

        for feed in feeds:
            if len(feed) == 0:
                continue

            sqlParms = (feed,)
            c.execute(sqlGetDataFeed, sqlParms)
            all_rows = c.fetchall()
            feedData = {}

            feedData['feedName'] = feed
            feedData['feedDataId'] = all_rows[0][0]
            feedData['feedDataVal'] = all_rows[0][1]
            feedData['feedDataDt'] = all_rows[0][2]
            feedData['lat'] = all_rows[0][3]
            feedData['lng'] = all_rows[0][4]
            feedData['elv'] = all_rows[0][5]
            feedData['sent'] = all_rows[0][6]
            feedData['iotSite'] = all_rows[0][7]

            if feedData['sent'] is None or feedData['iotSite'] is None:
                SendData2AdafruitIO(feedData)
    except Exception as ex:
        write_log("ERROR", "Failed to connect to database ({})".format(ex), True)

    conn.close()


def write2fifo(msg):
    global pipePath
    if not os.path.exists(pipePath):
        return False

    with open(pipePath, 'w') as f:
        f.write(msg)
        f.close()

    return True


def check_internet(host="8.8.8.8", port=53, timeout=3):
    """
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except Exception as ex:
        return False


# Configuration functions
# ---------------------------------------------------------
def CreateSectionIfNotExist(section):
    if not Config.has_section(section):
        Config.add_section(section)


def GetOptionCreateIfNotExist(section, option, defaultvalue):
    global Config

    CreateSectionIfNotExist(section)
    # if not Config.has_option(section, option):
    #     Config.add_option(section, option, defaultvalue)

    return Config.get(section, option)


def LoadProgramConfiguration():
    global ADAFRUIT_IO_KEY, feeds

    ADAFRUIT_IO_KEY = GetOptionCreateIfNotExist("AdafruitIO", "AdafruitIoKey", "")
    ADAFRUIT_IO_USERNAME = GetOptionCreateIfNotExist("AdafruitIO", "UserName", "")

    for i in range(0, 10):
        feeds.append(GetOptionCreateIfNotExist("Feeds", "Feed" + str(i), ""))


# Standard set of functions in Tricorder
# ---------------------------------------------------------
def timestamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.datetime.now().strftime(fmt)[:-3]


def write_log(message_type, message, echo_message=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and message_type.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        write_log_header(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message) + "\n")
        f.close()

    if echo_message:
        print(fmt.format(s1=timestamp(), s2=scriptName, s3=message_type, s4=message))


def write_log_header(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


# **********************************************************
# ***             Main program logic follows             ***
# **********************************************************
if __name__ == '__main__':
    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    killer = GracefulKiller()

    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    write_log("INFORMATION", "Started")

    Config = configparser.ConfigParser()
    Config.read(configFile)
    LoadProgramConfiguration()

    aio = Client(ADAFRUIT_IO_KEY)
    # mqtt = MQTTClient(ADAFRUIT_IO_USERNAME, ADAFRUIT_IO_KEY)
    # mqtt.connect()
    # mqtt.loop_background()

    while True:
        if killer.kill_now:
            break

        UpdateIotFeeds()

        time.sleep(60)
