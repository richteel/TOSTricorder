#!/usr/bin/env python3

import signal
import sys
import os
import os.path
import time
import datetime
import sqlite3
import socket
import ntplib


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


programLogFile = "/home/pi/TOSTricorder/tricorder.log"
pipePath = "/tmp/dbwritesensordata.fifo"
databaseFile = "/home/pi/TOSTricorder/tricorder.db"
create_database_script = "/home/pi/TOSTricorder/tricorder.sql"
scriptName = __file__
sqlTemplateGps = "INSERT INTO [LocationData] ([DateTime_UTC], [DateTime_Local], [Latitude], [Longitude], [Elevation]) VALUES(?, ?, ?, ?, ?);"
sqlTemplateData = "INSERT INTO [FeedData] ([FeedId], [Value]) SELECT [Feeds].[FeedId], ? FROM [Feeds] WHERE [Feeds].[DisplayName]=?;"
sqlTemplateDataGps = "INSERT INTO [FeedData] ([FeedId], [Value], [Latitude], [Longitude], [Elevation]) SELECT [Feeds].[FeedId], ?, ?, ?, ? FROM [Feeds] WHERE [Feeds].[DisplayName]=?;"
sqlTemplateUpdateIot = '''INSERT INTO FeedDataIotSite (FeedDataId, IotSiteId, Sent)
    SELECT ?, IOT.IotSiteId, CURRENT_TIMESTAMP
    FROM IotSites IOT
    WHERE IOT.DisplayName = ?;'''

# Global Settings
updateMS = 250
gpsTimeFormat = '%Y-%m-%dT%H:%M:%S.%fZ'
nullTimeISO = "1970-01-01T00:00:00.000Z"
gpsMaxAgeMinutes = 15
gpsDataExpiresAt = datetime.datetime.now()
verbose = 1
time_valid = False

# Last GPS Information
gps_datetime_utc = datetime.datetime.strptime(nullTimeISO, gpsTimeFormat)
gps_datetime_local = datetime.datetime.strptime(nullTimeISO, gpsTimeFormat)
Latitude = 39.8333333
Longitude = -98.585522
Elevation = 578.495


# **********************************************************
# ***                     FUNCTIONS                      ***
# **********************************************************
def archiveLog(filename):
    if os.path.isfile(filename):
        os.rename(filename, filename + ".bak")


def check_internet(host="8.8.8.8", port=53, timeout=3):
    """
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except Exception as ex:
        return False


def create_database(db_filename, script_filename):
    qry = open(script_filename, 'r').read()
    conn = sqlite3.connect(db_filename)
    c = conn.cursor()
    c.executescript(qry)
    conn.commit()
    c.close()
    conn.close()
    # Allow other users to read/write the database
    os.chmod(db_filename, 0o666)


def get_internet_time():
    try:
        c = ntplib.NTPClient()
        response = c.request('time.nist.gov', version=3)
        internet_time = datetime.datetime.fromtimestamp(response.tx_time)
        return internet_time
    except Exception as e:
        writeLog("DEBUG", "Error while attempting to get intenet time: ({0})".format(e), True)
        return datetime.datetime.strptime(nullTimeISO, gpsTimeFormat)


def get_date_diff_seconds(first_datetime, second_datetime):
    return abs((first_datetime.replace(tzinfo=None) - second_datetime.replace(tzinfo=None)).total_seconds())


def get_uptime_secs():
    uptime_seconds = 0.0
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
    return uptime_seconds


def set_system_time(the_time):
    global time_valid
    current_system_time = datetime.datetime.now()
    current_system_time_utc = current_system_time.replace(tzinfo=None)
    the_time_utc = the_time.replace(tzinfo=None)
    time_delta_secs = get_date_diff_seconds(current_system_time_utc, the_time_utc)
    if time_delta_secs > 60:
        os.system('date --set="%s"' % the_time)
        writeLog("INFORMATION", "The system clock was off by {} seconds".format(time_delta_secs), True)
        writeLog("INFORMATION", "The system clock changed from {} to {}".format(current_system_time, the_time), True)

    time_valid = True


def isValidDateTime(dataValue):
    global gpsTimeFormat
    try:
        # GPS TIME: 2013-01-24T10:39:27.000Z
        # See: http://stackoverflow.com/questions/29367252/strptime-format-and-saving-the-milliseconds
        q = datetime.datetime.strptime(dataValue, gpsTimeFormat)
        return True
    except ValueError:
        return False


def isValidFloat(dataValue):
    try:
        q = float(dataValue)
        return True
    except ValueError:
        return False


def isValidInt(dataValue):
    try:
        q = int(dataValue)
        return True
    except ValueError:
        return False


def isValidIntRange(dataValue, minVal=0, maxVal=255):
    try:
        q = int(dataValue)
        if minVal <= q <= maxVal:
            return True
        else:
            return False
    except ValueError:
        return False


def isValidPath(dataValue):
    return os.path.isfile(dataValue)


def processLine(line):
    global databaseFile, programLogFile, gps_datetime_utc, gps_datetime_local, Latitude, Longitude, Elevation, \
        gpsTimeFormat, sqlTemplateGps, sqlTemplateData, sqlTemplateDataGps, gpsDataExpiresAt, time_valid, verbose, updateMS

    data = line
    words = data.split(" ")
    err = False
    sql = ""
    sqlParms = None

    feedName = words[0].lower()

    if feedName == "verbose":
        if not len(words) == 2:
            err = True
        elif not isValidInt(words[1], 0, 1):
            err = True
        else:
            verbose = int(words[1])
    elif feedName == "updatems":
        if not len(words) == 2:
            err = True
        elif not isValidInt(words[1], 100, 5000):
            err = True
        else:
            updateMS = int(words[1])
    elif feedName == "gps":
        if not len(words) == 5:
            err = True
        elif not isValidDateTime(words[1]):
            err = True
        elif not isValidFloat(words[2]):
            err = True
        elif not isValidFloat(words[3]):
            err = True
        elif not isValidFloat(words[4]):
            err = True
        else:
            gps_datetime_utc = datetime.datetime.strptime(words[1], gpsTimeFormat)
            # time = time.replace(tzinfo=datetime.timezone.utc).astimezone()
            gps_datetime_local = gps_datetime_utc.replace(tzinfo=datetime.timezone.utc).astimezone()
            Latitude = words[2]
            Longitude = words[3]
            Elevation = words[4]
            sql = sqlTemplateGps
            sqlParms = (gps_datetime_utc, gps_datetime_local, Latitude, Longitude, Elevation)
            gpsDataExpiresAt = gps_datetime_utc + datetime.timedelta(minutes=gpsMaxAgeMinutes)

            time_delta_secs = get_date_diff_seconds(datetime.datetime.now(), gps_datetime_local)

            if time_delta_secs > 60:
                writeLog("WARNING", "System Time appears to be off by at least {} seconds".format(time_delta_secs),
                         True)
                time_valid = False

    elif feedName == "camera-image":
        if not len(words) == 2:
            err = True
        elif not isValidPath(words[1]):
            err = True
        else:
            # Add GPS Data if recent
            if datetime.datetime.now() > gpsDataExpiresAt:
                sql = sqlTemplateData
                sqlParms = (words[1], feedName)
            else:
                sql = sqlTemplateDataGps
                sqlParms = (words[1], Latitude, Longitude, Elevation, feedName)
    elif feedName == 'iot':
        if not len(words) == 3:
            err = True
        else:
            sql = sqlTemplateUpdateIot
            sqlParms = (words[2], words[1])
    elif len(words) == 2:
        if not isValidFloat(words[1]):
            err = True
        else:
            # Add GPS Data if recent
            if datetime.datetime.now() > gpsDataExpiresAt:
                sql = sqlTemplateData
                sqlParms = (words[1], feedName)
            else:
                sql = sqlTemplateDataGps
                sqlParms = (float(words[1]), Latitude, Longitude, Elevation, feedName)
    else:
        err = True

    if err:
        writeLog("WARNING", "Command not recognized, incorrect number of arguments, or arguments out of range: " + line,
                 True)
        writeLog("WARNING", "Message ({}) had {} words".format(feedName, len(words)), True)
    else:
        # Save data to the database
        writeLog("DEBUG", "Saving data to the database: " + line, True)
        conn = sqlite3.connect(databaseFile)
        c = conn.cursor()
        try:
            c.execute(sql, sqlParms)
            conn.commit()
        except sqlite3.Error as e:
            writeLog("ERROR", "Error saving data to the database (" + e.args[0] + ")", True)
        conn.close()


def readPipe(pipe):
    try:
        data = os.read(pipe, 1024)
        if not data:
            return

        data = data.strip()

        if len(data) == 0:
            return

        lines = data.decode("utf-8").split(os.linesep)

        for line in lines:
            writeLog("DEBUG", "Processing Line: {0}".format(line), True)
            processLine(line)
    except Exception as e:
        writeLog("DEBUG", "Error while attempting to read: ({0})".format(e), True)
        return


def timeStamp(fmt='%Y-%m-%d %H:%M:%S.%f'):
    return datetime.datetime.now().strftime(fmt)[:-3]


def writeLog(messageType, message, echoMessage=False):
    global verbose, programLogFile, scriptName
    if verbose == 0 and messageType.lower() == "debug":
        return

    fmt = "{s1:s}\t{s2:s}\t{s3:s}\t{s4:s}"

    if not os.path.isfile(programLogFile):
        writeLogFileHeader(programLogFile, 'DateTime\tScript\tMessageType\tMessage\n')
    with open(programLogFile, 'a') as f:
        f.write(fmt.format(s1=timeStamp(), s2=scriptName, s3=messageType, s4=message) + "\n")
        f.close()

    if echoMessage:
        print(fmt.format(s1=timeStamp(), s2=scriptName, s3=messageType, s4=message))


def writeLogFileHeader(filename, header):
    with open(filename, 'w') as f:
        f.write(header)
        f.close()


# **********************************************************
# ***             Main program logic follows             ***
# **********************************************************
if __name__ == '__main__':
    # Check that the user ran as sudo
    if not os.geteuid() == 0:
        sys.exit("Please run using sudo /usr/bin/python " + os.path.basename(__file__) + " (You may use 'sudo !!' now)")

    killer = GracefulKiller()
    pipe = -1

    # Archive the log file if it exists
    # archiveLog(programLogFile)

    if os.path.exists(pipePath):
        os.unlink(pipePath)

    if not os.path.exists(pipePath):
        os.mkfifo(pipePath)
        os.chmod(pipePath, 0o666)

    if not os.path.exists(databaseFile):
        create_database(databaseFile, create_database_script)

    time.sleep(1)
    if os.path.exists(pipePath):
        pipe = os.open(pipePath, os.O_RDONLY | os.O_NONBLOCK)

    nextUpdateTime = datetime.datetime.now()
    nextUpdateTime = nextUpdateTime + datetime.timedelta(milliseconds=updateMS)
    writeLog("INFORMATION", "Started")

    while True:
        if killer.kill_now:
            break

        # attempt to validate datetime
        no_time = datetime.datetime.strptime(nullTimeISO, gpsTimeFormat)
        the_time = datetime.datetime.strptime(nullTimeISO, gpsTimeFormat)
        if not time_valid and check_internet():
            the_time = get_internet_time()
            time_delta_secs = get_date_diff_seconds(the_time, no_time)
            if time_delta_secs > 946684800:
                set_system_time(the_time)

        time_delta_secs = get_date_diff_seconds(gps_datetime_local, no_time)
        if not time_valid and time_delta_secs > 946684800:
            set_system_time(gps_datetime_local)

        if datetime.datetime.now() > nextUpdateTime:
            if pipe <= 0 and os.path.exists(pipePath):
                pipe = os.open(pipePath, os.O_RDONLY | os.O_NONBLOCK)

            if pipe > 0:
                readPipe(pipe)

            nextUpdateTime = datetime.datetime.now()
            nextUpdateTime = nextUpdateTime + datetime.timedelta(milliseconds=updateMS)

        time.sleep(updateMS / 4000.0)  # set to wait time in seconds

if pipe > 0:
    os.close(pipe)

if os.path.exists(pipePath):
    os.unlink(pipePath)

writeLog("INFORMATION", "Ended")
