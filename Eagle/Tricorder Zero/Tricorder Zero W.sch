<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="Stiffner" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tBridges" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="tBPL" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="bBPL" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="MPL" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="Not-On-BRD" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="sName" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bPlace" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<description>Raspberry Pi Zero W Tricorder</description>
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="LETTER_L">
<frame x1="0" y1="0" x2="248.92" y2="185.42" columns="12" rows="17" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LETTER_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
LETTER landscape</description>
<gates>
<gate name="G$1" symbol="LETTER_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TeelSys-Tricorder">
<packages>
<package name="RAZPBERRYPIZERO1.3">
<pad name="P$1" x="-1.27" y="22.86" drill="0.9" shape="square"/>
<pad name="P$2" x="1.27" y="22.86" drill="0.9"/>
<pad name="P$3" x="-1.27" y="20.32" drill="0.9"/>
<pad name="P$4" x="1.27" y="20.32" drill="0.9"/>
<pad name="P$5" x="-1.27" y="17.78" drill="0.9"/>
<pad name="P$6" x="1.27" y="17.78" drill="0.9"/>
<pad name="P$7" x="-1.27" y="15.24" drill="0.9"/>
<pad name="P$8" x="1.27" y="15.24" drill="0.9"/>
<pad name="P$9" x="-1.27" y="12.7" drill="0.9"/>
<pad name="P$10" x="1.27" y="12.7" drill="0.9"/>
<pad name="P$11" x="-1.27" y="10.16" drill="0.9"/>
<pad name="P$12" x="1.27" y="10.16" drill="0.9"/>
<pad name="P$13" x="-1.27" y="7.62" drill="0.9"/>
<pad name="P$14" x="1.27" y="7.62" drill="0.9"/>
<pad name="P$15" x="-1.27" y="5.08" drill="0.9"/>
<pad name="P$16" x="1.27" y="5.08" drill="0.9"/>
<pad name="P$17" x="-1.27" y="2.54" drill="0.9"/>
<pad name="P$18" x="1.27" y="2.54" drill="0.9"/>
<pad name="P$19" x="-1.27" y="0" drill="0.9"/>
<pad name="P$20" x="1.27" y="0" drill="0.9"/>
<pad name="P$21" x="-1.27" y="-2.54" drill="0.9"/>
<pad name="P$22" x="1.27" y="-2.54" drill="0.9"/>
<pad name="P$23" x="-1.27" y="-5.08" drill="0.9"/>
<pad name="P$24" x="1.27" y="-5.08" drill="0.9"/>
<pad name="P$25" x="-1.27" y="-7.62" drill="0.9"/>
<pad name="P$26" x="1.27" y="-7.62" drill="0.9"/>
<pad name="P$27" x="-1.27" y="-10.16" drill="0.9"/>
<pad name="P$28" x="1.27" y="-10.16" drill="0.9"/>
<pad name="P$29" x="-1.27" y="-12.7" drill="0.9"/>
<pad name="P$30" x="1.27" y="-12.7" drill="0.9"/>
<pad name="P$31" x="-1.27" y="-15.24" drill="0.9"/>
<pad name="P$32" x="1.27" y="-15.24" drill="0.9"/>
<pad name="P$33" x="-1.27" y="-17.78" drill="0.9"/>
<pad name="P$34" x="1.27" y="-17.78" drill="0.9"/>
<pad name="P$35" x="-1.27" y="-20.32" drill="0.9"/>
<pad name="P$36" x="1.27" y="-20.32" drill="0.9"/>
<pad name="P$37" x="-1.27" y="-22.86" drill="0.9"/>
<pad name="P$38" x="1.27" y="-22.86" drill="0.9"/>
<pad name="P$39" x="-1.27" y="-25.4" drill="0.9"/>
<pad name="P$40" x="1.27" y="-25.4" drill="0.9"/>
<hole x="0" y="29" drill="2.75"/>
<wire x1="0" y1="32.5" x2="3.5" y2="29" width="0.127" layer="21" curve="-90"/>
<hole x="0" y="-29" drill="2.75"/>
<wire x1="3.5" y1="-29" x2="0" y2="-32.5" width="0.127" layer="21" curve="-90"/>
<wire x1="3.5" y1="29" x2="3.5" y2="-29" width="0.127" layer="21"/>
<wire x1="0" y1="-32.5" x2="-3" y2="-32.5" width="0.127" layer="21"/>
<wire x1="-3" y1="-32.5" x2="-20" y2="-32.5" width="0.127" layer="21"/>
<wire x1="-20" y1="-32.5" x2="-23.5" y2="-32.5" width="0.127" layer="21"/>
<wire x1="-26.5" y1="-29.5" x2="-26.5" y2="-25.5" width="0.127" layer="21"/>
<wire x1="-26.5" y1="-25.5" x2="-26.5" y2="-17.5" width="0.127" layer="21"/>
<wire x1="-26.5" y1="-17.5" x2="-26.5" y2="-12.9" width="0.127" layer="21"/>
<wire x1="-26.5" y1="-12.9" x2="-26.5" y2="-4.9" width="0.127" layer="21"/>
<wire x1="-26.5" y1="-4.9" x2="-26.5" y2="14.6" width="0.127" layer="21"/>
<wire x1="-26.5" y1="14.6" x2="-26.5" y2="25.6" width="0.127" layer="21"/>
<wire x1="-26.5" y1="25.6" x2="-26.5" y2="29" width="0.127" layer="21"/>
<wire x1="0" y1="32.5" x2="-3.6" y2="32.5" width="0.127" layer="21"/>
<hole x="-23" y="29" drill="2.75"/>
<wire x1="-3.6" y1="32.5" x2="-15.6" y2="32.5" width="0.127" layer="21"/>
<wire x1="-15.6" y1="32.5" x2="-23" y2="32.5" width="0.127" layer="21"/>
<wire x1="-26.5" y1="29" x2="-23" y2="32.5" width="0.127" layer="21" curve="-90"/>
<hole x="-23" y="-29" drill="2.75"/>
<wire x1="-23.5" y1="-32.5" x2="-26.5" y2="-29.5" width="0.127" layer="21" curve="-90"/>
<pad name="PP1" x="-20.5" y="-21.5" drill="1.1"/>
<pad name="PP6" x="-23.04" y="-21.5" drill="1.1"/>
<wire x1="-26.5" y1="-25.5" x2="-21.5" y2="-25.5" width="0.127" layer="21"/>
<wire x1="-21.5" y1="-25.5" x2="-21.5" y2="-17.5" width="0.127" layer="21"/>
<wire x1="-21.5" y1="-17.5" x2="-26.5" y2="-17.5" width="0.127" layer="21"/>
<pad name="PP22" x="-24.5" y="-10.17" drill="1.1"/>
<pad name="PP23" x="-24.5" y="-7.63" drill="1.1"/>
<wire x1="-26.5" y1="-12.9" x2="-21.5" y2="-12.9" width="0.127" layer="21"/>
<wire x1="-21.5" y1="-12.9" x2="-21.5" y2="-4.9" width="0.127" layer="21"/>
<wire x1="-21.5" y1="-4.9" x2="-26.5" y2="-4.9" width="0.127" layer="21"/>
<wire x1="-26.5" y1="14.6" x2="-18.5" y2="14.6" width="0.127" layer="21"/>
<wire x1="-18.5" y1="14.6" x2="-18.5" y2="25.6" width="0.127" layer="21"/>
<wire x1="-18.5" y1="25.6" x2="-26.5" y2="25.6" width="0.127" layer="21"/>
<wire x1="-15.6" y1="32.5" x2="-15.6" y2="19.5" width="0.127" layer="21"/>
<wire x1="-15.6" y1="19.5" x2="-3.6" y2="19.5" width="0.127" layer="21"/>
<wire x1="-3.6" y1="19.5" x2="-3.6" y2="32.5" width="0.127" layer="21"/>
<wire x1="-20" y1="-32.5" x2="-20" y2="-28.5" width="0.127" layer="21"/>
<wire x1="-20" y1="-28.5" x2="-3" y2="-28.5" width="0.127" layer="21"/>
<wire x1="-3" y1="-28.5" x2="-3" y2="-32.5" width="0.127" layer="21"/>
<text x="-11.43" y="1.27" size="1.27" layer="21" rot="SR270" align="center">adafruit 3400
Raspberry Pi Zero W</text>
<pad name="VID" x="-6.35" y="-22.86" drill="0.9" shape="square"/>
<pad name="RUN_GND" x="-3.81" y="-22.86" drill="0.9"/>
<pad name="VID_GND" x="-6.35" y="-25.4" drill="0.9"/>
<pad name="RUN" x="-3.81" y="-25.4" drill="0.9" shape="square"/>
<wire x1="-5.08" y1="-21.59" x2="-2.54" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-21.59" x2="-2.54" y2="-26.67" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-26.67" x2="-5.08" y2="-26.67" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-26.67" x2="-5.08" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-21.59" x2="-7.62" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-21.59" x2="-7.62" y2="-26.67" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-26.67" x2="-5.08" y2="-26.67" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-26.67" x2="2.54" y2="-26.67" width="0.127" layer="21"/>
<wire x1="2.54" y1="-26.67" x2="2.54" y2="24.13" width="0.127" layer="21"/>
<wire x1="2.54" y1="24.13" x2="-2.54" y2="24.13" width="0.127" layer="21"/>
<wire x1="-2.54" y1="24.13" x2="-2.54" y2="-21.59" width="0.127" layer="21"/>
<text x="-3.81" y="-20.32" size="1.27" layer="21" rot="SR270" align="center-right">RUN</text>
<text x="-6.35" y="-20.32" size="1.27" layer="21" rot="SR270" align="center-right">Video</text>
</package>
<package name="ZERO4U">
<hole x="0" y="29" drill="2.75"/>
<wire x1="0" y1="32.5" x2="3.5" y2="30.27" width="0.127" layer="21" curve="-90"/>
<hole x="0" y="-29" drill="2.75"/>
<wire x1="3.5" y1="-27.73" x2="0" y2="-32.5" width="0.127" layer="21" curve="-90"/>
<wire x1="3.5" y1="30.27" x2="3.5" y2="-27.73" width="0.127" layer="21"/>
<wire x1="0" y1="-32.5" x2="-23.5" y2="-32.5" width="0.127" layer="21"/>
<wire x1="-26.5" y1="-29.5" x2="-26.5" y2="4.7" width="0.127" layer="21"/>
<wire x1="-26.5" y1="17.4" x2="-26.5" y2="12.7" width="0.127" layer="21"/>
<wire x1="-26.5" y1="4.7" x2="-26.5" y2="12.7" width="0.127" layer="21"/>
<wire x1="-26.5" y1="17.4" x2="-26.5" y2="25.4" width="0.127" layer="21"/>
<wire x1="-26.5" y1="25.4" x2="-26.5" y2="29" width="0.127" layer="21"/>
<hole x="-23" y="29" drill="2.75"/>
<wire x1="0" y1="32.5" x2="-23" y2="32.5" width="0.127" layer="21"/>
<wire x1="-26.5" y1="29" x2="-23" y2="32.5" width="0.127" layer="21" curve="-90"/>
<hole x="-23" y="-29" drill="2.75"/>
<wire x1="-23.5" y1="-32.5" x2="-26.5" y2="-29.5" width="0.127" layer="21" curve="-90"/>
<wire x1="-26.5" y1="25.4" x2="-21.5" y2="25.4" width="0.127" layer="21"/>
<wire x1="-21.5" y1="25.4" x2="-21.5" y2="17.4" width="0.127" layer="21"/>
<wire x1="-21.5" y1="17.4" x2="-26.5" y2="17.4" width="0.127" layer="21"/>
<wire x1="-26.5" y1="12.7" x2="-21.5" y2="12.7" width="0.127" layer="21"/>
<wire x1="-21.5" y1="12.7" x2="-21.5" y2="4.7" width="0.127" layer="21"/>
<wire x1="-21.5" y1="4.7" x2="-26.5" y2="4.7" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-31.75" x2="-19.05" y2="-31.75" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-31.75" x2="-19.05" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-21.59" x2="-6.35" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-21.59" x2="-6.35" y2="-31.75" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="-7.62" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="8.89" width="0.127" layer="21"/>
<wire x1="-7.62" y1="8.89" x2="2.54" y2="8.89" width="0.127" layer="21"/>
<wire x1="2.54" y1="8.89" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="12.7" x2="-7.62" y2="12.7" width="0.127" layer="21"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="25.4" width="0.127" layer="21"/>
<wire x1="-7.62" y1="25.4" x2="2.54" y2="25.4" width="0.127" layer="21"/>
<wire x1="2.54" y1="25.4" x2="2.54" y2="12.7" width="0.127" layer="21"/>
<wire x1="2.54" y1="-20.32" x2="-7.62" y2="-20.32" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-20.32" x2="-7.62" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-7.62" x2="2.54" y2="-7.62" width="0.127" layer="21"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-20.32" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-7.62" x2="-19.05" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-7.62" x2="-19.05" y2="-15.24" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-15.24" x2="-25.4" y2="-15.24" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-15.24" x2="-25.4" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-17.78" y1="31.75" x2="-12.7" y2="31.75" width="0.127" layer="21"/>
<wire x1="-12.7" y1="31.75" x2="-12.7" y2="24.13" width="0.127" layer="21"/>
<wire x1="-12.7" y1="24.13" x2="-17.78" y2="24.13" width="0.127" layer="21"/>
<wire x1="-17.78" y1="24.13" x2="-17.78" y2="31.75" width="0.127" layer="21"/>
<smd name="USBGND" x="-23.04" y="21.5" dx="2.032" dy="2.032" layer="1" roundness="100" cream="no"/>
<smd name="USB+5V" x="-20.5" y="21.5" dx="2.032" dy="2.032" layer="1" roundness="100"/>
<smd name="USBD+" x="-24.5" y="10.17" dx="2.032" dy="2.032" layer="1" roundness="100"/>
<smd name="USBD-" x="-24.5" y="7.63" dx="2.032" dy="2.032" layer="1" roundness="100"/>
<text x="-12.7" y="3.81" size="1.27" layer="21" rot="SR270" align="center">adafruit 3298
Zero4U - 4 Port USB Hub
for Raspberry Pi Zero</text>
</package>
<package name="ADAFRUIT2465">
<wire x1="34.29" y1="22.86" x2="2.54" y2="22.86" width="0" layer="21"/>
<wire x1="2.54" y1="22.86" x2="0" y2="20.32" width="0" layer="21" curve="90"/>
<wire x1="0" y1="20.32" x2="0" y2="15.494" width="0" layer="21"/>
<wire x1="0" y1="15.494" x2="0.254" y2="15.24" width="0" layer="21"/>
<wire x1="0.254" y1="15.24" x2="0.254" y2="7.62" width="0" layer="21"/>
<wire x1="0.254" y1="7.62" x2="0" y2="7.366" width="0" layer="21"/>
<wire x1="0" y1="7.366" x2="0" y2="2.54" width="0" layer="21"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="0" layer="21" curve="90"/>
<wire x1="2.54" y1="0" x2="34.29" y2="0" width="0" layer="21"/>
<wire x1="34.29" y1="0" x2="36.068" y2="2.54" width="0" layer="21" curve="90"/>
<wire x1="36.068" y1="2.54" x2="36.068" y2="20.32" width="0" layer="21"/>
<wire x1="36.068" y1="20.32" x2="34.29" y2="22.86" width="0" layer="21" curve="90"/>
<text x="5.334" y="18.034" size="0.889" layer="21" ratio="12" rot="R180" align="center">LOW</text>
<text x="33.75" y="2.831" size="1.016" layer="21" ratio="12" rot="R180" align="center">PWR</text>
<text x="22.987" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">GND</text>
<text x="20.447" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">EN</text>
<text x="22.987" y="19.05" size="1.016" layer="21" ratio="12" align="center">adafruit 2465
PowerBoost 1000C</text>
<text x="6.604" y="2.032" size="1.016" layer="21" ratio="12">chrg</text>
<text x="15.367" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">LiPo Bat</text>
<text x="12.827" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">USB</text>
<text x="17.907" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">Vs</text>
<text x="12.573" y="16.002" size="1.27" layer="21" ratio="12">+</text>
<text x="7.874" y="16.002" size="1.27" layer="21" ratio="12">-</text>
<text x="30.607" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">5V</text>
<text x="28.067" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">GND</text>
<pad name="USB" x="12.827" y="1.27" drill="0.9"/>
<pad name="BAT" x="15.367" y="1.27" drill="0.9"/>
<pad name="VS" x="17.907" y="1.27" drill="0.9"/>
<pad name="EN" x="20.447" y="1.27" drill="0.9"/>
<pad name="GND" x="22.987" y="1.27" drill="0.9"/>
<pad name="LBO" x="25.527" y="1.27" drill="0.9"/>
<pad name="G" x="28.067" y="1.27" drill="0.9"/>
<pad name="5V" x="30.607" y="1.27" drill="0.9"/>
<wire x1="1.27" y1="13.97" x2="7.62" y2="13.97" width="0.127" layer="21"/>
<wire x1="7.62" y1="13.97" x2="7.62" y2="7.62" width="0.127" layer="21"/>
<wire x1="7.62" y1="7.62" x2="1.27" y2="7.62" width="0.127" layer="21"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="13.97" width="0.127" layer="21"/>
<wire x1="7.62" y1="21.59" x2="13.97" y2="21.59" width="0.127" layer="21"/>
<wire x1="13.97" y1="21.59" x2="13.97" y2="17.78" width="0.127" layer="21"/>
<wire x1="13.97" y1="17.78" x2="7.62" y2="17.78" width="0.127" layer="21"/>
<wire x1="7.62" y1="17.78" x2="7.62" y2="21.59" width="0.127" layer="21"/>
<hole x="2.54" y="20.32" drill="2.75"/>
<hole x="33.909" y="18.288" drill="2.75"/>
<hole x="2.54" y="2.54" drill="2.75"/>
<hole x="33.909" y="4.953" drill="2.75"/>
<pad name="OUT-" x="33.909" y="13.462" drill="0.9"/>
<pad name="OUT+" x="33.909" y="10.033" drill="0.9"/>
<text x="25.527" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">LBO</text>
<wire x1="5.08" y1="1.27" x2="6.35" y2="1.27" width="0.127" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="0.635" x2="5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="8.89" y1="1.27" x2="10.16" y2="1.27" width="0.127" layer="21"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="0.635" width="0.127" layer="21"/>
<wire x1="10.16" y1="0.635" x2="8.89" y2="0.635" width="0.127" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="1.27" width="0.127" layer="21"/>
<wire x1="33.02" y1="1.27" x2="34.29" y2="1.27" width="0.127" layer="21"/>
<wire x1="34.29" y1="1.27" x2="34.29" y2="0.635" width="0.127" layer="21"/>
<wire x1="34.29" y1="0.635" x2="33.02" y2="0.635" width="0.127" layer="21"/>
<wire x1="33.02" y1="0.635" x2="33.02" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="20.32" x2="5.08" y2="21.59" width="0.127" layer="21"/>
<wire x1="5.08" y1="21.59" x2="5.715" y2="21.59" width="0.127" layer="21"/>
<wire x1="5.715" y1="21.59" x2="5.715" y2="20.32" width="0.127" layer="21"/>
<wire x1="5.715" y1="20.32" x2="5.08" y2="20.32" width="0.127" layer="21"/>
</package>
<package name="LI_BATTERY">
<pad name="+" x="0" y="1.27" drill="0.9" shape="long"/>
<pad name="-" x="0" y="-1.27" drill="0.9" shape="long"/>
<text x="0" y="2.54" size="1.27" layer="21" align="bottom-center">+</text>
<text x="0" y="-3.81" size="1.27" layer="21" align="bottom-center">-</text>
</package>
<package name="ADAFRUIT1475">
<wire x1="0" y1="0" x2="28.8163" y2="0" width="0.127" layer="21"/>
<wire x1="28.8163" y1="0" x2="28.8163" y2="17.3228" width="0.127" layer="21"/>
<wire x1="28.8163" y1="17.3228" x2="0" y2="17.3228" width="0.127" layer="21"/>
<wire x1="0" y1="17.3228" x2="0" y2="0" width="0.127" layer="21"/>
<pad name="5V" x="0.9652" y="5.08" drill="0.8" shape="offset"/>
<pad name="D-" x="0.9652" y="7.62" drill="0.8" shape="offset"/>
<pad name="D+" x="0.9652" y="10.16" drill="0.8" shape="offset"/>
<pad name="GND" x="0.9652" y="12.7" drill="0.8" shape="offset"/>
<pad name="SPK_L1" x="20.32" y="16.51" drill="0.8"/>
<pad name="SPK_GND1" x="27.432" y="16.51" drill="0.8"/>
<pad name="SPK_L2" x="20.32" y="10.922" drill="0.8"/>
<pad name="SPK_R" x="23.876" y="10.922" drill="0.8"/>
<pad name="SPK_GND2" x="27.432" y="10.922" drill="0.8"/>
<pad name="MIC_L1" x="20.32" y="6.4008" drill="0.8"/>
<pad name="MIC_GND1" x="27.432" y="6.4008" drill="0.8"/>
<pad name="MIC_L2" x="20.32" y="0.8128" drill="0.8"/>
<pad name="MIC_R" x="23.876" y="0.8128" drill="0.8"/>
<pad name="MIC_GND2" x="27.432" y="0.8128" drill="0.8"/>
<text x="12.7" y="7.62" size="1.27" layer="21" align="center">adafruit 1475
USB Audio</text>
<text x="20.32" y="8.89" size="1.27" layer="21" align="center">L</text>
<text x="23.876" y="8.89" size="1.27" layer="21" align="center">R</text>
<text x="27.432" y="8.89" size="1.27" layer="21" align="center">G</text>
<text x="24.13" y="13.97" size="1.27" layer="21" align="center">SPK</text>
<text x="24.13" y="3.81" size="1.27" layer="21" align="center">MIC</text>
<text x="2.54" y="2.54" size="1.27" layer="21">5V</text>
<text x="2.54" y="13.97" size="1.27" layer="21">GND</text>
<text x="3.81" y="10.16" size="1.27" layer="21">D+</text>
<text x="3.81" y="6.35" size="1.27" layer="21">D-</text>
</package>
<package name="ADAFRUIT2716">
<wire x1="0" y1="0" x2="0" y2="13.208" width="0.127" layer="21"/>
<wire x1="13.97" y1="13.208" x2="13.97" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="15.748" x2="11.43" y2="15.748" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="13.97" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="13.208" x2="2.54" y2="15.748" width="0.127" layer="21" curve="-90"/>
<wire x1="11.43" y1="15.748" x2="13.97" y2="13.208" width="0.127" layer="21" curve="-90"/>
<hole x="6.985" y="12.7" drill="2.75"/>
<pad name="GND" x="6.985" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="3V3" x="4.445" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="1.905" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="AC" x="9.525" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="DC" x="12.065" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="6.985" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="4.445" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">3V3</text>
<text x="1.905" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">Vin</text>
<text x="9.525" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">AC</text>
<text x="12.065" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">DC</text>
<text x="6.985" y="9.525" size="0.8128" layer="21" align="center">adafruit 2716
MEMS Microphone</text>
</package>
<package name="RADIATION_WATCH_TYPE5">
<pad name="NS" x="12.7" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="GND2" x="10.16" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SIG" x="7.62" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="GND" x="5.08" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="+V" x="2.54" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="2.54" y="5.08" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">+V</text>
<text x="5.08" y="5.08" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="7.62" y="5.08" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SIG</text>
<text x="10.16" y="5.08" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="12.7" y="5.08" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">NS</text>
<text x="12.7" y="36.83" size="1.27" layer="21" rot="R90" align="center">Radiation Watch
Type 5</text>
<wire x1="0" y1="0" x2="25.4" y2="0" width="0.127" layer="21"/>
<wire x1="25.4" y1="0" x2="25.4" y2="17.78" width="0.127" layer="21"/>
<wire x1="25.4" y1="17.78" x2="25.4" y2="55.88" width="0.127" layer="21"/>
<wire x1="25.4" y1="55.88" x2="0" y2="55.88" width="0.127" layer="21"/>
<wire x1="0" y1="55.88" x2="0" y2="17.78" width="0.127" layer="21"/>
<wire x1="0" y1="17.78" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="16.51" width="0.127" layer="21"/>
<wire x1="15.24" y1="16.51" x2="21.336" y2="16.51" width="0.127" layer="21"/>
<wire x1="21.336" y1="16.51" x2="21.336" y2="1.27" width="0.127" layer="21"/>
<wire x1="21.336" y1="1.27" x2="15.24" y2="1.27" width="0.127" layer="21"/>
<wire x1="0" y1="17.78" x2="25.4" y2="17.78" width="0.127" layer="21"/>
<hole x="12.7" y="50.8" drill="2.75"/>
<hole x="12.7" y="25.4" drill="2.75"/>
</package>
<package name="SPARKFUN_13670">
<pad name="GND" x="-6.35" y="0" drill="0.9" shape="long" rot="R90"/>
<pad name="RXA" x="1.27" y="0" drill="0.9" shape="long" rot="R90"/>
<pad name="VCC" x="-3.81" y="0" drill="0.9" shape="long" rot="R90"/>
<pad name="TXA" x="-1.27" y="0" drill="0.9" shape="long" rot="R90"/>
<pad name="V_BAT" x="3.81" y="0" drill="0.9" shape="long" rot="R90"/>
<text x="-6.35" y="1.905" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="1.27" y="1.905" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">RXA</text>
<text x="-3.81" y="1.905" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">VCC</text>
<text x="-1.27" y="1.905" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">TXA</text>
<text x="3.81" y="1.905" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">V_BAT</text>
<text x="0" y="-3.175" size="0.8128" layer="21" align="center">SparkFun 13670
GPS Receiver - GP-735</text>
<pad name="PWR_CTRL" x="6.35" y="0" drill="0.9" shape="long" rot="R90"/>
<text x="6.35" y="1.905" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">PWR_CTRL</text>
<text x="-8.89" y="-1.27" size="1.27" layer="21">1</text>
</package>
<package name="ADAFRUIT746">
<wire x1="0" y1="2.54" x2="0" y2="30.369" width="0" layer="21"/>
<wire x1="25.4" y1="30.369" x2="25.4" y2="2.54" width="0" layer="21"/>
<wire x1="0" y1="30.369" x2="2.54" y2="33.925" width="0" layer="21" curve="-90"/>
<wire x1="2.54" y1="33.925" x2="22.86" y2="33.925" width="0" layer="21"/>
<wire x1="22.86" y1="33.925" x2="25.4" y2="30.369" width="0" layer="21" curve="-90"/>
<wire x1="22.86" y1="0" x2="2.54" y2="0" width="0" layer="21"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0" layer="21" curve="-90"/>
<wire x1="25.4" y1="2.54" x2="22.86" y2="0" width="0" layer="21" curve="-90"/>
<hole x="2.54" y="31.385" drill="2.75"/>
<pad name="GND" x="17.78" y="2.032" drill="0.9" shape="long" rot="R90"/>
<pad name="3V3" x="2.54" y="2.032" drill="0.9" shape="long" rot="R90"/>
<pad name="EN" x="5.08" y="2.032" drill="0.9" shape="long" rot="R90"/>
<pad name="FIX" x="10.16" y="2.032" drill="0.9" shape="long" rot="R90"/>
<pad name="TX" x="12.7" y="2.032" drill="0.9" shape="long" rot="R90"/>
<text x="17.78" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="2.54" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">3V3</text>
<text x="20.32" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">VIN</text>
<text x="5.08" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">EN</text>
<text x="12.7" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">TX</text>
<text x="12.065" y="28.575" size="0.8128" layer="21" align="center">adafruit 746
Ultimate GPS</text>
<pad name="VBAT" x="7.62" y="2.032" drill="0.9" shape="long" rot="R90"/>
<pad name="RX" x="15.24" y="2.032" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="20.32" y="2.032" drill="0.9" shape="long" rot="R90"/>
<pad name="PPS" x="22.86" y="2.032" drill="0.9" shape="long" rot="R90"/>
<text x="7.62" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">VBAT</text>
<text x="10.16" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">FIX</text>
<text x="15.24" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">RX</text>
<text x="22.86" y="3.81" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">PPS</text>
<hole x="22.86" y="31.385" drill="2.75"/>
</package>
<package name="ADAFRUIT1714">
<text x="1.038" y="5.543" size="1.778" layer="21" ratio="16" align="center">X</text>
<text x="5.038" y="5.643" size="1.778" layer="21" ratio="16" align="center">Y</text>
<text x="8.938" y="5.643" size="1.778" layer="21" ratio="16" align="center">Z</text>
<text x="-11.43" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">VIN</text>
<text x="-8.89" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">3Vo</text>
<text x="-6.35" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">GND</text>
<text x="-3.81" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">SCL</text>
<text x="-1.27" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">SDA</text>
<text x="1.27" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">GINT</text>
<text x="3.81" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">GRDY</text>
<text x="6.35" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">LIN1</text>
<text x="8.89" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">LIN2</text>
<text x="11.43" y="-6.985" size="1.016" layer="21" ratio="16" rot="R90" align="center-left">LRDY</text>
<text x="-11.43" y="1.27" size="1.016" layer="21" ratio="16" align="center-left">GIN = Gyro Ints</text>
<text x="-11.43" y="0" size="1.016" layer="21" ratio="16" align="center-left">LIN = Accel Ints</text>
<text x="-11.43" y="2.54" size="1.016" layer="21" ratio="16" align="center-left">VIN = 3.3-5V</text>
<text x="-7.262" y="9.443" size="1.778" layer="21" ratio="16" align="center">9 DOF</text>
<wire x1="-13.97" y1="-6.35" x2="-13.97" y2="6.35" width="0" layer="21"/>
<wire x1="-16.51" y1="11.43" x2="16.51" y2="11.43" width="0" layer="21"/>
<wire x1="16.51" y1="11.43" x2="19.05" y2="8.89" width="0" layer="21" curve="-90"/>
<wire x1="-16.51" y1="11.43" x2="-19.05" y2="8.89" width="0" layer="21" curve="90"/>
<wire x1="-19.05" y1="8.89" x2="-16.51" y2="6.35" width="0" layer="21" curve="90"/>
<wire x1="-16.51" y1="6.35" x2="-13.97" y2="6.35" width="0" layer="21"/>
<wire x1="13.97" y1="6.35" x2="13.97" y2="-6.35" width="0" layer="21"/>
<wire x1="-19.05" y1="-8.89" x2="-16.51" y2="-11.43" width="0" layer="21" curve="90"/>
<wire x1="19.05" y1="-8.89" x2="16.51" y2="-11.43" width="0" layer="21" curve="-90"/>
<wire x1="16.51" y1="-11.43" x2="-16.51" y2="-11.43" width="0" layer="21"/>
<wire x1="4.638" y1="10.143" x2="5.638" y2="10.143" width="0.4064" layer="21"/>
<wire x1="5.638" y1="10.143" x2="4.938" y2="10.843" width="0.4064" layer="21"/>
<wire x1="4.938" y1="10.843" x2="4.238" y2="10.143" width="0.4064" layer="21"/>
<wire x1="4.238" y1="10.143" x2="4.638" y2="10.143" width="0.4064" layer="21"/>
<wire x1="4.638" y1="10.143" x2="4.938" y2="10.443" width="0.4064" layer="21"/>
<wire x1="-0.462" y1="8.943" x2="0.638" y2="8.943" width="0.4064" layer="21"/>
<wire x1="0.638" y1="8.943" x2="0.638" y2="8.843" width="0.4064" layer="21"/>
<wire x1="0.638" y1="8.843" x2="-0.462" y2="8.843" width="0.4064" layer="21"/>
<wire x1="2.138" y1="9.043" x2="2.138" y2="8.743" width="0.4064" layer="21"/>
<wire x1="4.738" y1="9.943" x2="4.738" y2="8.943" width="0.4064" layer="21"/>
<wire x1="4.738" y1="8.943" x2="5.038" y2="8.943" width="0.4064" layer="21"/>
<wire x1="5.038" y1="8.943" x2="5.038" y2="9.943" width="0.4064" layer="21"/>
<wire x1="4.738" y1="7.443" x2="5.038" y2="7.443" width="0.4064" layer="21"/>
<text x="-11.43" y="-1.27" size="1.016" layer="21" ratio="15" align="center-left">LSM303DLHC</text>
<text x="-11.43" y="-2.54" size="1.016" layer="21" ratio="15" align="center-left">L3GD20</text>
<text x="-5.08" y="-2.54" size="1.016" layer="21" ratio="15" align="center-left">Gyroscope</text>
<text x="-1.27" y="-1.27" size="1.016" layer="21" ratio="15" align="center-left">Accel/Mag</text>
<wire x1="-13.97" y1="-6.35" x2="-16.51" y2="-6.35" width="0" layer="21"/>
<wire x1="-16.51" y1="-6.35" x2="-19.05" y2="-8.89" width="0" layer="21" curve="90"/>
<wire x1="19.05" y1="8.89" x2="16.51" y2="6.35" width="0" layer="21" curve="-90"/>
<wire x1="16.51" y1="6.35" x2="13.97" y2="6.35" width="0" layer="21"/>
<wire x1="13.97" y1="-6.35" x2="16.51" y2="-6.35" width="0" layer="21"/>
<wire x1="16.51" y1="-6.35" x2="19.05" y2="-8.89" width="0" layer="21" curve="-90"/>
<wire x1="2.6" y1="8.9" x2="2" y2="9.5" width="0.4064" layer="21"/>
<wire x1="2" y1="9.5" x2="2" y2="8.3" width="0.4064" layer="21"/>
<wire x1="2" y1="8.3" x2="2.6" y2="8.9" width="0.4064" layer="21"/>
<text x="-11.43" y="6.173" size="1.27" layer="21" ratio="16" align="center-left">adafruit
1714</text>
<hole x="-16.51" y="-8.89" drill="2.75"/>
<pad name="SDA" x="-1.27" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="GINT" x="1.27" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="GRDY" x="3.81" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="LIN1" x="6.35" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="LIN2" x="8.89" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="LRDY" x="11.43" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="SCL" x="-3.81" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="GND" x="-6.35" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="3VO" x="-8.89" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="-11.43" y="-8.89" drill="0.9" shape="long" rot="R90"/>
<hole x="16.51" y="-8.89" drill="2.75"/>
<hole x="-16.51" y="8.89" drill="2.75"/>
<hole x="16.51" y="8.89" drill="2.75"/>
</package>
<package name="ADAFRUIT1980">
<hole x="2.54" y="13.97" drill="2.75"/>
<pad name="GND" x="5.715" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="3VO" x="8.255" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="3.175" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="INT" x="10.795" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SDA" x="13.335" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="5.715" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="10.795" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">Int</text>
<text x="3.175" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">Vin</text>
<text x="13.335" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SDA</text>
<text x="15.875" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SCL</text>
<wire x1="16.51" y1="0" x2="2.54" y2="0" width="0" layer="21"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0" layer="21" curve="-90"/>
<wire x1="0" y1="2.54" x2="0" y2="13.97" width="0" layer="21"/>
<wire x1="0" y1="13.97" x2="2.54" y2="16.51" width="0" layer="21" curve="-90"/>
<wire x1="2.54" y1="16.51" x2="16.51" y2="16.51" width="0" layer="21"/>
<wire x1="16.51" y1="16.51" x2="19.05" y2="13.97" width="0" layer="21" curve="-90"/>
<wire x1="19.05" y1="13.97" x2="19.05" y2="2.54" width="0" layer="21"/>
<wire x1="19.05" y1="2.54" x2="16.51" y2="0" width="0" layer="21" curve="-90"/>
<text x="8.255" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">3vo</text>
<text x="9.525" y="12.7" size="1.016" layer="21" ratio="15" align="center">adafruit 1980

TSL2591
Lux Sensor</text>
<hole x="16.51" y="13.97" drill="2.75"/>
<pad name="SCL" x="15.875" y="2.54" drill="0.9" shape="long" rot="R90"/>
</package>
<package name="ADAFRUIT2857">
<hole x="2.54" y="10.16" drill="2.75"/>
<pad name="GND" x="3.81" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SCL" x="6.35" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="1.27" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SDA" x="8.89" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="ADR" x="11.43" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="3.81" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="6.35" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SCL</text>
<text x="1.27" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">Vin</text>
<text x="8.89" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SDA</text>
<text x="11.43" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">ADR</text>
<wire x1="17.78" y1="10.16" x2="17.78" y2="2.54" width="0" layer="21"/>
<wire x1="17.78" y1="2.54" x2="15.24" y2="0" width="0" layer="21" curve="-90"/>
<wire x1="15.24" y1="0" x2="2.54" y2="0" width="0" layer="21"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0" layer="21" curve="-90"/>
<wire x1="0" y1="2.54" x2="0" y2="10.16" width="0" layer="21"/>
<wire x1="0" y1="10.16" x2="2.54" y2="12.7" width="0" layer="21" curve="-90"/>
<wire x1="2.54" y1="12.7" x2="15.24" y2="12.7" width="0" layer="21"/>
<wire x1="15.24" y1="12.7" x2="17.78" y2="10.16" width="0" layer="21" curve="-90"/>
<text x="9.017" y="9.906" size="0.8128" layer="21" ratio="15" align="center">adafruit 2857

SHT31-D
HUMIDITY</text>
<hole x="15.24" y="10.16" drill="2.75"/>
<pad name="RST" x="13.97" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="ALR" x="16.51" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="13.97" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">RST</text>
<text x="16.51" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">ALR</text>
</package>
<package name="ADAFRUIT2651">
<wire x1="15.24" y1="0" x2="2.54" y2="0" width="0" layer="21"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0" layer="21" curve="-90"/>
<wire x1="0" y1="2.54" x2="0" y2="16.51" width="0" layer="21"/>
<wire x1="0" y1="16.51" x2="2.54" y2="19.05" width="0" layer="21" curve="-90"/>
<wire x1="2.54" y1="19.05" x2="15.24" y2="19.05" width="0" layer="21"/>
<wire x1="15.24" y1="19.05" x2="17.78" y2="16.51" width="0" layer="21" curve="-90"/>
<wire x1="17.78" y1="16.51" x2="17.78" y2="2.54" width="0" layer="21"/>
<wire x1="17.78" y1="2.54" x2="15.24" y2="0" width="0" layer="21" curve="-90"/>
<text x="9.144" y="14.859" size="0.9652" layer="21" font="vector" ratio="12" align="center">BMP280
Pressure &amp;
Temp Sensor
adafruit 2651</text>
<hole x="2.54" y="16.51" drill="2.75"/>
<pad name="GND" x="6.35" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="3VO" x="3.81" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="1.27" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SDO" x="11.43" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SDI" x="13.97" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="6.35" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="3.81" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">3Vo</text>
<text x="1.27" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">VIN</text>
<text x="11.43" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SDO/MISO</text>
<text x="13.97" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SDI/MOSI</text>
<pad name="SCK" x="8.89" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="CS" x="16.51" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="8.89" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SCK</text>
<text x="16.51" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">CS</text>
<hole x="15.24" y="16.51" drill="2.75"/>
</package>
<package name="ADAFRUIT1777">
<text x="10.16" y="14.478" size="1.27" layer="21" ratio="15" align="center">adafruit 1777
SI1145
UV Sensor</text>
<wire x1="0" y1="2.54" x2="0" y2="15.24" width="0" layer="21"/>
<wire x1="0" y1="15.24" x2="2.54" y2="17.78" width="0" layer="21" curve="-90"/>
<wire x1="2.54" y1="17.78" x2="17.78" y2="17.78" width="0" layer="21"/>
<wire x1="17.78" y1="17.78" x2="20.32" y2="15.24" width="0" layer="21" curve="-90"/>
<wire x1="20.32" y1="15.24" x2="20.32" y2="2.54" width="0" layer="21"/>
<wire x1="20.32" y1="2.54" x2="17.78" y2="0" width="0" layer="21" curve="-90"/>
<wire x1="17.78" y1="0" x2="2.54" y2="0" width="0" layer="21"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0" layer="21" curve="-90"/>
<hole x="2.54" y="15.24" drill="2.75"/>
<pad name="GND" x="5.08" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="3VO" x="7.62" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="2.54" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="LED" x="12.7" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="INT" x="10.16" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="5.08" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="7.62" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">3Vo</text>
<text x="2.54" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">Vin</text>
<text x="10.16" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">INT</text>
<text x="12.7" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">LED</text>
<pad name="SCL" x="15.24" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SDA" x="17.78" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="15.24" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SCL</text>
<text x="17.78" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SDA</text>
<hole x="17.78" y="15.24" drill="2.75"/>
</package>
<package name="ADAFRUIT3251">
<wire x1="0" y1="2.54" x2="0" y2="15.24" width="0" layer="21"/>
<wire x1="0" y1="15.24" x2="2.54" y2="17.78" width="0" layer="21" curve="-90"/>
<wire x1="2.54" y1="17.78" x2="12.7" y2="17.78" width="0" layer="21"/>
<wire x1="12.7" y1="17.78" x2="15.24" y2="15.24" width="0" layer="21" curve="-90"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="2.54" width="0" layer="21"/>
<wire x1="15.24" y1="2.54" x2="12.7" y2="0" width="0" layer="21" curve="-90"/>
<wire x1="12.7" y1="0" x2="2.54" y2="0" width="0" layer="21"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0" layer="21" curve="-90"/>
<text x="7.62" y="11.43" size="1.016" layer="21" ratio="16" align="center">adafruit 3251
Si7021
RH%</text>
<hole x="2.54" y="15.24" drill="2.75"/>
<pad name="GND" x="7.62" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="3VO" x="5.08" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="VIN" x="2.54" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SCL" x="10.16" y="2.54" drill="0.9" shape="long" rot="R90"/>
<pad name="SDA" x="12.7" y="2.54" drill="0.9" shape="long" rot="R90"/>
<text x="7.62" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">GND</text>
<text x="5.08" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">3Vo</text>
<text x="2.54" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">Vin</text>
<text x="10.16" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SCL</text>
<text x="12.7" y="4.445" size="1.016" layer="21" ratio="15" rot="R90" align="center-left">SDA</text>
<hole x="12.7" y="15.24" drill="2.75"/>
</package>
<package name="TEEL_PI_SW">
<wire x1="34.29" y1="22.86" x2="2.54" y2="22.86" width="0" layer="21"/>
<wire x1="2.54" y1="22.86" x2="0" y2="20.32" width="0" layer="21" curve="90"/>
<wire x1="0" y1="20.32" x2="0" y2="2.54" width="0" layer="21"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="0" layer="21" curve="90"/>
<wire x1="2.54" y1="0" x2="34.29" y2="0" width="0" layer="21"/>
<wire x1="34.29" y1="0" x2="36.068" y2="2.54" width="0" layer="21" curve="90"/>
<wire x1="36.068" y1="2.54" x2="36.068" y2="20.32" width="0" layer="21"/>
<wire x1="36.068" y1="20.32" x2="34.29" y2="22.86" width="0" layer="21" curve="90"/>
<text x="20.447" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">GND</text>
<text x="17.907" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">EN</text>
<text x="19.177" y="19.05" size="1.016" layer="21" ratio="12" align="center">TeelSys
Pi Power Switch</text>
<text x="12.827" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">LiPo Bat</text>
<text x="10.287" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">USB</text>
<text x="15.367" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">Vs</text>
<text x="28.067" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">STATUS</text>
<text x="25.527" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">CONTROL</text>
<pad name="USB" x="10.287" y="1.27" drill="0.9"/>
<pad name="BAT" x="12.827" y="1.27" drill="0.9"/>
<pad name="VS" x="15.367" y="1.27" drill="0.9"/>
<pad name="EN" x="17.907" y="1.27" drill="0.9"/>
<pad name="GND" x="20.447" y="1.27" drill="0.9"/>
<pad name="LBO" x="22.987" y="1.27" drill="0.9"/>
<pad name="CONTROL" x="25.527" y="1.27" drill="0.9"/>
<pad name="STATUS" x="28.067" y="1.27" drill="0.9"/>
<hole x="2.54" y="20.32" drill="2.75"/>
<hole x="33.909" y="20.32" drill="2.75"/>
<hole x="2.54" y="2.54" drill="2.75"/>
<hole x="33.909" y="2.54" drill="2.75"/>
<text x="22.987" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">LBO</text>
<pad name="PI_USB_5V" x="30.607" y="1.27" drill="0.9"/>
<text x="30.607" y="2.54" size="1.016" layer="21" ratio="12" rot="R90" align="center-left">PI_USB_5V</text>
</package>
<package name="PI_CAMERA">
<wire x1="0" y1="0" x2="23.862" y2="0" width="0.127" layer="21"/>
<wire x1="23.862" y1="0" x2="23.862" y2="25" width="0.127" layer="21"/>
<wire x1="23.862" y1="25" x2="0" y2="25" width="0.127" layer="21"/>
<wire x1="0" y1="25" x2="0" y2="0" width="0.127" layer="21"/>
<circle x="2" y="23" radius="1.1" width="0.127" layer="21"/>
<circle x="2" y="23" radius="1.1" width="0.127" layer="21"/>
<circle x="14.5" y="23" radius="1.1" width="0.127" layer="21"/>
<circle x="2" y="2" radius="1.1" width="0.127" layer="21"/>
<circle x="14.5" y="2" radius="1.1" width="0.127" layer="21"/>
<wire x1="23.5" y1="2.1" x2="18.362" y2="2.1" width="0.127" layer="21"/>
<wire x1="18.362" y1="2.1" x2="18.362" y2="22.9" width="0.127" layer="21"/>
<wire x1="18.362" y1="22.9" x2="23.5" y2="22.9" width="0.127" layer="21"/>
<circle x="14.4" y="12.5" radius="4.25" width="0.127" layer="21"/>
</package>
<package name="TRICORDER_HEAD">
<pad name="DISP_SCK" x="-2.54" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="DISP_LCD_CS" x="0" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="DISP_RESET" x="-5.08" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="DISP_MISO" x="-7.62" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="DISP_MOSI" x="-10.16" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="DISP_DC" x="-12.7" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="DISP_BACKLIGHT" x="-15.24" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="5V" x="-17.78" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="GND" x="-20.32" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="DISP_SD_CS" x="2.54" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="HEAD_SW" x="5.08" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="SW1" x="7.62" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="SW2" x="10.16" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="SW3" x="12.7" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="NEOPIXELS" x="15.24" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="SPK+" x="17.78" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="SPK-" x="20.32" y="0" drill="0.8" shape="long" rot="R90"/>
<text x="-20.32" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">GND</text>
<text x="-17.78" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">5V</text>
<text x="-15.24" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">BLGHT</text>
<text x="-12.7" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">DC</text>
<text x="-10.16" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">MOSI</text>
<text x="-7.62" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">MISO</text>
<text x="-5.08" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">RESET</text>
<text x="-2.54" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">SCK</text>
<text x="0" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">LCD_CS</text>
<text x="2.54" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">SD_CS</text>
<text x="5.08" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">HD_SW</text>
<text x="7.62" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">SW1</text>
<text x="10.16" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">SW2</text>
<text x="12.7" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">SW3</text>
<text x="15.24" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">NEOPIX</text>
<text x="17.78" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">SPK+</text>
<text x="20.32" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">SPK-</text>
</package>
<package name="USB_HEADER">
<pad name="VCC" x="-3.81" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="D-" x="-1.27" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="D+" x="1.27" y="0" drill="0.8" shape="long" rot="R90"/>
<pad name="GND" x="3.81" y="0" drill="0.8" shape="long" rot="R90"/>
<text x="-3.81" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">VCC</text>
<text x="-1.27" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">D-</text>
<text x="1.27" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">D+</text>
<text x="3.81" y="2.54" size="1.27" layer="21" rot="R90" align="center-left">GND</text>
</package>
</packages>
<symbols>
<symbol name="RAZPBERRYPIZERO1.3">
<pin name="01-3.3V" x="-22.86" y="48.26" visible="pin" length="middle"/>
<pin name="02-5V" x="22.86" y="48.26" visible="pin" length="middle" rot="R180"/>
<pin name="03-GPIO02" x="-22.86" y="43.18" visible="pin" length="middle"/>
<pin name="04-5V" x="22.86" y="43.18" visible="pin" length="middle" rot="R180"/>
<pin name="05-GPIO03" x="-22.86" y="38.1" visible="pin" length="middle"/>
<pin name="06-GROUND" x="22.86" y="38.1" visible="pin" length="middle" rot="R180"/>
<pin name="07-GPIO04" x="-22.86" y="33.02" visible="pin" length="middle"/>
<pin name="08-GPIO14" x="22.86" y="33.02" visible="pin" length="middle" rot="R180"/>
<pin name="09-GROUND" x="-22.86" y="27.94" visible="pin" length="middle"/>
<pin name="10-GPIO15" x="22.86" y="27.94" visible="pin" length="middle" rot="R180"/>
<pin name="11-GPIO17" x="-22.86" y="22.86" visible="pin" length="middle"/>
<pin name="12-GPIO18" x="22.86" y="22.86" visible="pin" length="middle" rot="R180"/>
<pin name="13-GPIO27" x="-22.86" y="17.78" visible="pin" length="middle"/>
<pin name="14-GROUND" x="22.86" y="17.78" visible="pin" length="middle" rot="R180"/>
<pin name="15-GPIO22" x="-22.86" y="12.7" visible="pin" length="middle"/>
<pin name="16-GPIO23" x="22.86" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="17-3.3V" x="-22.86" y="7.62" visible="pin" length="middle"/>
<pin name="18-GPIO24" x="22.86" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="19-GPIO10" x="-22.86" y="2.54" visible="pin" length="middle"/>
<pin name="20-GROUND" x="22.86" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="21-GPIO09" x="-22.86" y="-2.54" visible="pin" length="middle"/>
<pin name="22-GPIO25" x="22.86" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="23-GPIO11" x="-22.86" y="-7.62" visible="pin" length="middle"/>
<pin name="24-GPIO08" x="22.86" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="25-GROUND" x="-22.86" y="-12.7" visible="pin" length="middle"/>
<pin name="26-GPIO07" x="22.86" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="27-ID_DS" x="-22.86" y="-17.78" visible="pin" length="middle"/>
<pin name="28-ID_SC" x="22.86" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="29-GPIO05" x="-22.86" y="-22.86" visible="pin" length="middle"/>
<pin name="30-GROUND" x="22.86" y="-22.86" visible="pin" length="middle" rot="R180"/>
<pin name="31-GPIO06" x="-22.86" y="-27.94" visible="pin" length="middle"/>
<pin name="32--GPIO12" x="22.86" y="-27.94" visible="pin" length="middle" rot="R180"/>
<pin name="33-GPIO13" x="-22.86" y="-33.02" visible="pin" length="middle"/>
<pin name="34-GROUND" x="22.86" y="-33.02" visible="pin" length="middle" rot="R180"/>
<pin name="35-GPIO19" x="-22.86" y="-38.1" visible="pin" length="middle"/>
<pin name="36-GPIO16" x="22.86" y="-38.1" visible="pin" length="middle" rot="R180"/>
<pin name="37-GPIO26" x="-22.86" y="-43.18" visible="pin" length="middle"/>
<pin name="38-GPIO20" x="22.86" y="-43.18" visible="pin" length="middle" rot="R180"/>
<pin name="39-GROUND" x="-22.86" y="-48.26" visible="pin" length="middle"/>
<pin name="40-GPIO21" x="22.86" y="-48.26" visible="pin" length="middle" rot="R180"/>
<wire x1="-17.78" y1="53.34" x2="17.78" y2="53.34" width="0.254" layer="94"/>
<wire x1="17.78" y1="53.34" x2="17.78" y2="-53.34" width="0.254" layer="94"/>
<wire x1="17.78" y1="-53.34" x2="-17.78" y2="-53.34" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-53.34" x2="-17.78" y2="53.34" width="0.254" layer="94"/>
<pin name="PP23-USB_D-" x="-91.44" y="12.7" visible="pin" length="middle"/>
<pin name="PP22-USB_D+" x="-91.44" y="7.62" visible="pin" length="middle"/>
<pin name="PP1-5V" x="-91.44" y="17.78" visible="pin" length="middle"/>
<pin name="PP6-GND" x="-91.44" y="2.54" visible="pin" length="middle"/>
<wire x1="25.4" y1="58.42" x2="-93.98" y2="58.42" width="0.254" layer="94"/>
<wire x1="-93.98" y1="58.42" x2="-93.98" y2="-60.96" width="0.254" layer="94"/>
<wire x1="-93.98" y1="-60.96" x2="25.4" y2="-60.96" width="0.254" layer="94"/>
<wire x1="25.4" y1="-60.96" x2="25.4" y2="58.42" width="0.254" layer="94"/>
<text x="-91.44" y="60.96" size="1.27" layer="95">&gt;NAME</text>
<text x="-76.2" y="50.8" size="1.27" layer="94" align="center">adafruit 3400
Rapberry Pi Zero W</text>
<pin name="RUN_GND" x="-91.44" y="-27.94" visible="pin" length="middle"/>
<pin name="VID_GND" x="-91.44" y="-33.02" visible="pin" length="middle"/>
<pin name="RUN" x="-91.44" y="-22.86" visible="pin" length="middle"/>
<pin name="VID" x="-91.44" y="-38.1" visible="pin" length="middle"/>
</symbol>
<symbol name="ZERO4U">
<pin name="USBD-" x="-5.08" y="2.54" visible="pin" length="middle"/>
<pin name="USBD+" x="-5.08" y="-2.54" visible="pin" length="middle"/>
<pin name="USBGND" x="-5.08" y="-7.62" visible="pin" length="middle"/>
<pin name="USB+5V" x="-5.08" y="7.62" visible="pin" length="middle"/>
<wire x1="-2.54" y1="10.16" x2="25.4" y2="10.16" width="0.254" layer="97"/>
<wire x1="25.4" y1="10.16" x2="25.4" y2="-10.16" width="0.254" layer="97"/>
<wire x1="25.4" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="10.16" width="0.254" layer="97"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="17.78" y="2.54" size="1.27" layer="94" align="center">adafruit 3298
Zero4U - 4 Port 
USB Hub for
Rapberry Pi Zero</text>
</symbol>
<symbol name="ADAFRUIT2465">
<text x="20.32" y="20.32" size="1.27" layer="94" align="center">adafruit 2465
PowerBoost 1000C</text>
<wire x1="0" y1="0" x2="40.64" y2="0" width="0.254" layer="94"/>
<wire x1="40.64" y1="0" x2="40.64" y2="25.4" width="0.254" layer="94"/>
<wire x1="40.64" y1="25.4" x2="0" y2="25.4" width="0.254" layer="94"/>
<wire x1="0" y1="25.4" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="USB" x="2.54" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="BAT" x="7.62" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="VS" x="12.7" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="EN" x="17.78" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="22.86" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="LBO" x="27.94" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="G" x="33.02" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="5V" x="38.1" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="OUT+" x="43.18" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="OUT-" x="43.18" y="17.78" visible="pin" length="middle" rot="R180"/>
<text x="0" y="27.94" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="+3.3V">
<pin name="+3.3V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LI_BATTERY">
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="3.175" y1="2.54" x2="3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.4064" layer="94"/>
<text x="-3.81" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="-" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="ADAFRUIT1475">
<pin name="MIC_GND1" x="30.48" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="MIC_GND2" x="30.48" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="MIC_R" x="30.48" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="MIC_L1" x="30.48" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="MIC_L2" x="30.48" y="20.32" visible="pin" length="middle" rot="R180"/>
<pin name="SPK_GND1" x="-30.48" y="0" visible="pin" length="middle"/>
<pin name="SPK_GND2" x="-30.48" y="5.08" visible="pin" length="middle"/>
<pin name="SPK_R" x="-30.48" y="10.16" visible="pin" length="middle"/>
<pin name="SPK_L1" x="-30.48" y="15.24" visible="pin" length="middle"/>
<pin name="SPK_L2" x="-30.48" y="20.32" visible="pin" length="middle"/>
<pin name="5V" x="-7.62" y="35.56" visible="pin" length="middle" rot="R270"/>
<pin name="D-" x="-2.54" y="35.56" visible="pin" length="middle" rot="R270"/>
<pin name="D+" x="2.54" y="35.56" visible="pin" length="middle" rot="R270"/>
<pin name="GND" x="7.62" y="35.56" visible="pin" length="middle" rot="R270"/>
<wire x1="-25.4" y1="-2.54" x2="-25.4" y2="30.48" width="0.254" layer="94"/>
<wire x1="-25.4" y1="30.48" x2="25.4" y2="30.48" width="0.254" layer="94"/>
<wire x1="25.4" y1="30.48" x2="25.4" y2="-2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-2.54" x2="-25.4" y2="-2.54" width="0.254" layer="94"/>
<text x="0" y="12.7" size="1.4224" layer="94" align="center">adafruit 1475
USB Audio</text>
<text x="-25.4" y="-7.62" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="ADAFRUIT2716">
<pin name="GND" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="3V3" x="-5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="-10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="AC" x="5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="DC" x="10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="10.16" size="1.27" layer="94" align="center">adafruit 2716
MEMS Microphone</text>
<wire x1="-12.7" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="0" width="0.254" layer="94"/>
<text x="-12.7" y="17.78" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="RADIATION_WATCH_TYPE5">
<text x="0" y="10.16" size="1.27" layer="94" align="center">Radiation Watch
Type 5</text>
<pin name="SIG" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="-5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="+V" x="-10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND2" x="5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="NS" x="10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<wire x1="-12.7" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="0" width="0.254" layer="94"/>
<text x="-12.7" y="17.78" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="SPARKFUN_13670">
<pin name="RXA" x="2.54" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="TXA" x="-2.54" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VCC" x="-7.62" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="-12.7" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="V_BAT" x="7.62" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="PWR_CTRL" x="12.7" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="12.7" size="1.27" layer="94" align="center">SparkFun 13670
GPS Receiver - GP-735</text>
<wire x1="-15.24" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="17.78" width="0.254" layer="94"/>
<wire x1="15.24" y1="17.78" x2="-15.24" y2="17.78" width="0.254" layer="94"/>
<wire x1="-15.24" y1="17.78" x2="-15.24" y2="0" width="0.254" layer="94"/>
<text x="-15.24" y="20.32" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="ADAFRUIT746">
<pin name="TX" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="FIX" x="-5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VBAT" x="-10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="EN" x="-15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="3V3" x="-20.32" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="RX" x="5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="PPS" x="20.32" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="10.16" size="1.27" layer="94" align="center">adafruit 746
Ultimate GPS</text>
<wire x1="-22.86" y1="0" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="22.86" y2="15.24" width="0.254" layer="94"/>
<wire x1="22.86" y1="15.24" x2="-22.86" y2="15.24" width="0.254" layer="94"/>
<wire x1="-22.86" y1="15.24" x2="-22.86" y2="0" width="0.254" layer="94"/>
<text x="-22.86" y="17.78" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="ADAFRUIT1714">
<pin name="VIN" x="-22.86" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="LRDY" x="22.86" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="3VO" x="-17.78" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="-12.7" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SCL" x="-7.62" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SDA" x="-2.54" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GINT" x="2.54" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GRDY" x="7.62" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="LIN1" x="12.7" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="LIN2" x="17.78" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="12.7" size="1.27" layer="94" align="center">adafruit 1714
9-DOF</text>
<text x="-25.4" y="20.32" size="1.27" layer="95">&gt;NAME</text>
<wire x1="-25.4" y1="0" x2="25.4" y2="0" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="25.4" y2="17.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="17.78" x2="-25.4" y2="17.78" width="0.254" layer="94"/>
<wire x1="-25.4" y1="17.78" x2="-25.4" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="ADAFRUIT1980">
<pin name="3VO" x="-2.54" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="-7.62" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="-12.7" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="INT" x="2.54" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SDA" x="7.62" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="10.16" size="1.27" layer="94" align="center">adafruit 1980
TSL2591
Lux Sensor</text>
<wire x1="-15.24" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="15.24" x2="-15.24" y2="0" width="0.254" layer="94"/>
<text x="-15.24" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<pin name="SCL" x="12.7" y="-5.08" visible="pin" length="middle" rot="R90"/>
</symbol>
<symbol name="ADAFRUIT2857">
<pin name="SCL" x="-5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="-10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="-15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="ADR" x="5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="RST" x="10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="10.16" size="1.27" layer="94" align="center">adafruit 2857
SHT31-D
Humidity</text>
<wire x1="-17.78" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="15.24" x2="-17.78" y2="0" width="0.254" layer="94"/>
<text x="-17.78" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<pin name="ALR" x="15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SDA" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
</symbol>
<symbol name="ADAFRUIT2651">
<pin name="GND" x="-5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="3VO" x="-10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="-15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SCK" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SDO/MISO" x="5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="17.78" size="1.27" layer="94" align="center">adafruit 2651
BMP280
Pressure &amp; Temp Sensor</text>
<wire x1="-17.78" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="22.86" width="0.254" layer="94"/>
<wire x1="17.78" y1="22.86" x2="-17.78" y2="22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="22.86" x2="-17.78" y2="0" width="0.254" layer="94"/>
<text x="-17.78" y="25.4" size="1.27" layer="95">&gt;NAME</text>
<pin name="SDI/MOSI" x="10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="CS" x="15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
</symbol>
<symbol name="ADAFRUIT1777">
<pin name="3VO" x="-5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="-10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="-15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="INT" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="LED" x="5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="10.16" size="1.27" layer="94" align="center">adafruit 1777
SI1145
UV Sensor</text>
<wire x1="-17.78" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="15.24" x2="-17.78" y2="0" width="0.254" layer="94"/>
<text x="-17.78" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<pin name="SCL" x="10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SDA" x="15.24" y="-5.08" visible="pin" length="middle" rot="R90"/>
</symbol>
<symbol name="ADAFRUIT3251">
<pin name="GND" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="3VO" x="-5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="-10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SCL" x="5.08" y="-5.08" visible="pin" length="middle" rot="R90"/>
<pin name="SDA" x="10.16" y="-5.08" visible="pin" length="middle" rot="R90"/>
<text x="0" y="10.16" size="1.27" layer="94" align="center">adafruit 3251
Si7021
RH%</text>
<wire x1="-12.7" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="0" width="0.254" layer="94"/>
<text x="-12.7" y="17.78" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="TEEL_PI_SW">
<text x="22.86" y="20.32" size="1.27" layer="94" align="center">TeelSys
Pi Power Switch</text>
<wire x1="0" y1="0" x2="45.72" y2="0" width="0.254" layer="94"/>
<wire x1="45.72" y1="0" x2="45.72" y2="25.4" width="0.254" layer="94"/>
<wire x1="45.72" y1="25.4" x2="0" y2="25.4" width="0.254" layer="94"/>
<wire x1="0" y1="25.4" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="USB" x="2.54" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="BAT" x="7.62" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="VS" x="12.7" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="EN" x="17.78" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="22.86" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="LBO" x="27.94" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="CONTROL" x="33.02" y="-2.54" visible="pin" length="middle" rot="R90"/>
<pin name="STATUS" x="38.1" y="-2.54" visible="pin" length="middle" rot="R90"/>
<text x="0" y="27.94" size="1.27" layer="95">&gt;NAME</text>
<pin name="PI_USB_5V" x="43.18" y="-2.54" visible="pin" length="middle" rot="R90"/>
</symbol>
<symbol name="PI_CAMERA">
<wire x1="0" y1="0" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="20.32" y2="20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="10.16" y="10.16" size="1.778" layer="94" align="center">Raspberry
Pi
Camera</text>
</symbol>
<symbol name="TRICORDER_HEAD">
<pin name="GND" x="2.54" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<text x="0" y="25.4" size="1.27" layer="95">&gt;NAME</text>
<text x="43.18" y="25.4" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="0" y1="0" x2="86.36" y2="0" width="0.254" layer="94"/>
<wire x1="86.36" y1="0" x2="86.36" y2="22.86" width="0.254" layer="94"/>
<wire x1="86.36" y1="22.86" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="5V" x="7.62" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_BACKLIGHT" x="12.7" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_DC" x="17.78" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_MOSI" x="22.86" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_MISO" x="27.94" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_RESET" x="33.02" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_SCK" x="38.1" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_LCD_CS" x="43.18" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="DISP_SD_CS" x="48.26" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="HEAD_SW" x="53.34" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="SW1" x="58.42" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="SW2" x="63.5" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="SW3" x="68.58" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="NEOPIXELS" x="73.66" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="SPK+" x="78.74" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
<pin name="SPK-" x="83.82" y="-2.54" visible="pin" length="middle" direction="pas" function="dot" rot="R90"/>
</symbol>
<symbol name="USB_HEADER">
<pin name="VCC" x="-7.62" y="0" length="middle" rot="R90"/>
<pin name="D-" x="-2.54" y="0" length="middle" rot="R90"/>
<pin name="D+" x="2.54" y="0" length="middle" rot="R90"/>
<pin name="GND" x="7.62" y="0" length="middle" rot="R90"/>
<wire x1="-10.16" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RAZPBERRYPIZERO1.3" prefix="ASM">
<description>Raspberry Pi Zero W
PRODUCT ID: 3400</description>
<gates>
<gate name="G$1" symbol="RAZPBERRYPIZERO1.3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RAZPBERRYPIZERO1.3">
<connects>
<connect gate="G$1" pin="01-3.3V" pad="P$1"/>
<connect gate="G$1" pin="02-5V" pad="P$2"/>
<connect gate="G$1" pin="03-GPIO02" pad="P$3"/>
<connect gate="G$1" pin="04-5V" pad="P$4"/>
<connect gate="G$1" pin="05-GPIO03" pad="P$5"/>
<connect gate="G$1" pin="06-GROUND" pad="P$6"/>
<connect gate="G$1" pin="07-GPIO04" pad="P$7"/>
<connect gate="G$1" pin="08-GPIO14" pad="P$8"/>
<connect gate="G$1" pin="09-GROUND" pad="P$9"/>
<connect gate="G$1" pin="10-GPIO15" pad="P$10"/>
<connect gate="G$1" pin="11-GPIO17" pad="P$11"/>
<connect gate="G$1" pin="12-GPIO18" pad="P$12"/>
<connect gate="G$1" pin="13-GPIO27" pad="P$13"/>
<connect gate="G$1" pin="14-GROUND" pad="P$14"/>
<connect gate="G$1" pin="15-GPIO22" pad="P$15"/>
<connect gate="G$1" pin="16-GPIO23" pad="P$16"/>
<connect gate="G$1" pin="17-3.3V" pad="P$17"/>
<connect gate="G$1" pin="18-GPIO24" pad="P$18"/>
<connect gate="G$1" pin="19-GPIO10" pad="P$19"/>
<connect gate="G$1" pin="20-GROUND" pad="P$20"/>
<connect gate="G$1" pin="21-GPIO09" pad="P$21"/>
<connect gate="G$1" pin="22-GPIO25" pad="P$22"/>
<connect gate="G$1" pin="23-GPIO11" pad="P$23"/>
<connect gate="G$1" pin="24-GPIO08" pad="P$24"/>
<connect gate="G$1" pin="25-GROUND" pad="P$25"/>
<connect gate="G$1" pin="26-GPIO07" pad="P$26"/>
<connect gate="G$1" pin="27-ID_DS" pad="P$27"/>
<connect gate="G$1" pin="28-ID_SC" pad="P$28"/>
<connect gate="G$1" pin="29-GPIO05" pad="P$29"/>
<connect gate="G$1" pin="30-GROUND" pad="P$30"/>
<connect gate="G$1" pin="31-GPIO06" pad="P$31"/>
<connect gate="G$1" pin="32--GPIO12" pad="P$32"/>
<connect gate="G$1" pin="33-GPIO13" pad="P$33"/>
<connect gate="G$1" pin="34-GROUND" pad="P$34"/>
<connect gate="G$1" pin="35-GPIO19" pad="P$35"/>
<connect gate="G$1" pin="36-GPIO16" pad="P$36"/>
<connect gate="G$1" pin="37-GPIO26" pad="P$37"/>
<connect gate="G$1" pin="38-GPIO20" pad="P$38"/>
<connect gate="G$1" pin="39-GROUND" pad="P$39"/>
<connect gate="G$1" pin="40-GPIO21" pad="P$40"/>
<connect gate="G$1" pin="PP1-5V" pad="PP1"/>
<connect gate="G$1" pin="PP22-USB_D+" pad="PP22"/>
<connect gate="G$1" pin="PP23-USB_D-" pad="PP23"/>
<connect gate="G$1" pin="PP6-GND" pad="PP6"/>
<connect gate="G$1" pin="RUN" pad="RUN"/>
<connect gate="G$1" pin="RUN_GND" pad="RUN_GND"/>
<connect gate="G$1" pin="VID" pad="VID"/>
<connect gate="G$1" pin="VID_GND" pad="VID_GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZERO4U" prefix="ASM">
<description>Zero4U - 4 Port USB Hub for Raspberry Pi Zero v1.3
PRODUCT ID: 3298</description>
<gates>
<gate name="G$1" symbol="ZERO4U" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="ZERO4U">
<connects>
<connect gate="G$1" pin="USB+5V" pad="USB+5V"/>
<connect gate="G$1" pin="USBD+" pad="USBD+"/>
<connect gate="G$1" pin="USBD-" pad="USBD-"/>
<connect gate="G$1" pin="USBGND" pad="USBGND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT2465" prefix="ASM">
<description>PowerBoost 1000 Charger - Rechargeable 5V Lipo USB Boost @ 1A - 1000C
PRODUCT ID: 2465</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT2465" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT2465">
<connects>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="BAT" pad="BAT"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="LBO" pad="LBO"/>
<connect gate="G$1" pin="OUT+" pad="OUT+"/>
<connect gate="G$1" pin="OUT-" pad="OUT-"/>
<connect gate="G$1" pin="USB" pad="USB"/>
<connect gate="G$1" pin="VS" pad="VS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3.3V" prefix="SUPPLY">
<description>SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="+3.3V" x="0" y="2.54"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LI_BATTERY" prefix="BAT">
<description>Lithium Ion Battery Pack - 3.7V 6600mAh
PRODUCT ID: 353</description>
<gates>
<gate name="G$1" symbol="LI_BATTERY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LI_BATTERY">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT1475" prefix="ASM">
<description>USB Audio Adapter - Works with Raspberry Pi
PRODUCT ID: 1475</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT1475" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="ADAFRUIT1475">
<connects>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="MIC_GND1" pad="MIC_GND1"/>
<connect gate="G$1" pin="MIC_GND2" pad="MIC_GND2"/>
<connect gate="G$1" pin="MIC_L1" pad="MIC_L1"/>
<connect gate="G$1" pin="MIC_L2" pad="MIC_L2"/>
<connect gate="G$1" pin="MIC_R" pad="MIC_R"/>
<connect gate="G$1" pin="SPK_GND1" pad="SPK_GND1"/>
<connect gate="G$1" pin="SPK_GND2" pad="SPK_GND2"/>
<connect gate="G$1" pin="SPK_L1" pad="SPK_L1"/>
<connect gate="G$1" pin="SPK_L2" pad="SPK_L2"/>
<connect gate="G$1" pin="SPK_R" pad="SPK_R"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT2716" prefix="ASM">
<description>Adafruit Silicon MEMS Microphone Breakout - SPW2430
PRODUCT ID: 2716</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT2716" x="0" y="5.08"/>
</gates>
<devices>
<device name="" package="ADAFRUIT2716">
<connects>
<connect gate="G$1" pin="3V3" pad="3V3"/>
<connect gate="G$1" pin="AC" pad="AC"/>
<connect gate="G$1" pin="DC" pad="DC"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RADIATION_WATCH_TYPE5" prefix="ASM">
<description>Radiation-Watch Type 5 (Embedded) Geiger Counter</description>
<gates>
<gate name="G$1" symbol="RADIATION_WATCH_TYPE5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RADIATION_WATCH_TYPE5">
<connects>
<connect gate="G$1" pin="+V" pad="+V"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="NS" pad="NS"/>
<connect gate="G$1" pin="SIG" pad="SIG"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPARKFUN_13670" prefix="ASM">
<description>GPS Receiver - GP-735 (56 Channel)</description>
<gates>
<gate name="G$1" symbol="SPARKFUN_13670" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SPARKFUN_13670">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="PWR_CTRL" pad="PWR_CTRL"/>
<connect gate="G$1" pin="RXA" pad="RXA"/>
<connect gate="G$1" pin="TXA" pad="TXA"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="V_BAT" pad="V_BAT"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="GPS-13670" constant="no"/>
<attribute name="VENDOR" value="Spark fun" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT746" prefix="ASM">
<description>Adafruit Ultimate GPS Breakout - 66 channel w/10 Hz updates - Version 3
PRODUCT ID: 746</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT746" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT746">
<connects>
<connect gate="G$1" pin="3V3" pad="3V3"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="FIX" pad="FIX"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="PPS" pad="PPS"/>
<connect gate="G$1" pin="RX" pad="RX"/>
<connect gate="G$1" pin="TX" pad="TX"/>
<connect gate="G$1" pin="VBAT" pad="VBAT"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT1714" prefix="ASM">
<description>Adafruit 9-DOF IMU Breakout - L3GD20H + LSM303
PRODUCT ID: 1714</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT1714" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT1714">
<connects>
<connect gate="G$1" pin="3VO" pad="3VO"/>
<connect gate="G$1" pin="GINT" pad="GINT"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GRDY" pad="GRDY"/>
<connect gate="G$1" pin="LIN1" pad="LIN1"/>
<connect gate="G$1" pin="LIN2" pad="LIN2"/>
<connect gate="G$1" pin="LRDY" pad="LRDY"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT1980" prefix="ASM">
<description>Adafruit TSL2591 High Dynamic Range Digital Light Sensor
PRODUCT ID: 1980</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT1980" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT1980">
<connects>
<connect gate="G$1" pin="3VO" pad="3VO"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="INT" pad="INT"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT2857" prefix="ASM">
<description>Adafruit Sensiron SHT31-D Temperature &amp; Humidity Sensor Breakout
PRODUCT ID: 2857</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT2857" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT2857">
<connects>
<connect gate="G$1" pin="ADR" pad="ADR"/>
<connect gate="G$1" pin="ALR" pad="ALR"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT2651" prefix="ASM">
<description>Adafruit BMP280 I2C or SPI Barometric Pressure &amp; Altitude Sensor
PRODUCT ID: 2651</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT2651" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT2651">
<connects>
<connect gate="G$1" pin="3VO" pad="3VO"/>
<connect gate="G$1" pin="CS" pad="CS"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
<connect gate="G$1" pin="SDI/MOSI" pad="SDI"/>
<connect gate="G$1" pin="SDO/MISO" pad="SDO"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT1777" prefix="ASM">
<description>SI1145 Digital UV Index / IR / Visible Light Sensor
PRODUCT ID: 1777</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT1777" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT1777">
<connects>
<connect gate="G$1" pin="3VO" pad="3VO"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="INT" pad="INT"/>
<connect gate="G$1" pin="LED" pad="LED"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADAFRUIT3251" prefix="ASM">
<description>Adafruit Si7021 Temperature &amp; Humidity Sensor Breakout Board
PRODUCT ID: 3251</description>
<gates>
<gate name="G$1" symbol="ADAFRUIT3251" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ADAFRUIT3251">
<connects>
<connect gate="G$1" pin="3VO" pad="3VO"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TEEL_PI_SW" prefix="ASM">
<description>TeelSys - Raspberry Pi Power Switch</description>
<gates>
<gate name="G$1" symbol="TEEL_PI_SW" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TEEL_PI_SW">
<connects>
<connect gate="G$1" pin="BAT" pad="BAT"/>
<connect gate="G$1" pin="CONTROL" pad="CONTROL"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="LBO" pad="LBO"/>
<connect gate="G$1" pin="PI_USB_5V" pad="PI_USB_5V"/>
<connect gate="G$1" pin="STATUS" pad="STATUS"/>
<connect gate="G$1" pin="USB" pad="USB"/>
<connect gate="G$1" pin="VS" pad="VS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PI_CAMERA" prefix="ASM">
<gates>
<gate name="G$1" symbol="PI_CAMERA" x="-10.16" y="-10.16"/>
</gates>
<devices>
<device name="" package="PI_CAMERA">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRICORDER_HEAD" prefix="JP">
<gates>
<gate name="G$1" symbol="TRICORDER_HEAD" x="-43.18" y="-10.16"/>
</gates>
<devices>
<device name="" package="TRICORDER_HEAD">
<connects>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="DISP_BACKLIGHT" pad="DISP_BACKLIGHT"/>
<connect gate="G$1" pin="DISP_DC" pad="DISP_DC"/>
<connect gate="G$1" pin="DISP_LCD_CS" pad="DISP_LCD_CS"/>
<connect gate="G$1" pin="DISP_MISO" pad="DISP_MISO"/>
<connect gate="G$1" pin="DISP_MOSI" pad="DISP_MOSI"/>
<connect gate="G$1" pin="DISP_RESET" pad="DISP_RESET"/>
<connect gate="G$1" pin="DISP_SCK" pad="DISP_SCK"/>
<connect gate="G$1" pin="DISP_SD_CS" pad="DISP_SD_CS"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="HEAD_SW" pad="HEAD_SW"/>
<connect gate="G$1" pin="NEOPIXELS" pad="NEOPIXELS"/>
<connect gate="G$1" pin="SPK+" pad="SPK+"/>
<connect gate="G$1" pin="SPK-" pad="SPK-"/>
<connect gate="G$1" pin="SW1" pad="SW1"/>
<connect gate="G$1" pin="SW2" pad="SW2"/>
<connect gate="G$1" pin="SW3" pad="SW3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB_HEADER" prefix="JP">
<gates>
<gate name="G$1" symbol="USB_HEADER" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="USB_HEADER">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+05V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="EG1218">
<wire x1="-5.842" y1="2.032" x2="-0.254" y2="2.032" width="0.127" layer="21"/>
<wire x1="-0.254" y1="2.032" x2="0" y2="1.778" width="0.127" layer="21"/>
<wire x1="0" y1="1.778" x2="0.254" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.254" y1="2.032" x2="5.842" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.842" y1="2.032" x2="5.842" y2="1.524" width="0.127" layer="21"/>
<wire x1="5.842" y1="1.524" x2="5.334" y2="1.524" width="0.127" layer="21"/>
<wire x1="5.334" y1="1.524" x2="5.334" y2="-1.524" width="0.127" layer="21"/>
<wire x1="5.334" y1="-1.524" x2="5.842" y2="-1.524" width="0.127" layer="21"/>
<wire x1="5.842" y1="-1.524" x2="5.842" y2="-2.032" width="0.127" layer="21"/>
<wire x1="5.842" y1="-2.032" x2="0.254" y2="-2.032" width="0.127" layer="21"/>
<wire x1="0.254" y1="-2.032" x2="0" y2="-1.778" width="0.127" layer="21"/>
<wire x1="0" y1="-1.778" x2="-0.254" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-2.032" x2="-5.842" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-5.842" y1="-2.032" x2="-5.842" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-5.842" y1="-1.524" x2="-5.334" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-1.524" x2="-5.334" y2="1.524" width="0.127" layer="21"/>
<wire x1="-5.334" y1="1.524" x2="-5.842" y2="1.524" width="0.127" layer="21"/>
<wire x1="-5.842" y1="1.524" x2="-5.842" y2="2.032" width="0.127" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="2.286" y2="1.016" width="0.127" layer="25"/>
<wire x1="2.286" y1="1.016" x2="2.286" y2="-1.016" width="0.127" layer="25"/>
<wire x1="2.286" y1="-1.016" x2="-2.286" y2="-1.016" width="0.127" layer="25"/>
<wire x1="-2.286" y1="-1.016" x2="-2.286" y2="1.016" width="0.127" layer="25"/>
<wire x1="-2.032" y1="1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.016" x2="-1.778" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.27" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="1.016" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-1.016" x2="-0.762" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<pad name="P" x="0" y="0" drill="0.9" diameter="1.6764"/>
<pad name="O" x="-2.54" y="0" drill="0.9" diameter="1.6764"/>
<pad name="S" x="2.54" y="0" drill="0.9" diameter="1.6764"/>
<text x="-2.794" y="2.54" size="1.27" layer="25" font="vector">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="SIS">
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.635" y2="3.175" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.254" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="0" width="0.1524" layer="94"/>
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="O" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EG1218" prefix="S" uservalue="yes">
<description>&lt;b&gt;SLIDING SWITCH&lt;/b&gt;&lt;p&gt;
0.1" spacing through hole, vertical SPDT slide switch
&lt;p&gt;http://www.ladyada.net/library/pcb/eaglelibrary.html&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="SIS" x="0" y="0"/>
</gates>
<devices>
<device name="S" package="EG1218">
<connects>
<connect gate="1" pin="O" pad="O"/>
<connect gate="1" pin="P" pad="P"/>
<connect gate="1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="teelsys-0.2">
<packages>
<package name="TEELSYS_LOGO_ART_2">
<rectangle x1="-5.0165" y1="-5.2705" x2="-3.3655" y2="-5.1435" layer="21"/>
<rectangle x1="-5.5245" y1="-5.1435" x2="-2.6035" y2="-5.0165" layer="21"/>
<rectangle x1="-6.0325" y1="-5.0165" x2="-1.5875" y2="-4.8895" layer="21"/>
<rectangle x1="-6.5405" y1="-4.8895" x2="-0.8255" y2="-4.7625" layer="21"/>
<rectangle x1="-6.7945" y1="-4.7625" x2="0.0635" y2="-4.6355" layer="21"/>
<rectangle x1="-7.1755" y1="-4.6355" x2="0.8255" y2="-4.5085" layer="21"/>
<rectangle x1="-7.4295" y1="-4.5085" x2="1.5875" y2="-4.3815" layer="21"/>
<rectangle x1="-7.6835" y1="-4.3815" x2="2.2225" y2="-4.2545" layer="21"/>
<rectangle x1="-7.9375" y1="-4.2545" x2="2.9845" y2="-4.1275" layer="21"/>
<rectangle x1="-8.1915" y1="-4.1275" x2="3.6195" y2="-4.0005" layer="21"/>
<rectangle x1="-8.3185" y1="-4.0005" x2="4.2545" y2="-3.8735" layer="21"/>
<rectangle x1="-8.5725" y1="-3.8735" x2="4.8895" y2="-3.7465" layer="21"/>
<rectangle x1="-8.6995" y1="-3.7465" x2="5.5245" y2="-3.6195" layer="21"/>
<rectangle x1="-8.9535" y1="-3.6195" x2="6.1595" y2="-3.4925" layer="21"/>
<rectangle x1="-9.0805" y1="-3.4925" x2="6.6675" y2="-3.3655" layer="21"/>
<rectangle x1="-9.3345" y1="-3.3655" x2="7.3025" y2="-3.2385" layer="21"/>
<rectangle x1="-9.4615" y1="-3.2385" x2="7.8105" y2="-3.1115" layer="21"/>
<rectangle x1="-9.5885" y1="-3.1115" x2="8.3185" y2="-2.9845" layer="21"/>
<rectangle x1="-9.8425" y1="-2.9845" x2="8.6995" y2="-2.8575" layer="21"/>
<rectangle x1="-9.9695" y1="-2.8575" x2="9.2075" y2="-2.7305" layer="21"/>
<rectangle x1="-10.0965" y1="-2.7305" x2="-7.9375" y2="-2.6035" layer="21"/>
<rectangle x1="-7.3025" y1="-2.7305" x2="9.5885" y2="-2.6035" layer="21"/>
<rectangle x1="-10.2235" y1="-2.6035" x2="-8.0645" y2="-2.4765" layer="21"/>
<rectangle x1="-7.1755" y1="-2.6035" x2="9.9695" y2="-2.4765" layer="21"/>
<rectangle x1="-10.3505" y1="-2.4765" x2="-8.1915" y2="-2.3495" layer="21"/>
<rectangle x1="-7.1755" y1="-2.4765" x2="10.2235" y2="-2.3495" layer="21"/>
<rectangle x1="-10.6045" y1="-2.3495" x2="-8.1915" y2="-2.2225" layer="21"/>
<rectangle x1="0.4445" y1="-2.3495" x2="10.4775" y2="-2.2225" layer="21"/>
<rectangle x1="-10.6045" y1="-2.2225" x2="-8.1915" y2="-2.0955" layer="21"/>
<rectangle x1="0.4445" y1="-2.2225" x2="10.7315" y2="-2.0955" layer="21"/>
<rectangle x1="-10.7315" y1="-2.0955" x2="-8.1915" y2="-1.9685" layer="21"/>
<rectangle x1="-7.0485" y1="-2.0955" x2="3.6195" y2="-1.9685" layer="21"/>
<rectangle x1="4.3815" y1="-2.0955" x2="10.8585" y2="-1.9685" layer="21"/>
<rectangle x1="-10.8585" y1="-1.9685" x2="-8.1915" y2="-1.8415" layer="21"/>
<rectangle x1="-7.1755" y1="-1.9685" x2="3.6195" y2="-1.8415" layer="21"/>
<rectangle x1="4.6355" y1="-1.9685" x2="11.1125" y2="-1.8415" layer="21"/>
<rectangle x1="-10.9855" y1="-1.8415" x2="-8.0645" y2="-1.7145" layer="21"/>
<rectangle x1="-7.3025" y1="-1.8415" x2="3.6195" y2="-1.7145" layer="21"/>
<rectangle x1="4.7625" y1="-1.8415" x2="11.2395" y2="-1.7145" layer="21"/>
<rectangle x1="-10.9855" y1="-1.7145" x2="-7.9375" y2="-1.5875" layer="21"/>
<rectangle x1="-7.4295" y1="-1.7145" x2="3.6195" y2="-1.5875" layer="21"/>
<rectangle x1="4.8895" y1="-1.7145" x2="11.3665" y2="-1.5875" layer="21"/>
<rectangle x1="-11.1125" y1="-1.5875" x2="3.6195" y2="-1.4605" layer="21"/>
<rectangle x1="4.8895" y1="-1.5875" x2="11.4935" y2="-1.4605" layer="21"/>
<rectangle x1="-11.2395" y1="-1.4605" x2="4.1275" y2="-1.3335" layer="21"/>
<rectangle x1="4.8895" y1="-1.4605" x2="11.4935" y2="-1.3335" layer="21"/>
<rectangle x1="-11.2395" y1="-1.3335" x2="-6.1595" y2="-1.2065" layer="21"/>
<rectangle x1="-5.6515" y1="-1.3335" x2="-4.2545" y2="-1.2065" layer="21"/>
<rectangle x1="-3.4925" y1="-1.3335" x2="-1.9685" y2="-1.2065" layer="21"/>
<rectangle x1="-1.2065" y1="-1.3335" x2="-0.0635" y2="-1.2065" layer="21"/>
<rectangle x1="0.3175" y1="-1.3335" x2="1.4605" y2="-1.2065" layer="21"/>
<rectangle x1="2.4765" y1="-1.3335" x2="4.2545" y2="-1.2065" layer="21"/>
<rectangle x1="5.0165" y1="-1.3335" x2="6.4135" y2="-1.2065" layer="21"/>
<rectangle x1="7.4295" y1="-1.3335" x2="11.6205" y2="-1.2065" layer="21"/>
<rectangle x1="-11.3665" y1="-1.2065" x2="-6.2865" y2="-1.0795" layer="21"/>
<rectangle x1="-5.6515" y1="-1.2065" x2="-4.5085" y2="-1.0795" layer="21"/>
<rectangle x1="-3.3655" y1="-1.2065" x2="-2.2225" y2="-1.0795" layer="21"/>
<rectangle x1="-0.9525" y1="-1.2065" x2="-0.0635" y2="-1.0795" layer="21"/>
<rectangle x1="0.4445" y1="-1.2065" x2="1.2065" y2="-1.0795" layer="21"/>
<rectangle x1="2.8575" y1="-1.2065" x2="4.2545" y2="-1.0795" layer="21"/>
<rectangle x1="5.0165" y1="-1.2065" x2="6.1595" y2="-1.0795" layer="21"/>
<rectangle x1="7.6835" y1="-1.2065" x2="11.7475" y2="-1.0795" layer="21"/>
<rectangle x1="-11.3665" y1="-1.0795" x2="-6.2865" y2="-0.9525" layer="21"/>
<rectangle x1="-5.6515" y1="-1.0795" x2="-4.6355" y2="-0.9525" layer="21"/>
<rectangle x1="-3.1115" y1="-1.0795" x2="-2.3495" y2="-0.9525" layer="21"/>
<rectangle x1="-0.8255" y1="-1.0795" x2="-0.0635" y2="-0.9525" layer="21"/>
<rectangle x1="0.4445" y1="-1.0795" x2="1.0795" y2="-0.9525" layer="21"/>
<rectangle x1="2.9845" y1="-1.0795" x2="4.2545" y2="-0.9525" layer="21"/>
<rectangle x1="5.0165" y1="-1.0795" x2="6.0325" y2="-0.9525" layer="21"/>
<rectangle x1="7.8105" y1="-1.0795" x2="11.7475" y2="-0.9525" layer="21"/>
<rectangle x1="-11.3665" y1="-0.9525" x2="-6.2865" y2="-0.8255" layer="21"/>
<rectangle x1="-5.6515" y1="-0.9525" x2="-4.7625" y2="-0.8255" layer="21"/>
<rectangle x1="-4.0005" y1="-0.9525" x2="-3.7465" y2="-0.8255" layer="21"/>
<rectangle x1="-3.1115" y1="-0.9525" x2="-2.3495" y2="-0.8255" layer="21"/>
<rectangle x1="-1.7145" y1="-0.9525" x2="-1.3335" y2="-0.8255" layer="21"/>
<rectangle x1="-0.6985" y1="-0.9525" x2="-0.0635" y2="-0.8255" layer="21"/>
<rectangle x1="0.4445" y1="-0.9525" x2="0.9525" y2="-0.8255" layer="21"/>
<rectangle x1="3.1115" y1="-0.9525" x2="4.1275" y2="-0.8255" layer="21"/>
<rectangle x1="5.1435" y1="-0.9525" x2="5.9055" y2="-0.8255" layer="21"/>
<rectangle x1="7.9375" y1="-0.9525" x2="11.8745" y2="-0.8255" layer="21"/>
<rectangle x1="-11.4935" y1="-0.8255" x2="-6.2865" y2="-0.6985" layer="21"/>
<rectangle x1="-5.6515" y1="-0.8255" x2="-4.8895" y2="-0.6985" layer="21"/>
<rectangle x1="-4.2545" y1="-0.8255" x2="-3.4925" y2="-0.6985" layer="21"/>
<rectangle x1="-2.9845" y1="-0.8255" x2="-2.4765" y2="-0.6985" layer="21"/>
<rectangle x1="-1.8415" y1="-0.8255" x2="-1.2065" y2="-0.6985" layer="21"/>
<rectangle x1="-0.5715" y1="-0.8255" x2="-0.0635" y2="-0.6985" layer="21"/>
<rectangle x1="0.4445" y1="-0.8255" x2="0.8255" y2="-0.6985" layer="21"/>
<rectangle x1="3.2385" y1="-0.8255" x2="4.1275" y2="-0.6985" layer="21"/>
<rectangle x1="5.1435" y1="-0.8255" x2="5.7785" y2="-0.6985" layer="21"/>
<rectangle x1="6.7945" y1="-0.8255" x2="6.9215" y2="-0.6985" layer="21"/>
<rectangle x1="8.0645" y1="-0.8255" x2="11.8745" y2="-0.6985" layer="21"/>
<rectangle x1="-11.4935" y1="-0.6985" x2="-6.2865" y2="-0.5715" layer="21"/>
<rectangle x1="-5.6515" y1="-0.6985" x2="-4.8895" y2="-0.5715" layer="21"/>
<rectangle x1="-4.3815" y1="-0.6985" x2="-3.4925" y2="-0.5715" layer="21"/>
<rectangle x1="-2.9845" y1="-0.6985" x2="-2.4765" y2="-0.5715" layer="21"/>
<rectangle x1="-1.9685" y1="-0.6985" x2="-1.0795" y2="-0.5715" layer="21"/>
<rectangle x1="-0.5715" y1="-0.6985" x2="-0.0635" y2="-0.5715" layer="21"/>
<rectangle x1="0.4445" y1="-0.6985" x2="0.6985" y2="-0.5715" layer="21"/>
<rectangle x1="3.3655" y1="-0.6985" x2="4.0005" y2="-0.5715" layer="21"/>
<rectangle x1="5.1435" y1="-0.6985" x2="5.7785" y2="-0.5715" layer="21"/>
<rectangle x1="6.5405" y1="-0.6985" x2="7.3025" y2="-0.5715" layer="21"/>
<rectangle x1="8.0645" y1="-0.6985" x2="11.8745" y2="-0.5715" layer="21"/>
<rectangle x1="-11.4935" y1="-0.5715" x2="-6.2865" y2="-0.4445" layer="21"/>
<rectangle x1="-5.6515" y1="-0.5715" x2="-4.8895" y2="-0.4445" layer="21"/>
<rectangle x1="-4.3815" y1="-0.5715" x2="-2.6035" y2="-0.4445" layer="21"/>
<rectangle x1="-1.9685" y1="-0.5715" x2="-0.0635" y2="-0.4445" layer="21"/>
<rectangle x1="0.4445" y1="-0.5715" x2="0.6985" y2="-0.4445" layer="21"/>
<rectangle x1="1.7145" y1="-0.5715" x2="2.3495" y2="-0.4445" layer="21"/>
<rectangle x1="3.3655" y1="-0.5715" x2="4.0005" y2="-0.4445" layer="21"/>
<rectangle x1="5.2705" y1="-0.5715" x2="7.0485" y2="-0.4445" layer="21"/>
<rectangle x1="8.0645" y1="-0.5715" x2="12.0015" y2="-0.4445" layer="21"/>
<rectangle x1="-11.4935" y1="-0.4445" x2="-6.2865" y2="-0.3175" layer="21"/>
<rectangle x1="-5.6515" y1="-0.4445" x2="-4.8895" y2="-0.3175" layer="21"/>
<rectangle x1="-4.3815" y1="-0.4445" x2="-2.6035" y2="-0.3175" layer="21"/>
<rectangle x1="-2.0955" y1="-0.4445" x2="-0.0635" y2="-0.3175" layer="21"/>
<rectangle x1="0.4445" y1="-0.4445" x2="0.6985" y2="-0.3175" layer="21"/>
<rectangle x1="1.5875" y1="-0.4445" x2="2.4765" y2="-0.3175" layer="21"/>
<rectangle x1="3.3655" y1="-0.4445" x2="4.0005" y2="-0.3175" layer="21"/>
<rectangle x1="5.2705" y1="-0.4445" x2="6.4135" y2="-0.3175" layer="21"/>
<rectangle x1="8.0645" y1="-0.4445" x2="12.0015" y2="-0.3175" layer="21"/>
<rectangle x1="-11.4935" y1="-0.3175" x2="-6.2865" y2="-0.1905" layer="21"/>
<rectangle x1="-5.6515" y1="-0.3175" x2="-4.8895" y2="-0.1905" layer="21"/>
<rectangle x1="-4.3815" y1="-0.3175" x2="-2.6035" y2="-0.1905" layer="21"/>
<rectangle x1="-2.0955" y1="-0.3175" x2="-0.0635" y2="-0.1905" layer="21"/>
<rectangle x1="0.4445" y1="-0.3175" x2="1.3335" y2="-0.1905" layer="21"/>
<rectangle x1="1.5875" y1="-0.3175" x2="2.3495" y2="-0.1905" layer="21"/>
<rectangle x1="3.3655" y1="-0.3175" x2="3.8735" y2="-0.1905" layer="21"/>
<rectangle x1="5.3975" y1="-0.3175" x2="6.1595" y2="-0.1905" layer="21"/>
<rectangle x1="8.0645" y1="-0.3175" x2="12.0015" y2="-0.1905" layer="21"/>
<rectangle x1="-11.4935" y1="-0.1905" x2="-6.2865" y2="-0.0635" layer="21"/>
<rectangle x1="-5.6515" y1="-0.1905" x2="-5.0165" y2="-0.0635" layer="21"/>
<rectangle x1="-2.8575" y1="-0.1905" x2="-2.6035" y2="-0.0635" layer="21"/>
<rectangle x1="-0.5715" y1="-0.1905" x2="-0.0635" y2="-0.0635" layer="21"/>
<rectangle x1="0.4445" y1="-0.1905" x2="1.9685" y2="-0.0635" layer="21"/>
<rectangle x1="3.3655" y1="-0.1905" x2="3.8735" y2="-0.0635" layer="21"/>
<rectangle x1="5.3975" y1="-0.1905" x2="6.0325" y2="-0.0635" layer="21"/>
<rectangle x1="7.9375" y1="-0.1905" x2="12.0015" y2="-0.0635" layer="21"/>
<rectangle x1="-11.4935" y1="-0.0635" x2="-6.2865" y2="0.0635" layer="21"/>
<rectangle x1="-5.6515" y1="-0.0635" x2="-4.8895" y2="0.0635" layer="21"/>
<rectangle x1="-2.8575" y1="-0.0635" x2="-2.6035" y2="0.0635" layer="21"/>
<rectangle x1="-0.5715" y1="-0.0635" x2="-0.0635" y2="0.0635" layer="21"/>
<rectangle x1="0.4445" y1="-0.0635" x2="1.4605" y2="0.0635" layer="21"/>
<rectangle x1="3.3655" y1="-0.0635" x2="3.8735" y2="0.0635" layer="21"/>
<rectangle x1="5.3975" y1="-0.0635" x2="5.9055" y2="0.0635" layer="21"/>
<rectangle x1="7.8105" y1="-0.0635" x2="12.0015" y2="0.0635" layer="21"/>
<rectangle x1="-11.3665" y1="0.0635" x2="-6.2865" y2="0.1905" layer="21"/>
<rectangle x1="-5.6515" y1="0.0635" x2="-4.8895" y2="0.1905" layer="21"/>
<rectangle x1="-2.8575" y1="0.0635" x2="-2.6035" y2="0.1905" layer="21"/>
<rectangle x1="-0.5715" y1="0.0635" x2="-0.0635" y2="0.1905" layer="21"/>
<rectangle x1="0.4445" y1="0.0635" x2="1.2065" y2="0.1905" layer="21"/>
<rectangle x1="3.2385" y1="0.0635" x2="3.7465" y2="0.1905" layer="21"/>
<rectangle x1="4.6355" y1="0.0635" x2="4.8895" y2="0.1905" layer="21"/>
<rectangle x1="5.5245" y1="0.0635" x2="5.9055" y2="0.1905" layer="21"/>
<rectangle x1="7.6835" y1="0.0635" x2="12.0015" y2="0.1905" layer="21"/>
<rectangle x1="-11.3665" y1="0.1905" x2="-6.2865" y2="0.3175" layer="21"/>
<rectangle x1="-5.6515" y1="0.1905" x2="-4.8895" y2="0.3175" layer="21"/>
<rectangle x1="-4.3815" y1="0.1905" x2="-3.4925" y2="0.3175" layer="21"/>
<rectangle x1="-2.9845" y1="0.1905" x2="-2.6035" y2="0.3175" layer="21"/>
<rectangle x1="-2.0955" y1="0.1905" x2="-1.0795" y2="0.3175" layer="21"/>
<rectangle x1="-0.5715" y1="0.1905" x2="-0.0635" y2="0.3175" layer="21"/>
<rectangle x1="0.4445" y1="0.1905" x2="1.0795" y2="0.3175" layer="21"/>
<rectangle x1="3.2385" y1="0.1905" x2="3.7465" y2="0.3175" layer="21"/>
<rectangle x1="4.6355" y1="0.1905" x2="4.8895" y2="0.3175" layer="21"/>
<rectangle x1="5.5245" y1="0.1905" x2="5.9055" y2="0.3175" layer="21"/>
<rectangle x1="7.1755" y1="0.1905" x2="8.4455" y2="0.3175" layer="21"/>
<rectangle x1="8.6995" y1="0.1905" x2="9.0805" y2="0.3175" layer="21"/>
<rectangle x1="9.3345" y1="0.1905" x2="11.8745" y2="0.3175" layer="21"/>
<rectangle x1="-11.3665" y1="0.3175" x2="-6.2865" y2="0.4445" layer="21"/>
<rectangle x1="-5.6515" y1="0.3175" x2="-4.8895" y2="0.4445" layer="21"/>
<rectangle x1="-4.3815" y1="0.3175" x2="-3.4925" y2="0.4445" layer="21"/>
<rectangle x1="-2.9845" y1="0.3175" x2="-2.4765" y2="0.4445" layer="21"/>
<rectangle x1="-1.9685" y1="0.3175" x2="-1.0795" y2="0.4445" layer="21"/>
<rectangle x1="-0.5715" y1="0.3175" x2="-0.0635" y2="0.4445" layer="21"/>
<rectangle x1="0.4445" y1="0.3175" x2="0.9525" y2="0.4445" layer="21"/>
<rectangle x1="3.1115" y1="0.3175" x2="3.7465" y2="0.4445" layer="21"/>
<rectangle x1="4.6355" y1="0.3175" x2="4.8895" y2="0.4445" layer="21"/>
<rectangle x1="5.5245" y1="0.3175" x2="5.9055" y2="0.4445" layer="21"/>
<rectangle x1="6.6675" y1="0.3175" x2="7.3025" y2="0.4445" layer="21"/>
<rectangle x1="7.4295" y1="0.3175" x2="8.4455" y2="0.4445" layer="21"/>
<rectangle x1="9.3345" y1="0.3175" x2="11.8745" y2="0.4445" layer="21"/>
<rectangle x1="-11.2395" y1="0.4445" x2="-6.2865" y2="0.5715" layer="21"/>
<rectangle x1="-5.6515" y1="0.4445" x2="-4.7625" y2="0.5715" layer="21"/>
<rectangle x1="-4.2545" y1="0.4445" x2="-3.4925" y2="0.5715" layer="21"/>
<rectangle x1="-2.9845" y1="0.4445" x2="-2.4765" y2="0.5715" layer="21"/>
<rectangle x1="-1.9685" y1="0.4445" x2="-1.2065" y2="0.5715" layer="21"/>
<rectangle x1="-0.6985" y1="0.4445" x2="-0.0635" y2="0.5715" layer="21"/>
<rectangle x1="0.4445" y1="0.4445" x2="0.8255" y2="0.5715" layer="21"/>
<rectangle x1="2.9845" y1="0.4445" x2="3.6195" y2="0.5715" layer="21"/>
<rectangle x1="4.5085" y1="0.4445" x2="5.0165" y2="0.5715" layer="21"/>
<rectangle x1="5.6515" y1="0.4445" x2="5.9055" y2="0.5715" layer="21"/>
<rectangle x1="6.6675" y1="0.4445" x2="7.0485" y2="0.5715" layer="21"/>
<rectangle x1="7.9375" y1="0.4445" x2="8.4455" y2="0.5715" layer="21"/>
<rectangle x1="9.3345" y1="0.4445" x2="11.7475" y2="0.5715" layer="21"/>
<rectangle x1="-11.2395" y1="0.5715" x2="-6.2865" y2="0.6985" layer="21"/>
<rectangle x1="-5.6515" y1="0.5715" x2="-4.7625" y2="0.6985" layer="21"/>
<rectangle x1="-4.1275" y1="0.5715" x2="-3.7465" y2="0.6985" layer="21"/>
<rectangle x1="-3.1115" y1="0.5715" x2="-2.3495" y2="0.6985" layer="21"/>
<rectangle x1="-1.8415" y1="0.5715" x2="-1.3335" y2="0.6985" layer="21"/>
<rectangle x1="-0.6985" y1="0.5715" x2="-0.0635" y2="0.6985" layer="21"/>
<rectangle x1="0.4445" y1="0.5715" x2="0.8255" y2="0.6985" layer="21"/>
<rectangle x1="2.6035" y1="0.5715" x2="3.6195" y2="0.6985" layer="21"/>
<rectangle x1="4.5085" y1="0.5715" x2="5.0165" y2="0.6985" layer="21"/>
<rectangle x1="5.6515" y1="0.5715" x2="5.9055" y2="0.6985" layer="21"/>
<rectangle x1="7.9375" y1="0.5715" x2="8.5725" y2="0.6985" layer="21"/>
<rectangle x1="9.3345" y1="0.5715" x2="11.7475" y2="0.6985" layer="21"/>
<rectangle x1="-11.1125" y1="0.6985" x2="-6.2865" y2="0.8255" layer="21"/>
<rectangle x1="-5.6515" y1="0.6985" x2="-4.6355" y2="0.8255" layer="21"/>
<rectangle x1="-3.1115" y1="0.6985" x2="-2.3495" y2="0.8255" layer="21"/>
<rectangle x1="-0.8255" y1="0.6985" x2="-0.0635" y2="0.8255" layer="21"/>
<rectangle x1="0.4445" y1="0.6985" x2="0.8255" y2="0.8255" layer="21"/>
<rectangle x1="2.2225" y1="0.6985" x2="3.4925" y2="0.8255" layer="21"/>
<rectangle x1="4.5085" y1="0.6985" x2="5.0165" y2="0.8255" layer="21"/>
<rectangle x1="5.6515" y1="0.6985" x2="6.0325" y2="0.8255" layer="21"/>
<rectangle x1="7.8105" y1="0.6985" x2="8.4455" y2="0.8255" layer="21"/>
<rectangle x1="9.3345" y1="0.6985" x2="11.6205" y2="0.8255" layer="21"/>
<rectangle x1="-11.1125" y1="0.8255" x2="-6.2865" y2="0.9525" layer="21"/>
<rectangle x1="-5.6515" y1="0.8255" x2="-4.5085" y2="0.9525" layer="21"/>
<rectangle x1="-3.2385" y1="0.8255" x2="-2.0955" y2="0.9525" layer="21"/>
<rectangle x1="-0.9525" y1="0.8255" x2="-0.0635" y2="0.9525" layer="21"/>
<rectangle x1="0.4445" y1="0.8255" x2="0.8255" y2="0.9525" layer="21"/>
<rectangle x1="1.8415" y1="0.8255" x2="3.4925" y2="0.9525" layer="21"/>
<rectangle x1="4.3815" y1="0.8255" x2="5.1435" y2="0.9525" layer="21"/>
<rectangle x1="5.7785" y1="0.8255" x2="6.1595" y2="0.9525" layer="21"/>
<rectangle x1="7.6835" y1="0.8255" x2="8.3185" y2="0.9525" layer="21"/>
<rectangle x1="9.5885" y1="0.8255" x2="11.6205" y2="0.9525" layer="21"/>
<rectangle x1="-10.9855" y1="0.9525" x2="-6.2865" y2="1.0795" layer="21"/>
<rectangle x1="-5.6515" y1="0.9525" x2="-4.2545" y2="1.0795" layer="21"/>
<rectangle x1="-3.4925" y1="0.9525" x2="-1.9685" y2="1.0795" layer="21"/>
<rectangle x1="-1.0795" y1="0.9525" x2="-0.0635" y2="1.0795" layer="21"/>
<rectangle x1="0.4445" y1="0.9525" x2="0.8255" y2="1.0795" layer="21"/>
<rectangle x1="1.7145" y1="0.9525" x2="2.4765" y2="1.0795" layer="21"/>
<rectangle x1="2.9845" y1="0.9525" x2="3.4925" y2="1.0795" layer="21"/>
<rectangle x1="4.3815" y1="0.9525" x2="5.1435" y2="1.0795" layer="21"/>
<rectangle x1="5.7785" y1="0.9525" x2="6.4135" y2="1.0795" layer="21"/>
<rectangle x1="7.4295" y1="0.9525" x2="8.1915" y2="1.0795" layer="21"/>
<rectangle x1="9.5885" y1="0.9525" x2="11.4935" y2="1.0795" layer="21"/>
<rectangle x1="-10.8585" y1="1.0795" x2="-6.2865" y2="1.2065" layer="21"/>
<rectangle x1="-5.6515" y1="1.0795" x2="-0.0635" y2="1.2065" layer="21"/>
<rectangle x1="0.4445" y1="1.0795" x2="0.8255" y2="1.2065" layer="21"/>
<rectangle x1="1.8415" y1="1.0795" x2="2.2225" y2="1.2065" layer="21"/>
<rectangle x1="3.2385" y1="1.0795" x2="8.6995" y2="1.2065" layer="21"/>
<rectangle x1="9.2075" y1="1.0795" x2="11.4935" y2="1.2065" layer="21"/>
<rectangle x1="-10.7315" y1="1.2065" x2="-6.2865" y2="1.3335" layer="21"/>
<rectangle x1="-5.6515" y1="1.2065" x2="-0.0635" y2="1.3335" layer="21"/>
<rectangle x1="0.4445" y1="1.2065" x2="0.9525" y2="1.3335" layer="21"/>
<rectangle x1="3.2385" y1="1.2065" x2="8.6995" y2="1.3335" layer="21"/>
<rectangle x1="9.0805" y1="1.2065" x2="11.3665" y2="1.3335" layer="21"/>
<rectangle x1="-10.6045" y1="1.3335" x2="-7.0485" y2="1.4605" layer="21"/>
<rectangle x1="-4.8895" y1="1.3335" x2="-0.0635" y2="1.4605" layer="21"/>
<rectangle x1="0.4445" y1="1.3335" x2="0.9525" y2="1.4605" layer="21"/>
<rectangle x1="3.1115" y1="1.3335" x2="8.8265" y2="1.4605" layer="21"/>
<rectangle x1="9.0805" y1="1.3335" x2="11.2395" y2="1.4605" layer="21"/>
<rectangle x1="-10.4775" y1="1.4605" x2="-7.0485" y2="1.5875" layer="21"/>
<rectangle x1="-4.8895" y1="1.4605" x2="-0.0635" y2="1.5875" layer="21"/>
<rectangle x1="0.4445" y1="1.4605" x2="1.0795" y2="1.5875" layer="21"/>
<rectangle x1="3.1115" y1="1.4605" x2="8.8265" y2="1.5875" layer="21"/>
<rectangle x1="8.9535" y1="1.4605" x2="11.1125" y2="1.5875" layer="21"/>
<rectangle x1="-10.2235" y1="1.5875" x2="-7.0485" y2="1.7145" layer="21"/>
<rectangle x1="-4.8895" y1="1.5875" x2="-0.0635" y2="1.7145" layer="21"/>
<rectangle x1="0.4445" y1="1.5875" x2="1.2065" y2="1.7145" layer="21"/>
<rectangle x1="2.9845" y1="1.5875" x2="10.9855" y2="1.7145" layer="21"/>
<rectangle x1="-10.0965" y1="1.7145" x2="-7.0485" y2="1.8415" layer="21"/>
<rectangle x1="-4.8895" y1="1.7145" x2="-0.0635" y2="1.8415" layer="21"/>
<rectangle x1="0.4445" y1="1.7145" x2="1.4605" y2="1.8415" layer="21"/>
<rectangle x1="2.7305" y1="1.7145" x2="10.8585" y2="1.8415" layer="21"/>
<rectangle x1="-9.9695" y1="1.8415" x2="10.7315" y2="1.9685" layer="21"/>
<rectangle x1="-9.7155" y1="1.9685" x2="10.6045" y2="2.0955" layer="21"/>
<rectangle x1="-9.4615" y1="2.0955" x2="10.3505" y2="2.2225" layer="21"/>
<rectangle x1="-9.0805" y1="2.2225" x2="10.2235" y2="2.3495" layer="21"/>
<rectangle x1="-8.8265" y1="2.3495" x2="9.9695" y2="2.4765" layer="21"/>
<rectangle x1="-8.4455" y1="2.4765" x2="9.8425" y2="2.6035" layer="21"/>
<rectangle x1="-8.0645" y1="2.6035" x2="9.5885" y2="2.7305" layer="21"/>
<rectangle x1="-7.6835" y1="2.7305" x2="9.3345" y2="2.8575" layer="21"/>
<rectangle x1="-7.1755" y1="2.8575" x2="9.0805" y2="2.9845" layer="21"/>
<rectangle x1="-6.7945" y1="2.9845" x2="8.8265" y2="3.1115" layer="21"/>
<rectangle x1="-6.4135" y1="3.1115" x2="8.6995" y2="3.2385" layer="21"/>
<rectangle x1="-5.9055" y1="3.2385" x2="8.4455" y2="3.3655" layer="21"/>
<rectangle x1="-5.5245" y1="3.3655" x2="8.1915" y2="3.4925" layer="21"/>
<rectangle x1="-5.0165" y1="3.4925" x2="7.9375" y2="3.6195" layer="21"/>
<rectangle x1="-4.6355" y1="3.6195" x2="7.6835" y2="3.7465" layer="21"/>
<rectangle x1="-4.2545" y1="3.7465" x2="7.4295" y2="3.8735" layer="21"/>
<rectangle x1="-3.7465" y1="3.8735" x2="7.3025" y2="4.0005" layer="21"/>
<rectangle x1="-3.2385" y1="4.0005" x2="7.0485" y2="4.1275" layer="21"/>
<rectangle x1="-2.8575" y1="4.1275" x2="6.7945" y2="4.2545" layer="21"/>
<rectangle x1="-2.3495" y1="4.2545" x2="6.5405" y2="4.3815" layer="21"/>
<rectangle x1="-1.9685" y1="4.3815" x2="6.2865" y2="4.5085" layer="21"/>
<rectangle x1="-1.4605" y1="4.5085" x2="6.0325" y2="4.6355" layer="21"/>
<rectangle x1="-1.0795" y1="4.6355" x2="5.7785" y2="4.7625" layer="21"/>
<rectangle x1="-0.6985" y1="4.7625" x2="5.3975" y2="4.8895" layer="21"/>
<rectangle x1="-0.1905" y1="4.8895" x2="5.0165" y2="5.0165" layer="21"/>
<rectangle x1="0.3175" y1="5.0165" x2="4.6355" y2="5.1435" layer="21"/>
<rectangle x1="0.8255" y1="5.1435" x2="4.1275" y2="5.2705" layer="21"/>
<rectangle x1="1.5875" y1="5.2705" x2="3.4925" y2="5.3975" layer="21"/>
</package>
<package name="TEELSYS_LOGO_ART_BOTTOM_2">
<rectangle x1="3.6195" y1="-5.1435" x2="5.2705" y2="-5.0165" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="-5.0165" x2="5.7785" y2="-4.8895" layer="22" rot="R180"/>
<rectangle x1="1.8415" y1="-4.8895" x2="6.2865" y2="-4.7625" layer="22" rot="R180"/>
<rectangle x1="1.0795" y1="-4.7625" x2="6.7945" y2="-4.6355" layer="22" rot="R180"/>
<rectangle x1="0.1905" y1="-4.6355" x2="7.0485" y2="-4.5085" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="-4.5085" x2="7.4295" y2="-4.3815" layer="22" rot="R180"/>
<rectangle x1="-1.3335" y1="-4.3815" x2="7.6835" y2="-4.2545" layer="22" rot="R180"/>
<rectangle x1="-1.9685" y1="-4.2545" x2="7.9375" y2="-4.1275" layer="22" rot="R180"/>
<rectangle x1="-2.7305" y1="-4.1275" x2="8.1915" y2="-4.0005" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="-4.0005" x2="8.4455" y2="-3.8735" layer="22" rot="R180"/>
<rectangle x1="-4.0005" y1="-3.8735" x2="8.5725" y2="-3.7465" layer="22" rot="R180"/>
<rectangle x1="-4.6355" y1="-3.7465" x2="8.8265" y2="-3.6195" layer="22" rot="R180"/>
<rectangle x1="-5.2705" y1="-3.6195" x2="8.9535" y2="-3.4925" layer="22" rot="R180"/>
<rectangle x1="-5.9055" y1="-3.4925" x2="9.2075" y2="-3.3655" layer="22" rot="R180"/>
<rectangle x1="-6.4135" y1="-3.3655" x2="9.3345" y2="-3.2385" layer="22" rot="R180"/>
<rectangle x1="-7.0485" y1="-3.2385" x2="9.5885" y2="-3.1115" layer="22" rot="R180"/>
<rectangle x1="-7.5565" y1="-3.1115" x2="9.7155" y2="-2.9845" layer="22" rot="R180"/>
<rectangle x1="-8.0645" y1="-2.9845" x2="9.8425" y2="-2.8575" layer="22" rot="R180"/>
<rectangle x1="-8.4455" y1="-2.8575" x2="10.0965" y2="-2.7305" layer="22" rot="R180"/>
<rectangle x1="-8.9535" y1="-2.7305" x2="10.2235" y2="-2.6035" layer="22" rot="R180"/>
<rectangle x1="8.1915" y1="-2.6035" x2="10.3505" y2="-2.4765" layer="22" rot="R180"/>
<rectangle x1="-9.3345" y1="-2.6035" x2="7.5565" y2="-2.4765" layer="22" rot="R180"/>
<rectangle x1="8.3185" y1="-2.4765" x2="10.4775" y2="-2.3495" layer="22" rot="R180"/>
<rectangle x1="-9.7155" y1="-2.4765" x2="7.4295" y2="-2.3495" layer="22" rot="R180"/>
<rectangle x1="8.4455" y1="-2.3495" x2="10.6045" y2="-2.2225" layer="22" rot="R180"/>
<rectangle x1="-9.9695" y1="-2.3495" x2="7.4295" y2="-2.2225" layer="22" rot="R180"/>
<rectangle x1="8.4455" y1="-2.2225" x2="10.8585" y2="-2.0955" layer="22" rot="R180"/>
<rectangle x1="-10.2235" y1="-2.2225" x2="-0.1905" y2="-2.0955" layer="22" rot="R180"/>
<rectangle x1="8.4455" y1="-2.0955" x2="10.8585" y2="-1.9685" layer="22" rot="R180"/>
<rectangle x1="-10.4775" y1="-2.0955" x2="-0.1905" y2="-1.9685" layer="22" rot="R180"/>
<rectangle x1="8.4455" y1="-1.9685" x2="10.9855" y2="-1.8415" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="-1.9685" x2="7.3025" y2="-1.8415" layer="22" rot="R180"/>
<rectangle x1="-10.6045" y1="-1.9685" x2="-4.1275" y2="-1.8415" layer="22" rot="R180"/>
<rectangle x1="8.4455" y1="-1.8415" x2="11.1125" y2="-1.7145" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="-1.8415" x2="7.4295" y2="-1.7145" layer="22" rot="R180"/>
<rectangle x1="-10.8585" y1="-1.8415" x2="-4.3815" y2="-1.7145" layer="22" rot="R180"/>
<rectangle x1="8.3185" y1="-1.7145" x2="11.2395" y2="-1.5875" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="-1.7145" x2="7.5565" y2="-1.5875" layer="22" rot="R180"/>
<rectangle x1="-10.9855" y1="-1.7145" x2="-4.5085" y2="-1.5875" layer="22" rot="R180"/>
<rectangle x1="8.1915" y1="-1.5875" x2="11.2395" y2="-1.4605" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="-1.5875" x2="7.6835" y2="-1.4605" layer="22" rot="R180"/>
<rectangle x1="-11.1125" y1="-1.5875" x2="-4.6355" y2="-1.4605" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="-1.4605" x2="11.3665" y2="-1.3335" layer="22" rot="R180"/>
<rectangle x1="-11.2395" y1="-1.4605" x2="-4.6355" y2="-1.3335" layer="22" rot="R180"/>
<rectangle x1="-3.8735" y1="-1.3335" x2="11.4935" y2="-1.2065" layer="22" rot="R180"/>
<rectangle x1="-11.2395" y1="-1.3335" x2="-4.6355" y2="-1.2065" layer="22" rot="R180"/>
<rectangle x1="6.4135" y1="-1.2065" x2="11.4935" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="4.5085" y1="-1.2065" x2="5.9055" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="2.2225" y1="-1.2065" x2="3.7465" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-1.2065" x2="1.4605" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="-1.2065" y1="-1.2065" x2="-0.0635" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="-4.0005" y1="-1.2065" x2="-2.2225" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="-6.1595" y1="-1.2065" x2="-4.7625" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="-11.3665" y1="-1.2065" x2="-7.1755" y2="-1.0795" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-1.0795" x2="11.6205" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="4.7625" y1="-1.0795" x2="5.9055" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="2.4765" y1="-1.0795" x2="3.6195" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-1.0795" x2="1.2065" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="-0.9525" y1="-1.0795" x2="-0.1905" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="-4.0005" y1="-1.0795" x2="-2.6035" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="-5.9055" y1="-1.0795" x2="-4.7625" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="-11.4935" y1="-1.0795" x2="-7.4295" y2="-0.9525" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.9525" x2="11.6205" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="4.8895" y1="-0.9525" x2="5.9055" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="2.6035" y1="-0.9525" x2="3.3655" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.9525" x2="1.0795" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="-0.8255" y1="-0.9525" x2="-0.1905" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="-4.0005" y1="-0.9525" x2="-2.7305" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="-5.7785" y1="-0.9525" x2="-4.7625" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="-11.4935" y1="-0.9525" x2="-7.5565" y2="-0.8255" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.8255" x2="11.6205" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="5.0165" y1="-0.8255" x2="5.9055" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="4.0005" y1="-0.8255" x2="4.2545" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="2.6035" y1="-0.8255" x2="3.3655" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="1.5875" y1="-0.8255" x2="1.9685" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.8255" x2="0.9525" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="-0.6985" y1="-0.8255" x2="-0.1905" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="-3.8735" y1="-0.8255" x2="-2.8575" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="-5.6515" y1="-0.8255" x2="-4.8895" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="-11.6205" y1="-0.8255" x2="-7.6835" y2="-0.6985" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.6985" x2="11.7475" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="-0.6985" x2="5.9055" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="3.7465" y1="-0.6985" x2="4.5085" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="2.7305" y1="-0.6985" x2="3.2385" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="1.4605" y1="-0.6985" x2="2.0955" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.6985" x2="0.8255" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="-0.6985" x2="-0.1905" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="-3.8735" y1="-0.6985" x2="-2.9845" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="-5.5245" y1="-0.6985" x2="-4.8895" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="-6.6675" y1="-0.6985" x2="-6.5405" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="-11.6205" y1="-0.6985" x2="-7.8105" y2="-0.5715" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.5715" x2="11.7475" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="-0.5715" x2="5.9055" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="3.7465" y1="-0.5715" x2="4.6355" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="2.7305" y1="-0.5715" x2="3.2385" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="1.3335" y1="-0.5715" x2="2.2225" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.5715" x2="0.8255" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="-0.4445" y1="-0.5715" x2="-0.1905" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="-3.7465" y1="-0.5715" x2="-3.1115" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="-5.5245" y1="-0.5715" x2="-4.8895" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="-7.0485" y1="-0.5715" x2="-6.2865" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="-11.6205" y1="-0.5715" x2="-7.8105" y2="-0.4445" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.4445" x2="11.7475" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="-0.4445" x2="5.9055" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="-0.4445" x2="4.6355" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.4445" x2="2.2225" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="-0.4445" y1="-0.4445" x2="-0.1905" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="-2.0955" y1="-0.4445" x2="-1.4605" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="-3.7465" y1="-0.4445" x2="-3.1115" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="-6.7945" y1="-0.4445" x2="-5.0165" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="-11.7475" y1="-0.4445" x2="-7.8105" y2="-0.3175" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.3175" x2="11.7475" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="-0.3175" x2="5.9055" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="-0.3175" x2="4.6355" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.3175" x2="2.3495" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="-0.4445" y1="-0.3175" x2="-0.1905" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="-2.2225" y1="-0.3175" x2="-1.3335" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="-3.7465" y1="-0.3175" x2="-3.1115" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="-6.1595" y1="-0.3175" x2="-5.0165" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="-11.7475" y1="-0.3175" x2="-7.8105" y2="-0.1905" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.1905" x2="11.7475" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="-0.1905" x2="5.9055" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="-0.1905" x2="4.6355" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.1905" x2="2.3495" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="-1.0795" y1="-0.1905" x2="-0.1905" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="-2.0955" y1="-0.1905" x2="-1.3335" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="-3.6195" y1="-0.1905" x2="-3.1115" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="-5.9055" y1="-0.1905" x2="-5.1435" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="-11.7475" y1="-0.1905" x2="-7.8105" y2="-0.0635" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="-0.0635" x2="11.7475" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="5.2705" y1="-0.0635" x2="5.9055" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="-0.0635" x2="3.1115" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="-0.0635" x2="0.8255" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="-1.7145" y1="-0.0635" x2="-0.1905" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="-3.6195" y1="-0.0635" x2="-3.1115" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="-5.7785" y1="-0.0635" x2="-5.1435" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="-11.7475" y1="-0.0635" x2="-7.6835" y2="0.0635" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.0635" x2="11.7475" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="0.0635" x2="5.9055" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="0.0635" x2="3.1115" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.0635" x2="0.8255" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="-1.2065" y1="0.0635" x2="-0.1905" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="-3.6195" y1="0.0635" x2="-3.1115" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="-5.6515" y1="0.0635" x2="-5.1435" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="-11.7475" y1="0.0635" x2="-7.5565" y2="0.1905" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.1905" x2="11.6205" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="0.1905" x2="5.9055" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="0.1905" x2="3.1115" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.1905" x2="0.8255" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="-0.9525" y1="0.1905" x2="-0.1905" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="-3.4925" y1="0.1905" x2="-2.9845" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="-4.6355" y1="0.1905" x2="-4.3815" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="-5.6515" y1="0.1905" x2="-5.2705" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="-11.7475" y1="0.1905" x2="-7.4295" y2="0.3175" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.3175" x2="11.6205" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="0.3175" x2="5.9055" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="3.7465" y1="0.3175" x2="4.6355" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="2.8575" y1="0.3175" x2="3.2385" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="1.3335" y1="0.3175" x2="2.3495" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.3175" x2="0.8255" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="-0.8255" y1="0.3175" x2="-0.1905" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="-3.4925" y1="0.3175" x2="-2.9845" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="-4.6355" y1="0.3175" x2="-4.3815" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="-5.6515" y1="0.3175" x2="-5.2705" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="-8.1915" y1="0.3175" x2="-6.9215" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="-8.8265" y1="0.3175" x2="-8.4455" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="-11.6205" y1="0.3175" x2="-9.0805" y2="0.4445" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.4445" x2="11.6205" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="5.1435" y1="0.4445" x2="5.9055" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="3.7465" y1="0.4445" x2="4.6355" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="2.7305" y1="0.4445" x2="3.2385" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="1.3335" y1="0.4445" x2="2.2225" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.4445" x2="0.8255" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="-0.6985" y1="0.4445" x2="-0.1905" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="-3.4925" y1="0.4445" x2="-2.8575" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="-4.6355" y1="0.4445" x2="-4.3815" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="-5.6515" y1="0.4445" x2="-5.2705" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="-7.0485" y1="0.4445" x2="-6.4135" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="-8.1915" y1="0.4445" x2="-7.1755" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="-11.6205" y1="0.4445" x2="-9.0805" y2="0.5715" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.5715" x2="11.4935" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="5.0165" y1="0.5715" x2="5.9055" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="3.7465" y1="0.5715" x2="4.5085" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="2.7305" y1="0.5715" x2="3.2385" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="1.4605" y1="0.5715" x2="2.2225" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.5715" x2="0.9525" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="0.5715" x2="-0.1905" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="0.5715" x2="-2.7305" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="-4.7625" y1="0.5715" x2="-4.2545" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="-5.6515" y1="0.5715" x2="-5.3975" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="-6.7945" y1="0.5715" x2="-6.4135" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="-8.1915" y1="0.5715" x2="-7.6835" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="-11.4935" y1="0.5715" x2="-9.0805" y2="0.6985" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.6985" x2="11.4935" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="5.0165" y1="0.6985" x2="5.9055" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="4.0005" y1="0.6985" x2="4.3815" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="2.6035" y1="0.6985" x2="3.3655" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="1.5875" y1="0.6985" x2="2.0955" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.6985" x2="0.9525" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="0.6985" x2="-0.1905" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="-3.3655" y1="0.6985" x2="-2.3495" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="-4.7625" y1="0.6985" x2="-4.2545" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="-5.6515" y1="0.6985" x2="-5.3975" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="-8.3185" y1="0.6985" x2="-7.6835" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="-11.4935" y1="0.6985" x2="-9.0805" y2="0.8255" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.8255" x2="11.3665" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="4.8895" y1="0.8255" x2="5.9055" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="2.6035" y1="0.8255" x2="3.3655" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.8255" x2="1.0795" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="0.8255" x2="-0.1905" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="-3.2385" y1="0.8255" x2="-1.9685" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="-4.7625" y1="0.8255" x2="-4.2545" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="-5.7785" y1="0.8255" x2="-5.3975" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="-8.1915" y1="0.8255" x2="-7.5565" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="-11.3665" y1="0.8255" x2="-9.0805" y2="0.9525" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="0.9525" x2="11.3665" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="4.7625" y1="0.9525" x2="5.9055" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="2.3495" y1="0.9525" x2="3.4925" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="0.9525" x2="1.2065" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="0.9525" x2="-0.1905" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="-3.2385" y1="0.9525" x2="-1.5875" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="-4.8895" y1="0.9525" x2="-4.1275" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="-5.9055" y1="0.9525" x2="-5.5245" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="-8.0645" y1="0.9525" x2="-7.4295" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="-11.3665" y1="0.9525" x2="-9.3345" y2="1.0795" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="1.0795" x2="11.2395" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="4.5085" y1="1.0795" x2="5.9055" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="2.2225" y1="1.0795" x2="3.7465" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="1.0795" x2="1.3335" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="1.0795" x2="-0.1905" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="-2.2225" y1="1.0795" x2="-1.4605" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="-3.2385" y1="1.0795" x2="-2.7305" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="-4.8895" y1="1.0795" x2="-4.1275" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="-6.1595" y1="1.0795" x2="-5.5245" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="-7.9375" y1="1.0795" x2="-7.1755" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="-11.2395" y1="1.0795" x2="-9.3345" y2="1.2065" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="1.2065" x2="11.1125" y2="1.3335" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="1.2065" x2="5.9055" y2="1.3335" layer="22" rot="R180"/>
<rectangle x1="-0.5715" y1="1.2065" x2="-0.1905" y2="1.3335" layer="22" rot="R180"/>
<rectangle x1="-1.9685" y1="1.2065" x2="-1.5875" y2="1.3335" layer="22" rot="R180"/>
<rectangle x1="-8.4455" y1="1.2065" x2="-2.9845" y2="1.3335" layer="22" rot="R180"/>
<rectangle x1="-11.2395" y1="1.2065" x2="-8.9535" y2="1.3335" layer="22" rot="R180"/>
<rectangle x1="6.5405" y1="1.3335" x2="10.9855" y2="1.4605" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="1.3335" x2="5.9055" y2="1.4605" layer="22" rot="R180"/>
<rectangle x1="-0.6985" y1="1.3335" x2="-0.1905" y2="1.4605" layer="22" rot="R180"/>
<rectangle x1="-8.4455" y1="1.3335" x2="-2.9845" y2="1.4605" layer="22" rot="R180"/>
<rectangle x1="-11.1125" y1="1.3335" x2="-8.8265" y2="1.4605" layer="22" rot="R180"/>
<rectangle x1="7.3025" y1="1.4605" x2="10.8585" y2="1.5875" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="1.4605" x2="5.1435" y2="1.5875" layer="22" rot="R180"/>
<rectangle x1="-0.6985" y1="1.4605" x2="-0.1905" y2="1.5875" layer="22" rot="R180"/>
<rectangle x1="-8.5725" y1="1.4605" x2="-2.8575" y2="1.5875" layer="22" rot="R180"/>
<rectangle x1="-10.9855" y1="1.4605" x2="-8.8265" y2="1.5875" layer="22" rot="R180"/>
<rectangle x1="7.3025" y1="1.5875" x2="10.7315" y2="1.7145" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="1.5875" x2="5.1435" y2="1.7145" layer="22" rot="R180"/>
<rectangle x1="-0.8255" y1="1.5875" x2="-0.1905" y2="1.7145" layer="22" rot="R180"/>
<rectangle x1="-8.5725" y1="1.5875" x2="-2.8575" y2="1.7145" layer="22" rot="R180"/>
<rectangle x1="-10.8585" y1="1.5875" x2="-8.6995" y2="1.7145" layer="22" rot="R180"/>
<rectangle x1="7.3025" y1="1.7145" x2="10.4775" y2="1.8415" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="1.7145" x2="5.1435" y2="1.8415" layer="22" rot="R180"/>
<rectangle x1="-0.9525" y1="1.7145" x2="-0.1905" y2="1.8415" layer="22" rot="R180"/>
<rectangle x1="-10.7315" y1="1.7145" x2="-2.7305" y2="1.8415" layer="22" rot="R180"/>
<rectangle x1="7.3025" y1="1.8415" x2="10.3505" y2="1.9685" layer="22" rot="R180"/>
<rectangle x1="0.3175" y1="1.8415" x2="5.1435" y2="1.9685" layer="22" rot="R180"/>
<rectangle x1="-1.2065" y1="1.8415" x2="-0.1905" y2="1.9685" layer="22" rot="R180"/>
<rectangle x1="-10.6045" y1="1.8415" x2="-2.4765" y2="1.9685" layer="22" rot="R180"/>
<rectangle x1="-10.4775" y1="1.9685" x2="10.2235" y2="2.0955" layer="22" rot="R180"/>
<rectangle x1="-10.3505" y1="2.0955" x2="9.9695" y2="2.2225" layer="22" rot="R180"/>
<rectangle x1="-10.0965" y1="2.2225" x2="9.7155" y2="2.3495" layer="22" rot="R180"/>
<rectangle x1="-9.9695" y1="2.3495" x2="9.3345" y2="2.4765" layer="22" rot="R180"/>
<rectangle x1="-9.7155" y1="2.4765" x2="9.0805" y2="2.6035" layer="22" rot="R180"/>
<rectangle x1="-9.5885" y1="2.6035" x2="8.6995" y2="2.7305" layer="22" rot="R180"/>
<rectangle x1="-9.3345" y1="2.7305" x2="8.3185" y2="2.8575" layer="22" rot="R180"/>
<rectangle x1="-9.0805" y1="2.8575" x2="7.9375" y2="2.9845" layer="22" rot="R180"/>
<rectangle x1="-8.8265" y1="2.9845" x2="7.4295" y2="3.1115" layer="22" rot="R180"/>
<rectangle x1="-8.5725" y1="3.1115" x2="7.0485" y2="3.2385" layer="22" rot="R180"/>
<rectangle x1="-8.4455" y1="3.2385" x2="6.6675" y2="3.3655" layer="22" rot="R180"/>
<rectangle x1="-8.1915" y1="3.3655" x2="6.1595" y2="3.4925" layer="22" rot="R180"/>
<rectangle x1="-7.9375" y1="3.4925" x2="5.7785" y2="3.6195" layer="22" rot="R180"/>
<rectangle x1="-7.6835" y1="3.6195" x2="5.2705" y2="3.7465" layer="22" rot="R180"/>
<rectangle x1="-7.4295" y1="3.7465" x2="4.8895" y2="3.8735" layer="22" rot="R180"/>
<rectangle x1="-7.1755" y1="3.8735" x2="4.5085" y2="4.0005" layer="22" rot="R180"/>
<rectangle x1="-7.0485" y1="4.0005" x2="4.0005" y2="4.1275" layer="22" rot="R180"/>
<rectangle x1="-6.7945" y1="4.1275" x2="3.4925" y2="4.2545" layer="22" rot="R180"/>
<rectangle x1="-6.5405" y1="4.2545" x2="3.1115" y2="4.3815" layer="22" rot="R180"/>
<rectangle x1="-6.2865" y1="4.3815" x2="2.6035" y2="4.5085" layer="22" rot="R180"/>
<rectangle x1="-6.0325" y1="4.5085" x2="2.2225" y2="4.6355" layer="22" rot="R180"/>
<rectangle x1="-5.7785" y1="4.6355" x2="1.7145" y2="4.7625" layer="22" rot="R180"/>
<rectangle x1="-5.5245" y1="4.7625" x2="1.3335" y2="4.8895" layer="22" rot="R180"/>
<rectangle x1="-5.1435" y1="4.8895" x2="0.9525" y2="5.0165" layer="22" rot="R180"/>
<rectangle x1="-4.7625" y1="5.0165" x2="0.4445" y2="5.1435" layer="22" rot="R180"/>
<rectangle x1="-4.3815" y1="5.1435" x2="-0.0635" y2="5.2705" layer="22" rot="R180"/>
<rectangle x1="-3.8735" y1="5.2705" x2="-0.5715" y2="5.3975" layer="22" rot="R180"/>
<rectangle x1="-3.2385" y1="5.3975" x2="-1.3335" y2="5.5245" layer="22" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="TEELSYS_LOGO_ART">
<description>TeelSys Logo Art</description>
<rectangle x1="-4.6029125" y1="-4.8045" x2="-4.33413125" y2="-4.73730625" layer="105"/>
<rectangle x1="-5.342065625" y1="-4.737303125" x2="-3.594978125" y2="-4.670109375" layer="105"/>
<rectangle x1="-5.745240625" y1="-4.670109375" x2="-3.057409375" y2="-4.602915625" layer="105"/>
<rectangle x1="-6.014021875" y1="-4.6029125" x2="-2.519846875" y2="-4.53571875" layer="105"/>
<rectangle x1="-6.28280625" y1="-4.535715625" x2="-2.049475" y2="-4.468521875" layer="105"/>
<rectangle x1="-6.484390625" y1="-4.468521875" x2="-1.579103125" y2="-4.401328125" layer="105"/>
<rectangle x1="-6.685978125" y1="-4.401325" x2="-1.175928125" y2="-4.33413125" layer="105"/>
<rectangle x1="-6.820371875" y1="-4.334128125" x2="-0.772753125" y2="-4.266934375" layer="105"/>
<rectangle x1="-7.021959375" y1="-4.266934375" x2="-0.369578125" y2="-4.199740625" layer="105"/>
<rectangle x1="-7.156353125" y1="-4.1997375" x2="0.033596875" y2="-4.13254375" layer="105"/>
<rectangle x1="-7.29074375" y1="-4.13254375" x2="0.43676875" y2="-4.06535" layer="105"/>
<rectangle x1="-7.425134375" y1="-4.065346875" x2="0.839946875" y2="-3.998153125" layer="105"/>
<rectangle x1="-7.559525" y1="-3.99815" x2="1.24311875" y2="-3.93095625" layer="105"/>
<rectangle x1="-7.69391875" y1="-3.93095625" x2="1.5791" y2="-3.8637625" layer="105"/>
<rectangle x1="-7.828309375" y1="-3.863759375" x2="1.982271875" y2="-3.796565625" layer="105"/>
<rectangle x1="-7.9627" y1="-3.7965625" x2="2.31825" y2="-3.72936875" layer="105"/>
<rectangle x1="-8.097090625" y1="-3.72936875" x2="2.654228125" y2="-3.662175" layer="105"/>
<rectangle x1="-8.1642875" y1="-3.662171875" x2="2.99020625" y2="-3.594978125" layer="105"/>
<rectangle x1="-8.298678125" y1="-3.594975" x2="3.326190625" y2="-3.52778125" layer="105"/>
<rectangle x1="-8.43306875" y1="-3.52778125" x2="3.66216875" y2="-3.4605875" layer="105"/>
<rectangle x1="-8.500265625" y1="-3.460584375" x2="3.998146875" y2="-3.393390625" layer="105"/>
<rectangle x1="-8.63465625" y1="-3.3933875" x2="4.334125" y2="-3.32619375" layer="105"/>
<rectangle x1="-8.701853125" y1="-3.32619375" x2="4.602909375" y2="-3.259" layer="105"/>
<rectangle x1="-8.83624375" y1="-3.258996875" x2="4.9388875" y2="-3.191803125" layer="105"/>
<rectangle x1="-8.903440625" y1="-3.1918" x2="5.207665625" y2="-3.12460625" layer="105"/>
<rectangle x1="-8.970634375" y1="-3.12460625" x2="5.543646875" y2="-3.0574125" layer="105"/>
<rectangle x1="-9.105028125" y1="-3.057409375" x2="5.812434375" y2="-2.990215625" layer="105"/>
<rectangle x1="-9.172221875" y1="-2.990215625" x2="6.081215625" y2="-2.923021875" layer="105"/>
<rectangle x1="-9.23941875" y1="-2.92301875" x2="6.35" y2="-2.855825" layer="105"/>
<rectangle x1="-9.373809375" y1="-2.855821875" x2="6.618778125" y2="-2.788628125" layer="105"/>
<rectangle x1="-9.44100625" y1="-2.788628125" x2="6.8875625" y2="-2.721434375" layer="105"/>
<rectangle x1="-9.508203125" y1="-2.72143125" x2="7.089153125" y2="-2.6542375" layer="105"/>
<rectangle x1="-9.64259375" y1="-2.654234375" x2="7.35793125" y2="-2.587040625" layer="105"/>
<rectangle x1="-9.709790625" y1="-2.587040625" x2="-7.626721875" y2="-2.519846875" layer="105"/>
<rectangle x1="-7.559525" y1="-2.587040625" x2="7.55951875" y2="-2.519846875" layer="105"/>
<rectangle x1="-9.7769875" y1="-2.51984375" x2="-7.8283125" y2="-2.45265" layer="105"/>
<rectangle x1="-7.3579375" y1="-2.51984375" x2="7.76110625" y2="-2.45265" layer="105"/>
<rectangle x1="-9.84418125" y1="-2.452646875" x2="-7.9627" y2="-2.385453125" layer="105"/>
<rectangle x1="-7.29074375" y1="-2.452646875" x2="7.96269375" y2="-2.385453125" layer="105"/>
<rectangle x1="-9.911378125" y1="-2.385453125" x2="-8.029896875" y2="-2.318259375" layer="105"/>
<rectangle x1="-7.223546875" y1="-2.385453125" x2="8.164284375" y2="-2.318259375" layer="105"/>
<rectangle x1="-9.978575" y1="-2.31825625" x2="-8.0299" y2="-2.2510625" layer="105"/>
<rectangle x1="-7.156353125" y1="-2.31825625" x2="8.365871875" y2="-2.2510625" layer="105"/>
<rectangle x1="-10.112965625" y1="-2.251059375" x2="-8.097090625" y2="-2.183865625" layer="105"/>
<rectangle x1="-7.156353125" y1="-2.251059375" x2="8.500265625" y2="-2.183865625" layer="105"/>
<rectangle x1="-10.180159375" y1="-2.183865625" x2="-8.097090625" y2="-2.116671875" layer="105"/>
<rectangle x1="-7.08915625" y1="-2.183865625" x2="8.63465625" y2="-2.116671875" layer="105"/>
<rectangle x1="-10.24735625" y1="-2.11666875" x2="-8.09709375" y2="-2.049475" layer="105"/>
<rectangle x1="-0.30238125" y1="-2.11666875" x2="8.76904375" y2="-2.049475" layer="105"/>
<rectangle x1="-10.24735625" y1="-2.049471875" x2="-8.09709375" y2="-1.982278125" layer="105"/>
<rectangle x1="-0.30238125" y1="-2.049471875" x2="8.9034375" y2="-1.982278125" layer="105"/>
<rectangle x1="-10.314553125" y1="-1.982278125" x2="-8.097090625" y2="-1.915084375" layer="105"/>
<rectangle x1="-0.30238125" y1="-1.982278125" x2="8.97063125" y2="-1.915084375" layer="105"/>
<rectangle x1="-10.381746875" y1="-1.91508125" x2="-8.097090625" y2="-1.8478875" layer="105"/>
<rectangle x1="-0.30238125" y1="-1.91508125" x2="2.5198375" y2="-1.8478875" layer="105"/>
<rectangle x1="3.1246" y1="-1.91508125" x2="9.105025" y2="-1.8478875" layer="105"/>
<rectangle x1="-10.44894375" y1="-1.8478875" x2="-8.09709375" y2="-1.78069375" layer="105"/>
<rectangle x1="-7.08915625" y1="-1.8478875" x2="2.5198375" y2="-1.78069375" layer="105"/>
<rectangle x1="3.326190625" y1="-1.8478875" x2="9.172221875" y2="-1.78069375" layer="105"/>
<rectangle x1="-10.516140625" y1="-1.780690625" x2="-8.029896875" y2="-1.713496875" layer="105"/>
<rectangle x1="-7.156353125" y1="-1.780690625" x2="2.519840625" y2="-1.713496875" layer="105"/>
<rectangle x1="3.46058125" y1="-1.780690625" x2="9.2394125" y2="-1.713496875" layer="105"/>
<rectangle x1="-10.583334375" y1="-1.71349375" x2="-8.029896875" y2="-1.6463" layer="105"/>
<rectangle x1="-7.156353125" y1="-1.71349375" x2="2.519840625" y2="-1.6463" layer="105"/>
<rectangle x1="3.527778125" y1="-1.71349375" x2="9.306609375" y2="-1.6463" layer="105"/>
<rectangle x1="-10.583334375" y1="-1.6463" x2="-7.962703125" y2="-1.57910625" layer="105"/>
<rectangle x1="-7.223546875" y1="-1.6463" x2="2.519840625" y2="-1.57910625" layer="105"/>
<rectangle x1="3.527778125" y1="-1.6463" x2="9.441003125" y2="-1.57910625" layer="105"/>
<rectangle x1="-10.65053125" y1="-1.579103125" x2="-7.89550625" y2="-1.511909375" layer="105"/>
<rectangle x1="-7.3579375" y1="-1.579103125" x2="2.5198375" y2="-1.511909375" layer="105"/>
<rectangle x1="3.594971875" y1="-1.579103125" x2="9.441003125" y2="-1.511909375" layer="105"/>
<rectangle x1="-10.717725" y1="-1.51190625" x2="-7.69391875" y2="-1.4447125" layer="105"/>
<rectangle x1="-7.49233125" y1="-1.51190625" x2="2.5198375" y2="-1.4447125" layer="105"/>
<rectangle x1="3.66216875" y1="-1.51190625" x2="9.5082" y2="-1.4447125" layer="105"/>
<rectangle x1="-10.717725" y1="-1.4447125" x2="2.5198375" y2="-1.37751875" layer="105"/>
<rectangle x1="3.66216875" y1="-1.4447125" x2="9.57539375" y2="-1.37751875" layer="105"/>
<rectangle x1="-10.784921875" y1="-1.377515625" x2="2.519840625" y2="-1.310321875" layer="105"/>
<rectangle x1="3.66216875" y1="-1.377515625" x2="9.6425875" y2="-1.310321875" layer="105"/>
<rectangle x1="-10.784921875" y1="-1.31031875" x2="3.057403125" y2="-1.243125" layer="105"/>
<rectangle x1="3.729365625" y1="-1.31031875" x2="9.709784375" y2="-1.243125" layer="105"/>
<rectangle x1="-10.85211875" y1="-1.243125" x2="-4.33413125" y2="-1.17593125" layer="105"/>
<rectangle x1="-4.132540625" y1="-1.243125" x2="-2.251059375" y2="-1.17593125" layer="105"/>
<rectangle x1="-2.049471875" y1="-1.243125" x2="0.839946875" y2="-1.17593125" layer="105"/>
<rectangle x1="1.3775125" y1="-1.243125" x2="3.1246" y2="-1.17593125" layer="105"/>
<rectangle x1="3.729365625" y1="-1.243125" x2="5.207665625" y2="-1.17593125" layer="105"/>
<rectangle x1="5.7452375" y1="-1.243125" x2="9.7097875" y2="-1.17593125" layer="105"/>
<rectangle x1="-10.85211875" y1="-1.175928125" x2="-6.35" y2="-1.108734375" layer="105"/>
<rectangle x1="-5.812434375" y1="-1.175928125" x2="-4.602915625" y2="-1.108734375" layer="105"/>
<rectangle x1="-3.86375625" y1="-1.175928125" x2="-2.51984375" y2="-1.108734375" layer="105"/>
<rectangle x1="-1.7806875" y1="-1.175928125" x2="-0.77275625" y2="-1.108734375" layer="105"/>
<rectangle x1="-0.30238125" y1="-1.175928125" x2="0.5711625" y2="-1.108734375" layer="105"/>
<rectangle x1="1.64629375" y1="-1.175928125" x2="3.1246" y2="-1.108734375" layer="105"/>
<rectangle x1="3.729365625" y1="-1.175928125" x2="4.938884375" y2="-1.108734375" layer="105"/>
<rectangle x1="6.014021875" y1="-1.175928125" x2="9.776978125" y2="-1.108734375" layer="105"/>
<rectangle x1="-10.9193125" y1="-1.10873125" x2="-6.35" y2="-1.0415375" layer="105"/>
<rectangle x1="-5.812434375" y1="-1.10873125" x2="-4.737303125" y2="-1.0415375" layer="105"/>
<rectangle x1="-3.729365625" y1="-1.10873125" x2="-2.654234375" y2="-1.0415375" layer="105"/>
<rectangle x1="-1.646296875" y1="-1.10873125" x2="-0.772753125" y2="-1.0415375" layer="105"/>
<rectangle x1="-0.30238125" y1="-1.10873125" x2="0.43676875" y2="-1.0415375" layer="105"/>
<rectangle x1="1.7806875" y1="-1.10873125" x2="3.1246" y2="-1.0415375" layer="105"/>
<rectangle x1="3.796559375" y1="-1.10873125" x2="4.804496875" y2="-1.0415375" layer="105"/>
<rectangle x1="6.1484125" y1="-1.10873125" x2="9.844175" y2="-1.0415375" layer="105"/>
<rectangle x1="-10.9193125" y1="-1.0415375" x2="-6.35" y2="-0.97434375" layer="105"/>
<rectangle x1="-5.812434375" y1="-1.0415375" x2="-4.804503125" y2="-0.97434375" layer="105"/>
<rectangle x1="-3.66216875" y1="-1.0415375" x2="-2.72143125" y2="-0.97434375" layer="105"/>
<rectangle x1="-1.51190625" y1="-1.0415375" x2="-0.77275625" y2="-0.97434375" layer="105"/>
<rectangle x1="-0.30238125" y1="-1.0415375" x2="0.302375" y2="-0.97434375" layer="105"/>
<rectangle x1="1.915078125" y1="-1.0415375" x2="3.057403125" y2="-0.97434375" layer="105"/>
<rectangle x1="3.796559375" y1="-1.0415375" x2="4.737296875" y2="-0.97434375" layer="105"/>
<rectangle x1="6.215609375" y1="-1.0415375" x2="9.844178125" y2="-0.97434375" layer="105"/>
<rectangle x1="-10.9193125" y1="-0.974340625" x2="-6.35" y2="-0.907146875" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.974340625" x2="-4.871696875" y2="-0.907146875" layer="105"/>
<rectangle x1="-3.594975" y1="-0.974340625" x2="-2.788625" y2="-0.907146875" layer="105"/>
<rectangle x1="-1.444709375" y1="-0.974340625" x2="-0.772753125" y2="-0.907146875" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.974340625" x2="0.23518125" y2="-0.907146875" layer="105"/>
<rectangle x1="1.982275" y1="-0.974340625" x2="3.05740625" y2="-0.907146875" layer="105"/>
<rectangle x1="3.796559375" y1="-0.974340625" x2="4.670103125" y2="-0.907146875" layer="105"/>
<rectangle x1="6.35" y1="-0.974340625" x2="9.844175" y2="-0.907146875" layer="105"/>
<rectangle x1="-10.986509375" y1="-0.907146875" x2="-6.350003125" y2="-0.839953125" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.907146875" x2="-4.938890625" y2="-0.839953125" layer="105"/>
<rectangle x1="-3.527778125" y1="-0.907146875" x2="-2.855821875" y2="-0.839953125" layer="105"/>
<rectangle x1="-1.3775125" y1="-0.907146875" x2="-0.77275625" y2="-0.839953125" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.907146875" x2="0.1679875" y2="-0.839953125" layer="105"/>
<rectangle x1="2.04946875" y1="-0.907146875" x2="3.05740625" y2="-0.839953125" layer="105"/>
<rectangle x1="3.86375625" y1="-0.907146875" x2="4.60290625" y2="-0.839953125" layer="105"/>
<rectangle x1="6.35" y1="-0.907146875" x2="9.911375" y2="-0.839953125" layer="105"/>
<rectangle x1="-10.986509375" y1="-0.83995" x2="-6.350003125" y2="-0.77275625" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.83995" x2="-5.006090625" y2="-0.77275625" layer="105"/>
<rectangle x1="-4.334128125" y1="-0.83995" x2="-4.132540625" y2="-0.77275625" layer="105"/>
<rectangle x1="-3.460584375" y1="-0.83995" x2="-2.923021875" y2="-0.77275625" layer="105"/>
<rectangle x1="-2.251059375" y1="-0.83995" x2="-1.982278125" y2="-0.77275625" layer="105"/>
<rectangle x1="-1.3775125" y1="-0.83995" x2="-0.77275625" y2="-0.77275625" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.83995" x2="0.1007875" y2="-0.77275625" layer="105"/>
<rectangle x1="2.116665625" y1="-0.83995" x2="2.990209375" y2="-0.77275625" layer="105"/>
<rectangle x1="3.86375625" y1="-0.83995" x2="4.5357125" y2="-0.77275625" layer="105"/>
<rectangle x1="6.41719375" y1="-0.83995" x2="9.911375" y2="-0.77275625" layer="105"/>
<rectangle x1="-10.986509375" y1="-0.772753125" x2="-6.350003125" y2="-0.705559375" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.772753125" x2="-5.073284375" y2="-0.705559375" layer="105"/>
<rectangle x1="-4.468521875" y1="-0.772753125" x2="-3.930953125" y2="-0.705559375" layer="105"/>
<rectangle x1="-3.460584375" y1="-0.772753125" x2="-2.923021875" y2="-0.705559375" layer="105"/>
<rectangle x1="-2.385453125" y1="-0.772753125" x2="-1.847884375" y2="-0.705559375" layer="105"/>
<rectangle x1="-1.31031875" y1="-0.772753125" x2="-0.77275625" y2="-0.705559375" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.772753125" x2="0.03359375" y2="-0.705559375" layer="105"/>
<rectangle x1="2.183859375" y1="-0.772753125" x2="2.990209375" y2="-0.705559375" layer="105"/>
<rectangle x1="3.86375625" y1="-0.772753125" x2="4.5357125" y2="-0.705559375" layer="105"/>
<rectangle x1="6.484390625" y1="-0.772753125" x2="9.978565625" y2="-0.705559375" layer="105"/>
<rectangle x1="-10.986509375" y1="-0.705559375" x2="-6.350003125" y2="-0.638365625" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.705559375" x2="-5.073284375" y2="-0.638365625" layer="105"/>
<rectangle x1="-4.6029125" y1="-0.705559375" x2="-3.86375625" y2="-0.638365625" layer="105"/>
<rectangle x1="-3.3933875" y1="-0.705559375" x2="-2.9902125" y2="-0.638365625" layer="105"/>
<rectangle x1="-2.452646875" y1="-0.705559375" x2="-1.713496875" y2="-0.638365625" layer="105"/>
<rectangle x1="-1.31031875" y1="-0.705559375" x2="-0.77275625" y2="-0.638365625" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.705559375" x2="0.03359375" y2="-0.638365625" layer="105"/>
<rectangle x1="2.183859375" y1="-0.705559375" x2="2.923009375" y2="-0.638365625" layer="105"/>
<rectangle x1="3.93095" y1="-0.705559375" x2="4.4685125" y2="-0.638365625" layer="105"/>
<rectangle x1="5.274865625" y1="-0.705559375" x2="5.745234375" y2="-0.638365625" layer="105"/>
<rectangle x1="6.484390625" y1="-0.705559375" x2="9.978565625" y2="-0.638365625" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.6383625" x2="-6.35" y2="-0.57116875" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.6383625" x2="-5.073284375" y2="-0.57116875" layer="105"/>
<rectangle x1="-4.6029125" y1="-0.6383625" x2="-3.7965625" y2="-0.57116875" layer="105"/>
<rectangle x1="-3.3933875" y1="-0.6383625" x2="-2.9902125" y2="-0.57116875" layer="105"/>
<rectangle x1="-2.51984375" y1="-0.6383625" x2="-1.71349375" y2="-0.57116875" layer="105"/>
<rectangle x1="-1.243121875" y1="-0.6383625" x2="-0.772753125" y2="-0.57116875" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.6383625" x2="-0.0336" y2="-0.57116875" layer="105"/>
<rectangle x1="2.25105625" y1="-0.6383625" x2="2.9230125" y2="-0.57116875" layer="105"/>
<rectangle x1="3.93095" y1="-0.6383625" x2="4.4685125" y2="-0.57116875" layer="105"/>
<rectangle x1="5.140475" y1="-0.6383625" x2="5.81243125" y2="-0.57116875" layer="105"/>
<rectangle x1="6.484390625" y1="-0.6383625" x2="9.978565625" y2="-0.57116875" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.571165625" x2="-6.35" y2="-0.503971875" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.571165625" x2="-5.140478125" y2="-0.503971875" layer="105"/>
<rectangle x1="-4.670109375" y1="-0.571165625" x2="-3.796565625" y2="-0.503971875" layer="105"/>
<rectangle x1="-3.460584375" y1="-0.571165625" x2="-2.990215625" y2="-0.503971875" layer="105"/>
<rectangle x1="-2.51984375" y1="-0.571165625" x2="-1.6463" y2="-0.503971875" layer="105"/>
<rectangle x1="-1.31031875" y1="-0.571165625" x2="-0.77275625" y2="-0.503971875" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.571165625" x2="-0.0336" y2="-0.503971875" layer="105"/>
<rectangle x1="1.041534375" y1="-0.571165625" x2="1.310315625" y2="-0.503971875" layer="105"/>
<rectangle x1="2.25105625" y1="-0.571165625" x2="2.9230125" y2="-0.503971875" layer="105"/>
<rectangle x1="3.93095" y1="-0.571165625" x2="4.6701" y2="-0.503971875" layer="105"/>
<rectangle x1="5.073278125" y1="-0.571165625" x2="5.812434375" y2="-0.503971875" layer="105"/>
<rectangle x1="6.484390625" y1="-0.571165625" x2="9.978565625" y2="-0.503971875" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.503971875" x2="-6.35" y2="-0.436778125" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.503971875" x2="-5.140478125" y2="-0.436778125" layer="105"/>
<rectangle x1="-4.670109375" y1="-0.503971875" x2="-3.057409375" y2="-0.436778125" layer="105"/>
<rectangle x1="-2.5870375" y1="-0.503971875" x2="-0.77275625" y2="-0.436778125" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.503971875" x2="-0.1008" y2="-0.436778125" layer="105"/>
<rectangle x1="0.839946875" y1="-0.503971875" x2="1.444709375" y2="-0.436778125" layer="105"/>
<rectangle x1="2.25105625" y1="-0.503971875" x2="2.85581875" y2="-0.436778125" layer="105"/>
<rectangle x1="3.998146875" y1="-0.503971875" x2="5.678040625" y2="-0.436778125" layer="105"/>
<rectangle x1="6.484390625" y1="-0.503971875" x2="9.978565625" y2="-0.436778125" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.436775" x2="-6.35" y2="-0.36958125" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.436775" x2="-5.140478125" y2="-0.36958125" layer="105"/>
<rectangle x1="-4.670109375" y1="-0.436775" x2="-3.057409375" y2="-0.36958125" layer="105"/>
<rectangle x1="-2.5870375" y1="-0.436775" x2="-0.77275625" y2="-0.36958125" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.436775" x2="-0.1008" y2="-0.36958125" layer="105"/>
<rectangle x1="0.77275" y1="-0.436775" x2="1.44470625" y2="-0.36958125" layer="105"/>
<rectangle x1="2.318253125" y1="-0.436775" x2="2.855815625" y2="-0.36958125" layer="105"/>
<rectangle x1="3.998146875" y1="-0.436775" x2="5.207665625" y2="-0.36958125" layer="105"/>
<rectangle x1="6.484390625" y1="-0.436775" x2="10.045765625" y2="-0.36958125" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.369578125" x2="-6.35" y2="-0.302384375" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.369578125" x2="-5.140478125" y2="-0.302384375" layer="105"/>
<rectangle x1="-4.670109375" y1="-0.369578125" x2="-3.057409375" y2="-0.302384375" layer="105"/>
<rectangle x1="-2.5870375" y1="-0.369578125" x2="-0.77275625" y2="-0.302384375" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.369578125" x2="0.03359375" y2="-0.302384375" layer="105"/>
<rectangle x1="0.70555625" y1="-0.369578125" x2="1.44470625" y2="-0.302384375" layer="105"/>
<rectangle x1="2.318253125" y1="-0.369578125" x2="2.855815625" y2="-0.302384375" layer="105"/>
<rectangle x1="4.06534375" y1="-0.369578125" x2="4.9388875" y2="-0.302384375" layer="105"/>
<rectangle x1="6.484390625" y1="-0.369578125" x2="10.045765625" y2="-0.302384375" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.302384375" x2="-6.35" y2="-0.235190625" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.302384375" x2="-5.207678125" y2="-0.235190625" layer="105"/>
<rectangle x1="-4.737303125" y1="-0.302384375" x2="-3.057409375" y2="-0.235190625" layer="105"/>
<rectangle x1="-2.5870375" y1="-0.302384375" x2="-0.77275625" y2="-0.235190625" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.302384375" x2="0.369575" y2="-0.235190625" layer="105"/>
<rectangle x1="0.70555625" y1="-0.302384375" x2="1.44470625" y2="-0.235190625" layer="105"/>
<rectangle x1="2.318253125" y1="-0.302384375" x2="2.788621875" y2="-0.235190625" layer="105"/>
<rectangle x1="4.06534375" y1="-0.302384375" x2="4.80449375" y2="-0.235190625" layer="105"/>
<rectangle x1="6.484390625" y1="-0.302384375" x2="10.045765625" y2="-0.235190625" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.2351875" x2="-6.35" y2="-0.16799375" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.2351875" x2="-5.207678125" y2="-0.16799375" layer="105"/>
<rectangle x1="-4.6029125" y1="-0.2351875" x2="-4.53571875" y2="-0.16799375" layer="105"/>
<rectangle x1="-3.326190625" y1="-0.2351875" x2="-3.057409375" y2="-0.16799375" layer="105"/>
<rectangle x1="-1.243121875" y1="-0.2351875" x2="-0.772753125" y2="-0.16799375" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.2351875" x2="1.3103125" y2="-0.16799375" layer="105"/>
<rectangle x1="2.318253125" y1="-0.2351875" x2="2.788621875" y2="-0.16799375" layer="105"/>
<rectangle x1="4.06534375" y1="-0.2351875" x2="4.7373" y2="-0.16799375" layer="105"/>
<rectangle x1="6.484390625" y1="-0.2351875" x2="10.045765625" y2="-0.16799375" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.167990625" x2="-6.35" y2="-0.100796875" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.167990625" x2="-5.207678125" y2="-0.100796875" layer="105"/>
<rectangle x1="-3.326190625" y1="-0.167990625" x2="-3.057409375" y2="-0.100796875" layer="105"/>
<rectangle x1="-1.175928125" y1="-0.167990625" x2="-0.772753125" y2="-0.100796875" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.167990625" x2="1.04153125" y2="-0.100796875" layer="105"/>
<rectangle x1="2.318253125" y1="-0.167990625" x2="2.788621875" y2="-0.100796875" layer="105"/>
<rectangle x1="4.1325375" y1="-0.167990625" x2="4.6701" y2="-0.100796875" layer="105"/>
<rectangle x1="6.41719375" y1="-0.167990625" x2="10.0457625" y2="-0.100796875" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.100796875" x2="-6.35" y2="-0.033603125" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.100796875" x2="-5.207678125" y2="-0.033603125" layer="105"/>
<rectangle x1="-3.326190625" y1="-0.100796875" x2="-3.057409375" y2="-0.033603125" layer="105"/>
<rectangle x1="-1.175928125" y1="-0.100796875" x2="-0.772753125" y2="-0.033603125" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.100796875" x2="0.77275" y2="-0.033603125" layer="105"/>
<rectangle x1="2.318253125" y1="-0.100796875" x2="2.721421875" y2="-0.033603125" layer="105"/>
<rectangle x1="3.527778125" y1="-0.100796875" x2="3.594971875" y2="-0.033603125" layer="105"/>
<rectangle x1="4.1325375" y1="-0.100796875" x2="4.60290625" y2="-0.033603125" layer="105"/>
<rectangle x1="6.35" y1="-0.100796875" x2="10.0457625" y2="-0.033603125" layer="105"/>
<rectangle x1="-11.05370625" y1="-0.0336" x2="-6.35" y2="0.03359375" layer="105"/>
<rectangle x1="-5.812434375" y1="-0.0336" x2="-5.207678125" y2="0.03359375" layer="105"/>
<rectangle x1="-3.326190625" y1="-0.0336" x2="-3.057409375" y2="0.03359375" layer="105"/>
<rectangle x1="-1.175928125" y1="-0.0336" x2="-0.772753125" y2="0.03359375" layer="105"/>
<rectangle x1="-0.30238125" y1="-0.0336" x2="0.5711625" y2="0.03359375" layer="105"/>
<rectangle x1="2.25105625" y1="-0.0336" x2="2.721425" y2="0.03359375" layer="105"/>
<rectangle x1="3.527778125" y1="-0.0336" x2="3.594971875" y2="0.03359375" layer="105"/>
<rectangle x1="4.1325375" y1="-0.0336" x2="4.60290625" y2="0.03359375" layer="105"/>
<rectangle x1="6.282803125" y1="-0.0336" x2="9.978565625" y2="0.03359375" layer="105"/>
<rectangle x1="-10.986509375" y1="0.033596875" x2="-6.350003125" y2="0.100790625" layer="105"/>
<rectangle x1="-5.812434375" y1="0.033596875" x2="-5.140478125" y2="0.100790625" layer="105"/>
<rectangle x1="-3.326190625" y1="0.033596875" x2="-3.057409375" y2="0.100790625" layer="105"/>
<rectangle x1="-1.243121875" y1="0.033596875" x2="-0.772753125" y2="0.100790625" layer="105"/>
<rectangle x1="-0.30238125" y1="0.033596875" x2="0.43676875" y2="0.100790625" layer="105"/>
<rectangle x1="2.25105625" y1="0.033596875" x2="2.65423125" y2="0.100790625" layer="105"/>
<rectangle x1="3.46058125" y1="0.033596875" x2="3.59496875" y2="0.100790625" layer="105"/>
<rectangle x1="4.199734375" y1="0.033596875" x2="4.602909375" y2="0.100790625" layer="105"/>
<rectangle x1="6.215609375" y1="0.033596875" x2="9.978565625" y2="0.100790625" layer="105"/>
<rectangle x1="-10.986509375" y1="0.100790625" x2="-6.350003125" y2="0.167984375" layer="105"/>
<rectangle x1="-5.812434375" y1="0.100790625" x2="-5.140478125" y2="0.167984375" layer="105"/>
<rectangle x1="-4.670109375" y1="0.100790625" x2="-3.796565625" y2="0.167984375" layer="105"/>
<rectangle x1="-3.326190625" y1="0.100790625" x2="-3.057409375" y2="0.167984375" layer="105"/>
<rectangle x1="-2.5870375" y1="0.100790625" x2="-1.71349375" y2="0.167984375" layer="105"/>
<rectangle x1="-1.243121875" y1="0.100790625" x2="-0.772753125" y2="0.167984375" layer="105"/>
<rectangle x1="-0.30238125" y1="0.100790625" x2="0.302375" y2="0.167984375" layer="105"/>
<rectangle x1="2.25105625" y1="0.100790625" x2="2.65423125" y2="0.167984375" layer="105"/>
<rectangle x1="3.46058125" y1="0.100790625" x2="3.66216875" y2="0.167984375" layer="105"/>
<rectangle x1="4.199734375" y1="0.100790625" x2="4.535709375" y2="0.167984375" layer="105"/>
<rectangle x1="6.081215625" y1="0.100790625" x2="6.887565625" y2="0.167984375" layer="105"/>
<rectangle x1="6.954759375" y1="0.100790625" x2="7.626715625" y2="0.167984375" layer="105"/>
<rectangle x1="7.6939125" y1="0.100790625" x2="9.97856875" y2="0.167984375" layer="105"/>
<rectangle x1="-10.986509375" y1="0.1679875" x2="-6.350003125" y2="0.23518125" layer="105"/>
<rectangle x1="-5.812434375" y1="0.1679875" x2="-5.140478125" y2="0.23518125" layer="105"/>
<rectangle x1="-4.670109375" y1="0.1679875" x2="-3.796565625" y2="0.23518125" layer="105"/>
<rectangle x1="-3.3933875" y1="0.1679875" x2="-2.9902125" y2="0.23518125" layer="105"/>
<rectangle x1="-2.5870375" y1="0.1679875" x2="-1.71349375" y2="0.23518125" layer="105"/>
<rectangle x1="-1.243121875" y1="0.1679875" x2="-0.772753125" y2="0.23518125" layer="105"/>
<rectangle x1="-0.30238125" y1="0.1679875" x2="0.23518125" y2="0.23518125" layer="105"/>
<rectangle x1="2.183859375" y1="0.1679875" x2="2.654228125" y2="0.23518125" layer="105"/>
<rectangle x1="3.46058125" y1="0.1679875" x2="3.66216875" y2="0.23518125" layer="105"/>
<rectangle x1="4.199734375" y1="0.1679875" x2="4.535709375" y2="0.23518125" layer="105"/>
<rectangle x1="5.879628125" y1="0.1679875" x2="6.887565625" y2="0.23518125" layer="105"/>
<rectangle x1="7.089153125" y1="0.1679875" x2="7.492321875" y2="0.23518125" layer="105"/>
<rectangle x1="7.6939125" y1="0.1679875" x2="9.97856875" y2="0.23518125" layer="105"/>
<rectangle x1="-10.986509375" y1="0.23518125" x2="-6.350003125" y2="0.302375" layer="105"/>
<rectangle x1="-5.812434375" y1="0.23518125" x2="-5.140478125" y2="0.302375" layer="105"/>
<rectangle x1="-4.670109375" y1="0.23518125" x2="-3.796565625" y2="0.302375" layer="105"/>
<rectangle x1="-3.3933875" y1="0.23518125" x2="-2.9902125" y2="0.302375" layer="105"/>
<rectangle x1="-2.5870375" y1="0.23518125" x2="-1.71349375" y2="0.302375" layer="105"/>
<rectangle x1="-1.243121875" y1="0.23518125" x2="-0.772753125" y2="0.302375" layer="105"/>
<rectangle x1="-0.30238125" y1="0.23518125" x2="0.1679875" y2="0.302375" layer="105"/>
<rectangle x1="2.116665625" y1="0.23518125" x2="2.587034375" y2="0.302375" layer="105"/>
<rectangle x1="3.393384375" y1="0.23518125" x2="3.662165625" y2="0.302375" layer="105"/>
<rectangle x1="4.26693125" y1="0.23518125" x2="4.5357125" y2="0.302375" layer="105"/>
<rectangle x1="5.3420625" y1="0.23518125" x2="6.8875625" y2="0.302375" layer="105"/>
<rectangle x1="7.22354375" y1="0.23518125" x2="7.35793125" y2="0.302375" layer="105"/>
<rectangle x1="7.6939125" y1="0.23518125" x2="9.97856875" y2="0.302375" layer="105"/>
<rectangle x1="-10.9193125" y1="0.302378125" x2="-6.35" y2="0.369571875" layer="105"/>
<rectangle x1="-5.812434375" y1="0.302378125" x2="-5.073284375" y2="0.369571875" layer="105"/>
<rectangle x1="-4.670109375" y1="0.302378125" x2="-3.863759375" y2="0.369571875" layer="105"/>
<rectangle x1="-3.3933875" y1="0.302378125" x2="-2.9902125" y2="0.369571875" layer="105"/>
<rectangle x1="-2.51984375" y1="0.302378125" x2="-1.71349375" y2="0.369571875" layer="105"/>
<rectangle x1="-1.31031875" y1="0.302378125" x2="-0.77275625" y2="0.369571875" layer="105"/>
<rectangle x1="-0.30238125" y1="0.302378125" x2="0.1679875" y2="0.369571875" layer="105"/>
<rectangle x1="2.04946875" y1="0.302378125" x2="2.58703125" y2="0.369571875" layer="105"/>
<rectangle x1="3.393384375" y1="0.302378125" x2="3.729365625" y2="0.369571875" layer="105"/>
<rectangle x1="4.26693125" y1="0.302378125" x2="4.5357125" y2="0.369571875" layer="105"/>
<rectangle x1="5.20766875" y1="0.302378125" x2="5.81243125" y2="0.369571875" layer="105"/>
<rectangle x1="6.014021875" y1="0.302378125" x2="6.887565625" y2="0.369571875" layer="105"/>
<rectangle x1="7.6939125" y1="0.302378125" x2="9.911375" y2="0.369571875" layer="105"/>
<rectangle x1="-10.9193125" y1="0.369575" x2="-6.35" y2="0.43676875" layer="105"/>
<rectangle x1="-5.812434375" y1="0.369575" x2="-5.073284375" y2="0.43676875" layer="105"/>
<rectangle x1="-4.6029125" y1="0.369575" x2="-3.86375625" y2="0.43676875" layer="105"/>
<rectangle x1="-3.3933875" y1="0.369575" x2="-2.92301875" y2="0.43676875" layer="105"/>
<rectangle x1="-2.51984375" y1="0.369575" x2="-1.7806875" y2="0.43676875" layer="105"/>
<rectangle x1="-1.31031875" y1="0.369575" x2="-0.77275625" y2="0.43676875" layer="105"/>
<rectangle x1="-0.30238125" y1="0.369575" x2="0.1007875" y2="0.43676875" layer="105"/>
<rectangle x1="1.982275" y1="0.369575" x2="2.58703125" y2="0.43676875" layer="105"/>
<rectangle x1="3.393384375" y1="0.369575" x2="3.729365625" y2="0.43676875" layer="105"/>
<rectangle x1="4.26693125" y1="0.369575" x2="4.5357125" y2="0.43676875" layer="105"/>
<rectangle x1="5.20766875" y1="0.369575" x2="5.7452375" y2="0.43676875" layer="105"/>
<rectangle x1="6.35" y1="0.369575" x2="6.8875625" y2="0.43676875" layer="105"/>
<rectangle x1="7.6939125" y1="0.369575" x2="9.911375" y2="0.43676875" layer="105"/>
<rectangle x1="-10.85211875" y1="0.43676875" x2="-6.35" y2="0.5039625" layer="105"/>
<rectangle x1="-5.812434375" y1="0.43676875" x2="-5.006090625" y2="0.5039625" layer="105"/>
<rectangle x1="-4.535715625" y1="0.43676875" x2="-3.930953125" y2="0.5039625" layer="105"/>
<rectangle x1="-3.460584375" y1="0.43676875" x2="-2.923021875" y2="0.5039625" layer="105"/>
<rectangle x1="-2.452646875" y1="0.43676875" x2="-1.847884375" y2="0.5039625" layer="105"/>
<rectangle x1="-1.31031875" y1="0.43676875" x2="-0.77275625" y2="0.5039625" layer="105"/>
<rectangle x1="-0.30238125" y1="0.43676875" x2="0.1007875" y2="0.5039625" layer="105"/>
<rectangle x1="1.915078125" y1="0.43676875" x2="2.519840625" y2="0.5039625" layer="105"/>
<rectangle x1="3.326190625" y1="0.43676875" x2="3.729365625" y2="0.5039625" layer="105"/>
<rectangle x1="4.334125" y1="0.43676875" x2="4.60290625" y2="0.5039625" layer="105"/>
<rectangle x1="5.3420625" y1="0.43676875" x2="5.61084375" y2="0.5039625" layer="105"/>
<rectangle x1="6.41719375" y1="0.43676875" x2="6.8875625" y2="0.5039625" layer="105"/>
<rectangle x1="7.6939125" y1="0.43676875" x2="9.911375" y2="0.5039625" layer="105"/>
<rectangle x1="-10.85211875" y1="0.503965625" x2="-6.35" y2="0.571159375" layer="105"/>
<rectangle x1="-5.812434375" y1="0.503965625" x2="-5.006090625" y2="0.571159375" layer="105"/>
<rectangle x1="-4.468521875" y1="0.503965625" x2="-3.998153125" y2="0.571159375" layer="105"/>
<rectangle x1="-3.460584375" y1="0.503965625" x2="-2.855821875" y2="0.571159375" layer="105"/>
<rectangle x1="-2.385453125" y1="0.503965625" x2="-1.915084375" y2="0.571159375" layer="105"/>
<rectangle x1="-1.3775125" y1="0.503965625" x2="-0.77275625" y2="0.571159375" layer="105"/>
<rectangle x1="-0.30238125" y1="0.503965625" x2="0.03359375" y2="0.571159375" layer="105"/>
<rectangle x1="1.713490625" y1="0.503965625" x2="2.519840625" y2="0.571159375" layer="105"/>
<rectangle x1="3.326190625" y1="0.503965625" x2="3.796559375" y2="0.571159375" layer="105"/>
<rectangle x1="4.334125" y1="0.503965625" x2="4.60290625" y2="0.571159375" layer="105"/>
<rectangle x1="6.35" y1="0.503965625" x2="6.95475625" y2="0.571159375" layer="105"/>
<rectangle x1="7.62671875" y1="0.503965625" x2="9.844175" y2="0.571159375" layer="105"/>
<rectangle x1="-10.784921875" y1="0.5711625" x2="-6.350003125" y2="0.63835625" layer="105"/>
<rectangle x1="-5.812434375" y1="0.5711625" x2="-4.938890625" y2="0.63835625" layer="105"/>
<rectangle x1="-3.527778125" y1="0.5711625" x2="-2.855821875" y2="0.63835625" layer="105"/>
<rectangle x1="-1.444709375" y1="0.5711625" x2="-0.772753125" y2="0.63835625" layer="105"/>
<rectangle x1="-0.30238125" y1="0.5711625" x2="0.03359375" y2="0.63835625" layer="105"/>
<rectangle x1="1.511903125" y1="0.5711625" x2="2.452640625" y2="0.63835625" layer="105"/>
<rectangle x1="3.326190625" y1="0.5711625" x2="3.796559375" y2="0.63835625" layer="105"/>
<rectangle x1="4.334125" y1="0.5711625" x2="4.6701" y2="0.63835625" layer="105"/>
<rectangle x1="6.35" y1="0.5711625" x2="6.8875625" y2="0.63835625" layer="105"/>
<rectangle x1="7.6939125" y1="0.5711625" x2="9.844175" y2="0.63835625" layer="105"/>
<rectangle x1="-10.784921875" y1="0.63835625" x2="-6.350003125" y2="0.70555" layer="105"/>
<rectangle x1="-5.812434375" y1="0.63835625" x2="-4.871696875" y2="0.70555" layer="105"/>
<rectangle x1="-3.594975" y1="0.63835625" x2="-2.788625" y2="0.70555" layer="105"/>
<rectangle x1="-1.444709375" y1="0.63835625" x2="-0.772753125" y2="0.70555" layer="105"/>
<rectangle x1="-0.30238125" y1="0.63835625" x2="0.03359375" y2="0.70555" layer="105"/>
<rectangle x1="1.175925" y1="0.63835625" x2="2.45264375" y2="0.70555" layer="105"/>
<rectangle x1="3.25899375" y1="0.63835625" x2="3.79655625" y2="0.70555" layer="105"/>
<rectangle x1="4.401321875" y1="0.63835625" x2="4.670103125" y2="0.70555" layer="105"/>
<rectangle x1="6.282803125" y1="0.63835625" x2="6.820365625" y2="0.70555" layer="105"/>
<rectangle x1="7.761109375" y1="0.63835625" x2="9.776978125" y2="0.70555" layer="105"/>
<rectangle x1="-10.717725" y1="0.705553125" x2="-6.35" y2="0.772746875" layer="105"/>
<rectangle x1="-5.812434375" y1="0.705553125" x2="-4.804503125" y2="0.772746875" layer="105"/>
<rectangle x1="-3.66216875" y1="0.705553125" x2="-2.72143125" y2="0.772746875" layer="105"/>
<rectangle x1="-1.51190625" y1="0.705553125" x2="-0.77275625" y2="0.772746875" layer="105"/>
<rectangle x1="-0.30238125" y1="0.705553125" x2="0.03359375" y2="0.772746875" layer="105"/>
<rectangle x1="0.9743375" y1="0.705553125" x2="2.45264375" y2="0.772746875" layer="105"/>
<rectangle x1="3.25899375" y1="0.705553125" x2="3.86375625" y2="0.772746875" layer="105"/>
<rectangle x1="4.401321875" y1="0.705553125" x2="4.737296875" y2="0.772746875" layer="105"/>
<rectangle x1="6.215609375" y1="0.705553125" x2="6.753171875" y2="0.772746875" layer="105"/>
<rectangle x1="7.828303125" y1="0.705553125" x2="9.776978125" y2="0.772746875" layer="105"/>
<rectangle x1="-10.65053125" y1="0.77275" x2="-6.35" y2="0.83994375" layer="105"/>
<rectangle x1="-5.812434375" y1="0.77275" x2="-4.737303125" y2="0.83994375" layer="105"/>
<rectangle x1="-3.729365625" y1="0.77275" x2="-2.587040625" y2="0.83994375" layer="105"/>
<rectangle x1="-1.646296875" y1="0.77275" x2="-0.772753125" y2="0.83994375" layer="105"/>
<rectangle x1="-0.30238125" y1="0.77275" x2="0.03359375" y2="0.83994375" layer="105"/>
<rectangle x1="0.90714375" y1="0.77275" x2="1.5119" y2="0.83994375" layer="105"/>
<rectangle x1="1.64629375" y1="0.77275" x2="2.38544375" y2="0.83994375" layer="105"/>
<rectangle x1="3.25899375" y1="0.77275" x2="3.86375625" y2="0.83994375" layer="105"/>
<rectangle x1="4.401321875" y1="0.77275" x2="4.871690625" y2="0.83994375" layer="105"/>
<rectangle x1="6.1484125" y1="0.77275" x2="6.753175" y2="0.83994375" layer="105"/>
<rectangle x1="7.8955" y1="0.77275" x2="9.7097875" y2="0.83994375" layer="105"/>
<rectangle x1="-10.65053125" y1="0.83994375" x2="-6.35" y2="0.9071375" layer="105"/>
<rectangle x1="-5.812434375" y1="0.83994375" x2="-4.602915625" y2="0.9071375" layer="105"/>
<rectangle x1="-3.86375625" y1="0.83994375" x2="-2.51984375" y2="0.9071375" layer="105"/>
<rectangle x1="-1.71349375" y1="0.83994375" x2="-0.77275625" y2="0.9071375" layer="105"/>
<rectangle x1="-0.30238125" y1="0.83994375" x2="0.03359375" y2="0.9071375" layer="105"/>
<rectangle x1="0.839946875" y1="0.83994375" x2="1.511903125" y2="0.9071375" layer="105"/>
<rectangle x1="1.84788125" y1="0.83994375" x2="2.38544375" y2="0.9071375" layer="105"/>
<rectangle x1="3.25899375" y1="0.83994375" x2="3.86375625" y2="0.9071375" layer="105"/>
<rectangle x1="4.468515625" y1="0.83994375" x2="4.938884375" y2="0.9071375" layer="105"/>
<rectangle x1="6.014021875" y1="0.83994375" x2="6.685978125" y2="0.9071375" layer="105"/>
<rectangle x1="7.8955" y1="0.83994375" x2="9.6425875" y2="0.9071375" layer="105"/>
<rectangle x1="-10.583334375" y1="0.907140625" x2="-6.350003125" y2="0.974334375" layer="105"/>
<rectangle x1="-5.812434375" y1="0.907140625" x2="-4.334128125" y2="0.974334375" layer="105"/>
<rectangle x1="-4.132540625" y1="0.907140625" x2="-2.251059375" y2="0.974334375" layer="105"/>
<rectangle x1="-1.982278125" y1="0.907140625" x2="-0.772753125" y2="0.974334375" layer="105"/>
<rectangle x1="-0.30238125" y1="0.907140625" x2="0.03359375" y2="0.974334375" layer="105"/>
<rectangle x1="0.839946875" y1="0.907140625" x2="1.444709375" y2="0.974334375" layer="105"/>
<rectangle x1="2.04946875" y1="0.907140625" x2="5.140475" y2="0.974334375" layer="105"/>
<rectangle x1="5.812434375" y1="0.907140625" x2="6.618778125" y2="0.974334375" layer="105"/>
<rectangle x1="7.962696875" y1="0.907140625" x2="9.642590625" y2="0.974334375" layer="105"/>
<rectangle x1="-10.516140625" y1="0.9743375" x2="-6.350003125" y2="1.04153125" layer="105"/>
<rectangle x1="-5.812434375" y1="0.9743375" x2="-0.772753125" y2="1.04153125" layer="105"/>
<rectangle x1="-0.30238125" y1="0.9743375" x2="0.03359375" y2="1.04153125" layer="105"/>
<rectangle x1="0.90714375" y1="0.9743375" x2="1.3103125" y2="1.04153125" layer="105"/>
<rectangle x1="2.25105625" y1="0.9743375" x2="6.95475625" y2="1.04153125" layer="105"/>
<rectangle x1="7.559521875" y1="0.9743375" x2="9.575396875" y2="1.04153125" layer="105"/>
<rectangle x1="-10.44894375" y1="1.04153125" x2="-6.35" y2="1.108725" layer="105"/>
<rectangle x1="-5.812434375" y1="1.04153125" x2="-0.772753125" y2="1.108725" layer="105"/>
<rectangle x1="-0.30238125" y1="1.04153125" x2="0.1007875" y2="1.108725" layer="105"/>
<rectangle x1="2.183859375" y1="1.04153125" x2="7.089153125" y2="1.108725" layer="105"/>
<rectangle x1="7.492325" y1="1.04153125" x2="9.5082" y2="1.108725" layer="105"/>
<rectangle x1="-10.381746875" y1="1.108728125" x2="-6.350003125" y2="1.175921875" layer="105"/>
<rectangle x1="-5.812434375" y1="1.108728125" x2="-0.772753125" y2="1.175921875" layer="105"/>
<rectangle x1="-0.30238125" y1="1.108728125" x2="0.1007875" y2="1.175921875" layer="105"/>
<rectangle x1="2.183859375" y1="1.108728125" x2="7.156346875" y2="1.175921875" layer="105"/>
<rectangle x1="7.42513125" y1="1.108728125" x2="9.5082" y2="1.175921875" layer="105"/>
<rectangle x1="-10.314553125" y1="1.175921875" x2="-7.021959375" y2="1.243115625" layer="105"/>
<rectangle x1="-5.140478125" y1="1.175921875" x2="-0.772753125" y2="1.243115625" layer="105"/>
<rectangle x1="-0.30238125" y1="1.175921875" x2="0.1679875" y2="1.243115625" layer="105"/>
<rectangle x1="2.116665625" y1="1.175921875" x2="7.156346875" y2="1.243115625" layer="105"/>
<rectangle x1="7.42513125" y1="1.175921875" x2="9.441" y2="1.243115625" layer="105"/>
<rectangle x1="-10.24735625" y1="1.24311875" x2="-7.08915625" y2="1.3103125" layer="105"/>
<rectangle x1="-5.07328125" y1="1.24311875" x2="-0.77275625" y2="1.3103125" layer="105"/>
<rectangle x1="-0.30238125" y1="1.24311875" x2="0.1679875" y2="1.3103125" layer="105"/>
<rectangle x1="2.116665625" y1="1.24311875" x2="7.223540625" y2="1.3103125" layer="105"/>
<rectangle x1="7.357934375" y1="1.24311875" x2="9.373809375" y2="1.3103125" layer="105"/>
<rectangle x1="-10.180159375" y1="1.310315625" x2="-7.089159375" y2="1.377509375" layer="105"/>
<rectangle x1="-5.07328125" y1="1.310315625" x2="-0.77275625" y2="1.377509375" layer="105"/>
<rectangle x1="-0.30238125" y1="1.310315625" x2="0.23518125" y2="1.377509375" layer="105"/>
<rectangle x1="2.04946875" y1="1.310315625" x2="7.22354375" y2="1.377509375" layer="105"/>
<rectangle x1="7.357934375" y1="1.310315625" x2="9.306609375" y2="1.377509375" layer="105"/>
<rectangle x1="-10.112965625" y1="1.377509375" x2="-7.089159375" y2="1.444703125" layer="105"/>
<rectangle x1="-5.07328125" y1="1.377509375" x2="-0.77275625" y2="1.444703125" layer="105"/>
<rectangle x1="-0.30238125" y1="1.377509375" x2="0.302375" y2="1.444703125" layer="105"/>
<rectangle x1="1.982275" y1="1.377509375" x2="7.22354375" y2="1.444703125" layer="105"/>
<rectangle x1="7.357934375" y1="1.377509375" x2="9.239415625" y2="1.444703125" layer="105"/>
<rectangle x1="-9.978575" y1="1.44470625" x2="-7.08915625" y2="1.5119" layer="105"/>
<rectangle x1="-5.07328125" y1="1.44470625" x2="-0.77275625" y2="1.5119" layer="105"/>
<rectangle x1="-0.30238125" y1="1.44470625" x2="0.369575" y2="1.5119" layer="105"/>
<rectangle x1="1.915078125" y1="1.44470625" x2="9.172221875" y2="1.5119" layer="105"/>
<rectangle x1="-9.911378125" y1="1.511903125" x2="-7.089159375" y2="1.579096875" layer="105"/>
<rectangle x1="-5.07328125" y1="1.511903125" x2="-0.77275625" y2="1.579096875" layer="105"/>
<rectangle x1="-0.30238125" y1="1.511903125" x2="0.43676875" y2="1.579096875" layer="105"/>
<rectangle x1="1.7806875" y1="1.511903125" x2="9.105025" y2="1.579096875" layer="105"/>
<rectangle x1="-9.7769875" y1="1.579096875" x2="-7.08915625" y2="1.646290625" layer="105"/>
<rectangle x1="-5.07328125" y1="1.579096875" x2="-0.77275625" y2="1.646290625" layer="105"/>
<rectangle x1="-0.30238125" y1="1.579096875" x2="0.63835625" y2="1.646290625" layer="105"/>
<rectangle x1="1.64629375" y1="1.579096875" x2="9.03783125" y2="1.646290625" layer="105"/>
<rectangle x1="-9.709790625" y1="1.64629375" x2="0.839946875" y2="1.7134875" layer="105"/>
<rectangle x1="1.444709375" y1="1.64629375" x2="8.970634375" y2="1.7134875" layer="105"/>
<rectangle x1="-9.5754" y1="1.713490625" x2="8.9034375" y2="1.780684375" layer="105"/>
<rectangle x1="-9.508203125" y1="1.780684375" x2="8.769046875" y2="1.847878125" layer="105"/>
<rectangle x1="-9.373809375" y1="1.84788125" x2="8.701846875" y2="1.915075" layer="105"/>
<rectangle x1="-9.23941875" y1="1.915078125" x2="8.63465625" y2="1.982271875" layer="105"/>
<rectangle x1="-9.105028125" y1="1.982271875" x2="8.500265625" y2="2.049465625" layer="105"/>
<rectangle x1="-8.903440625" y1="2.04946875" x2="8.433065625" y2="2.1166625" layer="105"/>
<rectangle x1="-8.76905" y1="2.116665625" x2="8.298675" y2="2.183859375" layer="105"/>
<rectangle x1="-8.5674625" y1="2.183859375" x2="8.231475" y2="2.251053125" layer="105"/>
<rectangle x1="-8.365875" y1="2.25105625" x2="8.0970875" y2="2.31825" layer="105"/>
<rectangle x1="-8.1642875" y1="2.31825" x2="7.96269375" y2="2.38544375" layer="105"/>
<rectangle x1="-7.9627" y1="2.385446875" x2="7.8283" y2="2.452640625" layer="105"/>
<rectangle x1="-7.7611125" y1="2.45264375" x2="7.76110625" y2="2.5198375" layer="105"/>
<rectangle x1="-7.49233125" y1="2.5198375" x2="7.62671875" y2="2.58703125" layer="105"/>
<rectangle x1="-7.29074375" y1="2.587034375" x2="7.492325" y2="2.654228125" layer="105"/>
<rectangle x1="-7.08915625" y1="2.65423125" x2="7.35793125" y2="2.721425" layer="105"/>
<rectangle x1="-6.887565625" y1="2.721425" x2="7.223540625" y2="2.78861875" layer="105"/>
<rectangle x1="-6.685978125" y1="2.788621875" x2="7.156346875" y2="2.855815625" layer="105"/>
<rectangle x1="-6.417196875" y1="2.85581875" x2="7.021953125" y2="2.9230125" layer="105"/>
<rectangle x1="-6.215609375" y1="2.9230125" x2="6.887565625" y2="2.99020625" layer="105"/>
<rectangle x1="-6.014021875" y1="2.990209375" x2="6.753171875" y2="3.057403125" layer="105"/>
<rectangle x1="-5.745240625" y1="3.05740625" x2="6.685978125" y2="3.1246" layer="105"/>
<rectangle x1="-5.543653125" y1="3.1246" x2="6.551584375" y2="3.19179375" layer="105"/>
<rectangle x1="-5.27486875" y1="3.191796875" x2="6.41719375" y2="3.258990625" layer="105"/>
<rectangle x1="-5.07328125" y1="3.25899375" x2="6.2828" y2="3.3261875" layer="105"/>
<rectangle x1="-4.871696875" y1="3.3261875" x2="6.148409375" y2="3.39338125" layer="105"/>
<rectangle x1="-4.6029125" y1="3.393384375" x2="6.01401875" y2="3.460578125" layer="105"/>
<rectangle x1="-4.401321875" y1="3.460578125" x2="5.946821875" y2="3.527771875" layer="105"/>
<rectangle x1="-4.132540625" y1="3.527775" x2="5.812434375" y2="3.59496875" layer="105"/>
<rectangle x1="-3.930953125" y1="3.594971875" x2="5.678040625" y2="3.662165625" layer="105"/>
<rectangle x1="-3.66216875" y1="3.662165625" x2="5.54364375" y2="3.729359375" layer="105"/>
<rectangle x1="-3.460584375" y1="3.7293625" x2="5.409253125" y2="3.79655625" layer="105"/>
<rectangle x1="-3.1918" y1="3.796559375" x2="5.2748625" y2="3.863753125" layer="105"/>
<rectangle x1="-2.9902125" y1="3.863753125" x2="5.20766875" y2="3.930946875" layer="105"/>
<rectangle x1="-2.72143125" y1="3.93095" x2="5.073275" y2="3.99814375" layer="105"/>
<rectangle x1="-2.51984375" y1="3.998146875" x2="4.9388875" y2="4.065340625" layer="105"/>
<rectangle x1="-2.251059375" y1="4.065340625" x2="4.804496875" y2="4.132534375" layer="105"/>
<rectangle x1="-2.049471875" y1="4.1325375" x2="4.602909375" y2="4.19973125" layer="105"/>
<rectangle x1="-1.7806875" y1="4.199734375" x2="4.4685125" y2="4.266928125" layer="105"/>
<rectangle x1="-1.51190625" y1="4.266928125" x2="4.334125" y2="4.334121875" layer="105"/>
<rectangle x1="-1.31031875" y1="4.334125" x2="4.1325375" y2="4.40131875" layer="105"/>
<rectangle x1="-1.041534375" y1="4.40131875" x2="3.998146875" y2="4.4685125" layer="105"/>
<rectangle x1="-0.839946875" y1="4.468515625" x2="3.796559375" y2="4.535709375" layer="105"/>
<rectangle x1="-0.571165625" y1="4.5357125" x2="3.527778125" y2="4.60290625" layer="105"/>
<rectangle x1="-0.30238125" y1="4.60290625" x2="3.3261875" y2="4.6701" layer="105"/>
<rectangle x1="-0.0336" y1="4.670103125" x2="3.05740625" y2="4.737296875" layer="105"/>
<rectangle x1="0.302378125" y1="4.7373" x2="2.654228125" y2="4.80449375" layer="105"/>
<rectangle x1="0.839946875" y1="4.80449375" x2="2.183859375" y2="4.8716875" layer="105"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEELSYS_LOGO_ART_2">
<gates>
<gate name="G$1" symbol="TEELSYS_LOGO_ART" x="0" y="0"/>
</gates>
<devices>
<device name="TOP" package="TEELSYS_LOGO_ART_2">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BOTTOM" package="TEELSYS_LOGO_ART_BOTTOM_2">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="DOCUMENT_NUMBER" value="TeelSys-090_01"/>
<attribute name="DOCUMENT_REV" value="A"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="LETTER_L" device=""/>
<part name="ASM2" library="TeelSys-Tricorder" deviceset="RAZPBERRYPIZERO1.3" device=""/>
<part name="FRAME2" library="frames" deviceset="LETTER_L" device=""/>
<part name="ASM1" library="TeelSys-Tricorder" deviceset="ZERO4U" device=""/>
<part name="ASM3" library="TeelSys-Tricorder" deviceset="ADAFRUIT2465" device=""/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY16" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY1" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY8" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY9" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY3" library="TeelSys-Tricorder" deviceset="+3.3V" device=""/>
<part name="SUPPLY5" library="TeelSys-Tricorder" deviceset="+3.3V" device=""/>
<part name="BAT1" library="TeelSys-Tricorder" deviceset="LI_BATTERY" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="GND" device=""/>
<part name="S1" library="adafruit" deviceset="EG1218" device="S"/>
<part name="FRAME3" library="frames" deviceset="LETTER_L" device=""/>
<part name="FRAME4" library="frames" deviceset="LETTER_L" device=""/>
<part name="ASM4" library="TeelSys-Tricorder" deviceset="ADAFRUIT1475" device=""/>
<part name="SUPPLY18" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY21" library="supply2" deviceset="GND" device=""/>
<part name="ASM5" library="TeelSys-Tricorder" deviceset="ADAFRUIT2716" device=""/>
<part name="ASM6" library="TeelSys-Tricorder" deviceset="ADAFRUIT2716" device=""/>
<part name="SUPPLY22" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY23" library="supply2" deviceset="+5V" device=""/>
<part name="ASM7" library="TeelSys-Tricorder" deviceset="RADIATION_WATCH_TYPE5" device=""/>
<part name="SUPPLY24" library="TeelSys-Tricorder" deviceset="+3.3V" device=""/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="ASM8" library="TeelSys-Tricorder" deviceset="SPARKFUN_13670" device=""/>
<part name="SUPPLY26" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY27" library="supply2" deviceset="GND" device=""/>
<part name="ASM9" library="TeelSys-Tricorder" deviceset="ADAFRUIT746" device=""/>
<part name="SUPPLY28" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="+5V" device=""/>
<part name="ASM10" library="TeelSys-Tricorder" deviceset="ADAFRUIT1714" device=""/>
<part name="SUPPLY30" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY31" library="supply2" deviceset="+5V" device=""/>
<part name="ASM11" library="TeelSys-Tricorder" deviceset="ADAFRUIT1980" device=""/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="ASM12" library="TeelSys-Tricorder" deviceset="ADAFRUIT2857" device=""/>
<part name="SUPPLY35" library="supply2" deviceset="GND" device=""/>
<part name="ASM13" library="TeelSys-Tricorder" deviceset="ADAFRUIT2651" device=""/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY38" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY32" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY34" library="supply2" deviceset="+5V" device=""/>
<part name="ASM14" library="TeelSys-Tricorder" deviceset="ADAFRUIT1777" device=""/>
<part name="SUPPLY36" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="ASM15" library="TeelSys-Tricorder" deviceset="ADAFRUIT3251" device=""/>
<part name="SUPPLY40" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY41" library="supply2" deviceset="+5V" device=""/>
<part name="ASM16" library="TeelSys-Tricorder" deviceset="TEEL_PI_SW" device=""/>
<part name="ASM18" library="TeelSys-Tricorder" deviceset="PI_CAMERA" device=""/>
<part name="FRAME5" library="frames" deviceset="LETTER_L" device=""/>
<part name="JP1" library="TeelSys-Tricorder" deviceset="TRICORDER_HEAD" device=""/>
<part name="SUPPLY17" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY42" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="U$1" library="teelsys-0.2" deviceset="TEELSYS_LOGO_ART_2" device="TOP"/>
<part name="R2" library="resistor" deviceset="R-US_" device="R0805" value="470"/>
<part name="R3" library="resistor" deviceset="R-US_" device="R0805" value="470"/>
<part name="R1" library="resistor" deviceset="R-US_" device="R0805" value="10K"/>
<part name="JP2" library="TeelSys-Tricorder" deviceset="USB_HEADER" device=""/>
<part name="S2" library="adafruit" deviceset="EG1218" device="S"/>
</parts>
<sheets>
<sheet>
<description>Raspberry Pi &amp; Hub</description>
<plain>
<text x="149.86" y="27.94" size="2.54" layer="94">Raspberry Pi Zero &amp; Hub</text>
<text x="149.86" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_NUMBER</text>
<text x="241.3" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_REV</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="147.32" y="0"/>
<instance part="ASM2" gate="G$1" x="165.1" y="111.76"/>
<instance part="ASM1" gate="G$1" x="35.56" y="121.92" rot="MR0"/>
<instance part="SUPPLY1" gate="+5V" x="40.64" y="139.7"/>
<instance part="SUPPLY2" gate="GND" x="45.72" y="106.68"/>
<instance part="SUPPLY8" gate="+5V" x="198.12" y="165.1"/>
<instance part="SUPPLY9" gate="GND" x="200.66" y="149.86" rot="R90"/>
<instance part="SUPPLY10" gate="GND" x="200.66" y="129.54" rot="R90"/>
<instance part="SUPPLY11" gate="GND" x="200.66" y="114.3" rot="R90"/>
<instance part="SUPPLY12" gate="GND" x="200.66" y="88.9" rot="R90"/>
<instance part="SUPPLY13" gate="GND" x="200.66" y="78.74" rot="R90"/>
<instance part="SUPPLY7" gate="GND" x="134.62" y="63.5" rot="R270"/>
<instance part="SUPPLY6" gate="GND" x="134.62" y="99.06" rot="R270"/>
<instance part="SUPPLY4" gate="GND" x="134.62" y="139.7" rot="R270"/>
<instance part="SUPPLY3" gate="G$1" x="134.62" y="160.02" rot="R90"/>
<instance part="SUPPLY5" gate="G$1" x="137.16" y="119.38" rot="R90"/>
<instance part="ASM18" gate="G$1" x="20.32" y="22.86"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<pinref part="ASM1" gate="G$1" pin="USBD+"/>
<pinref part="ASM2" gate="G$1" pin="PP22-USB_D+"/>
<wire x1="40.64" y1="119.38" x2="73.66" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="ASM1" gate="G$1" pin="USBD-"/>
<pinref part="ASM2" gate="G$1" pin="PP23-USB_D-"/>
<wire x1="40.64" y1="124.46" x2="73.66" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="ASM1" gate="G$1" pin="USBGND"/>
<pinref part="ASM2" gate="G$1" pin="PP6-GND"/>
<wire x1="40.64" y1="114.3" x2="45.72" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="45.72" y1="114.3" x2="73.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="45.72" y1="109.22" x2="45.72" y2="114.3" width="0.1524" layer="91"/>
<junction x="45.72" y="114.3"/>
</segment>
<segment>
<pinref part="ASM2" gate="G$1" pin="06-GROUND"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="187.96" y1="149.86" x2="198.12" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<pinref part="ASM2" gate="G$1" pin="09-GROUND"/>
<wire x1="137.16" y1="139.7" x2="142.24" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<pinref part="ASM2" gate="G$1" pin="25-GROUND"/>
<wire x1="137.16" y1="99.06" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<pinref part="ASM2" gate="G$1" pin="39-GROUND"/>
<wire x1="137.16" y1="63.5" x2="142.24" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<pinref part="ASM2" gate="G$1" pin="34-GROUND"/>
<wire x1="198.12" y1="78.74" x2="187.96" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<pinref part="ASM2" gate="G$1" pin="30-GROUND"/>
<wire x1="198.12" y1="88.9" x2="187.96" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<pinref part="ASM2" gate="G$1" pin="20-GROUND"/>
<wire x1="198.12" y1="114.3" x2="187.96" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<pinref part="ASM2" gate="G$1" pin="14-GROUND"/>
<wire x1="198.12" y1="129.54" x2="187.96" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="ASM1" gate="G$1" pin="USB+5V"/>
<pinref part="SUPPLY1" gate="+5V" pin="+5V"/>
<wire x1="40.64" y1="137.16" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM2" gate="G$1" pin="02-5V"/>
<pinref part="SUPPLY8" gate="+5V" pin="+5V"/>
<wire x1="187.96" y1="160.02" x2="198.12" y2="160.02" width="0.1524" layer="91"/>
<wire x1="198.12" y1="160.02" x2="198.12" y2="162.56" width="0.1524" layer="91"/>
<pinref part="ASM2" gate="G$1" pin="04-5V"/>
<wire x1="187.96" y1="154.94" x2="198.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="198.12" y1="154.94" x2="198.12" y2="160.02" width="0.1524" layer="91"/>
<junction x="198.12" y="160.02"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<pinref part="SUPPLY3" gate="G$1" pin="+3.3V"/>
<wire x1="137.16" y1="160.02" x2="142.24" y2="160.02" width="0.1524" layer="91"/>
<pinref part="ASM2" gate="G$1" pin="01-3.3V"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="G$1" pin="+3.3V"/>
<pinref part="ASM2" gate="G$1" pin="17-3.3V"/>
<wire x1="139.7" y1="119.38" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="21-GPIO09" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="21-GPIO09"/>
<wire x1="142.24" y1="109.22" x2="129.54" y2="109.22" width="0.1524" layer="91"/>
<label x="129.54" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="18-GPIO24" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="18-GPIO24"/>
<wire x1="187.96" y1="119.38" x2="210.82" y2="119.38" width="0.1524" layer="91"/>
<label x="210.82" y="119.38" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="19-GPIO10" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="19-GPIO10"/>
<wire x1="142.24" y1="114.3" x2="129.54" y2="114.3" width="0.1524" layer="91"/>
<label x="129.54" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="15-GPIO22" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="15-GPIO22"/>
<wire x1="142.24" y1="124.46" x2="129.54" y2="124.46" width="0.1524" layer="91"/>
<label x="129.54" y="124.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="16-GPIO23" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="16-GPIO23"/>
<wire x1="187.96" y1="124.46" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<label x="210.82" y="124.46" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="08-GPIO14" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="08-GPIO14"/>
<wire x1="187.96" y1="144.78" x2="210.82" y2="144.78" width="0.1524" layer="91"/>
<label x="210.82" y="144.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="10-GPIO15" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="10-GPIO15"/>
<wire x1="187.96" y1="139.7" x2="210.82" y2="139.7" width="0.1524" layer="91"/>
<label x="210.82" y="139.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="03-GPIO02" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="03-GPIO02"/>
<wire x1="142.24" y1="154.94" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<label x="129.54" y="154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="05-GPIO03" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="05-GPIO03"/>
<wire x1="142.24" y1="149.86" x2="129.54" y2="149.86" width="0.1524" layer="91"/>
<label x="129.54" y="149.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="35-GPIO19" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="35-GPIO19"/>
<wire x1="142.24" y1="73.66" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
<label x="129.54" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="37-GPIO26" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="37-GPIO26"/>
<wire x1="142.24" y1="68.58" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<label x="129.54" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="36-GPIO16" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="36-GPIO16"/>
<wire x1="187.96" y1="73.66" x2="210.82" y2="73.66" width="0.1524" layer="91"/>
<label x="210.82" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="38-GPIO20" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="38-GPIO20"/>
<wire x1="187.96" y1="68.58" x2="210.82" y2="68.58" width="0.1524" layer="91"/>
<label x="210.82" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="33-GPIO13" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="33-GPIO13"/>
<wire x1="142.24" y1="78.74" x2="129.54" y2="78.74" width="0.1524" layer="91"/>
<label x="129.54" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="12-GPIO18" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="12-GPIO18"/>
<wire x1="187.96" y1="134.62" x2="210.82" y2="134.62" width="0.1524" layer="91"/>
<label x="210.82" y="134.62" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="23-GPIO11" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="23-GPIO11"/>
<wire x1="142.24" y1="104.14" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
<label x="129.54" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="24-GPIO08" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="24-GPIO08"/>
<wire x1="187.96" y1="104.14" x2="210.82" y2="104.14" width="0.1524" layer="91"/>
<label x="210.82" y="104.14" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="26-GPIO07" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="26-GPIO07"/>
<wire x1="187.96" y1="99.06" x2="210.82" y2="99.06" width="0.1524" layer="91"/>
<label x="210.82" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="22-GPIO25" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="22-GPIO25"/>
<wire x1="187.96" y1="109.22" x2="210.82" y2="109.22" width="0.1524" layer="91"/>
<label x="210.82" y="109.22" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PI_USB_5V" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="PP1-5V"/>
<wire x1="73.66" y1="129.54" x2="66.04" y2="129.54" width="0.1524" layer="91"/>
<label x="66.04" y="129.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="29-GPIO5" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="29-GPIO05"/>
<wire x1="142.24" y1="88.9" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<label x="129.54" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="07-GPIO4" class="0">
<segment>
<pinref part="ASM2" gate="G$1" pin="07-GPIO04"/>
<wire x1="142.24" y1="144.78" x2="129.54" y2="144.78" width="0.1524" layer="91"/>
<label x="129.54" y="144.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Power Supply</description>
<plain>
<text x="22.86" y="172.72" size="1.778" layer="97">3.7V 6600mAh
adafruit 353</text>
<text x="45.72" y="177.8" size="1.778" layer="97">Long Term Storage
Battery Disconnect</text>
<text x="149.86" y="27.94" size="2.54" layer="94">Power Supply</text>
<text x="149.86" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_NUMBER</text>
<text x="241.3" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_REV</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="FRAME2" gate="G$2" x="147.32" y="0"/>
<instance part="ASM3" gate="G$1" x="76.2" y="152.4"/>
<instance part="SUPPLY15" gate="GND" x="116.84" y="137.16"/>
<instance part="SUPPLY16" gate="+5V" x="142.24" y="149.86" rot="R270"/>
<instance part="BAT1" gate="G$1" x="30.48" y="165.1"/>
<instance part="SUPPLY14" gate="GND" x="17.78" y="157.48"/>
<instance part="S1" gate="1" x="50.8" y="165.1" rot="R90"/>
<instance part="ASM16" gate="G$1" x="76.2" y="119.38" rot="MR180"/>
<instance part="S2" gate="1" x="53.34" y="129.54" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="ASM3" gate="G$1" pin="G"/>
<wire x1="109.22" y1="149.86" x2="109.22" y2="139.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="139.7" x2="116.84" y2="139.7" width="0.1524" layer="91"/>
<pinref part="ASM16" gate="G$1" pin="GND"/>
<pinref part="ASM3" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="121.92" x2="99.06" y2="124.46" width="0.1524" layer="91"/>
<wire x1="99.06" y1="124.46" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<wire x1="99.06" y1="139.7" x2="99.06" y2="149.86" width="0.1524" layer="91"/>
<wire x1="109.22" y1="139.7" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<junction x="109.22" y="139.7"/>
<junction x="99.06" y="139.7"/>
<pinref part="S2" gate="1" pin="O"/>
<wire x1="53.34" y1="124.46" x2="99.06" y2="124.46" width="0.1524" layer="91"/>
<junction x="99.06" y="124.46"/>
</segment>
<segment>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="17.78" y1="160.02" x2="17.78" y2="165.1" width="0.1524" layer="91"/>
<pinref part="BAT1" gate="G$1" pin="-"/>
<wire x1="17.78" y1="165.1" x2="22.86" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="ASM3" gate="G$1" pin="5V"/>
<pinref part="SUPPLY16" gate="+5V" pin="+5V"/>
<wire x1="114.3" y1="149.86" x2="139.7" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="60.96" y1="167.64" x2="60.96" y2="144.78" width="0.1524" layer="91"/>
<pinref part="ASM3" gate="G$1" pin="BAT"/>
<wire x1="60.96" y1="144.78" x2="83.82" y2="144.78" width="0.1524" layer="91"/>
<wire x1="83.82" y1="144.78" x2="83.82" y2="149.86" width="0.1524" layer="91"/>
<pinref part="ASM16" gate="G$1" pin="BAT"/>
<wire x1="83.82" y1="121.92" x2="83.82" y2="149.86" width="0.1524" layer="91"/>
<junction x="83.82" y="149.86"/>
<pinref part="S1" gate="1" pin="P"/>
<wire x1="55.88" y1="167.64" x2="60.96" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="ASM3" gate="G$1" pin="USB"/>
<pinref part="ASM16" gate="G$1" pin="USB"/>
<wire x1="78.74" y1="149.86" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="ASM3" gate="G$1" pin="VS"/>
<pinref part="ASM16" gate="G$1" pin="VS"/>
<wire x1="88.9" y1="149.86" x2="88.9" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="ASM3" gate="G$1" pin="EN"/>
<pinref part="ASM16" gate="G$1" pin="EN"/>
<wire x1="93.98" y1="149.86" x2="93.98" y2="134.62" width="0.1524" layer="91"/>
<pinref part="S2" gate="1" pin="P"/>
<wire x1="93.98" y1="134.62" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="50.8" y1="134.62" x2="93.98" y2="134.62" width="0.1524" layer="91"/>
<junction x="93.98" y="134.62"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="ASM16" gate="G$1" pin="LBO"/>
<pinref part="ASM3" gate="G$1" pin="LBO"/>
<wire x1="104.14" y1="121.92" x2="104.14" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="07-GPIO4" class="0">
<segment>
<pinref part="ASM16" gate="G$1" pin="CONTROL"/>
<wire x1="109.22" y1="121.92" x2="109.22" y2="132.08" width="0.1524" layer="91"/>
<wire x1="109.22" y1="132.08" x2="124.46" y2="132.08" width="0.1524" layer="91"/>
<label x="124.46" y="132.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="29-GPIO5" class="0">
<segment>
<pinref part="ASM16" gate="G$1" pin="STATUS"/>
<wire x1="114.3" y1="121.92" x2="114.3" y2="127" width="0.1524" layer="91"/>
<wire x1="114.3" y1="127" x2="124.46" y2="127" width="0.1524" layer="91"/>
<label x="124.46" y="127" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PI_USB_5V" class="0">
<segment>
<pinref part="ASM16" gate="G$1" pin="PI_USB_5V"/>
<wire x1="119.38" y1="121.92" x2="124.46" y2="121.92" width="0.1524" layer="91"/>
<label x="124.46" y="121.92" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="+"/>
<wire x1="38.1" y1="165.1" x2="43.18" y2="165.1" width="0.1524" layer="91"/>
<wire x1="43.18" y1="165.1" x2="43.18" y2="170.18" width="0.1524" layer="91"/>
<pinref part="S1" gate="1" pin="S"/>
<wire x1="45.72" y1="170.18" x2="43.18" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Sensors</description>
<plain>
<text x="149.86" y="27.94" size="2.54" layer="94">Sensors</text>
<text x="149.86" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_NUMBER</text>
<text x="241.3" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_REV</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="FRAME3" gate="G$2" x="147.32" y="0"/>
<instance part="ASM7" gate="G$1" x="25.4" y="160.02"/>
<instance part="SUPPLY24" gate="G$1" x="10.16" y="149.86" rot="R90"/>
<instance part="SUPPLY25" gate="GND" x="20.32" y="147.32"/>
<instance part="ASM8" gate="G$1" x="88.9" y="160.02"/>
<instance part="SUPPLY26" gate="+5V" x="81.28" y="147.32" rot="R180"/>
<instance part="SUPPLY27" gate="GND" x="73.66" y="147.32"/>
<instance part="ASM9" gate="G$1" x="137.16" y="160.02"/>
<instance part="SUPPLY28" gate="GND" x="147.32" y="144.78"/>
<instance part="SUPPLY29" gate="+5V" x="152.4" y="149.86" rot="R180"/>
<instance part="ASM10" gate="G$1" x="205.74" y="160.02"/>
<instance part="SUPPLY30" gate="GND" x="193.04" y="144.78"/>
<instance part="SUPPLY31" gate="+5V" x="182.88" y="149.86" rot="R180"/>
<instance part="ASM11" gate="G$1" x="25.4" y="91.44"/>
<instance part="SUPPLY33" gate="GND" x="17.78" y="81.28"/>
<instance part="ASM12" gate="G$1" x="68.58" y="91.44"/>
<instance part="SUPPLY35" gate="GND" x="58.42" y="81.28"/>
<instance part="ASM13" gate="G$1" x="111.76" y="91.44"/>
<instance part="SUPPLY37" gate="GND" x="106.68" y="81.28"/>
<instance part="SUPPLY38" gate="+5V" x="10.16" y="81.28" rot="R90"/>
<instance part="SUPPLY32" gate="+5V" x="50.8" y="81.28" rot="R90"/>
<instance part="SUPPLY34" gate="+5V" x="93.98" y="81.28" rot="R90"/>
<instance part="ASM14" gate="G$1" x="157.48" y="91.44"/>
<instance part="SUPPLY36" gate="+5V" x="139.7" y="81.28" rot="R90"/>
<instance part="SUPPLY39" gate="GND" x="147.32" y="81.28"/>
<instance part="ASM15" gate="G$1" x="198.12" y="91.44"/>
<instance part="SUPPLY40" gate="GND" x="198.12" y="81.28"/>
<instance part="SUPPLY41" gate="+5V" x="185.42" y="83.82" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="+3.3V" class="0">
<segment>
<pinref part="SUPPLY24" gate="G$1" pin="+3.3V"/>
<pinref part="ASM7" gate="G$1" pin="+V"/>
<wire x1="12.7" y1="149.86" x2="15.24" y2="149.86" width="0.1524" layer="91"/>
<wire x1="15.24" y1="149.86" x2="15.24" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="ASM7" gate="G$1" pin="GND"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<wire x1="20.32" y1="154.94" x2="20.32" y2="149.86" width="0.1524" layer="91"/>
<pinref part="ASM7" gate="G$1" pin="GND2"/>
<wire x1="30.48" y1="154.94" x2="30.48" y2="149.86" width="0.1524" layer="91"/>
<wire x1="30.48" y1="149.86" x2="20.32" y2="149.86" width="0.1524" layer="91"/>
<junction x="20.32" y="149.86"/>
</segment>
<segment>
<pinref part="ASM8" gate="G$1" pin="GND"/>
<wire x1="76.2" y1="154.94" x2="76.2" y2="152.4" width="0.1524" layer="91"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="76.2" y1="152.4" x2="73.66" y2="152.4" width="0.1524" layer="91"/>
<wire x1="73.66" y1="152.4" x2="73.66" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM9" gate="G$1" pin="GND"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="147.32" y1="154.94" x2="147.32" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM10" gate="G$1" pin="GND"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="193.04" y1="154.94" x2="193.04" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM11" gate="G$1" pin="GND"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="17.78" y1="86.36" x2="17.78" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM12" gate="G$1" pin="GND"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<wire x1="58.42" y1="86.36" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM13" gate="G$1" pin="GND"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<wire x1="106.68" y1="86.36" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM14" gate="G$1" pin="GND"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<wire x1="147.32" y1="86.36" x2="147.32" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM15" gate="G$1" pin="GND"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<wire x1="198.12" y1="86.36" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="15-GPIO22" class="0">
<segment>
<pinref part="ASM7" gate="G$1" pin="SIG"/>
<wire x1="25.4" y1="154.94" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<wire x1="25.4" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<label x="40.64" y="144.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="16-GPIO23" class="0">
<segment>
<pinref part="ASM7" gate="G$1" pin="NS"/>
<wire x1="35.56" y1="154.94" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<label x="40.64" y="154.94" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="ASM8" gate="G$1" pin="VCC"/>
<pinref part="SUPPLY26" gate="+5V" pin="+5V"/>
<wire x1="81.28" y1="154.94" x2="81.28" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM9" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY29" gate="+5V" pin="+5V"/>
<wire x1="152.4" y1="154.94" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM10" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY31" gate="+5V" pin="+5V"/>
<wire x1="182.88" y1="154.94" x2="182.88" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM13" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY34" gate="+5V" pin="+5V"/>
<wire x1="96.52" y1="86.36" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM12" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY32" gate="+5V" pin="+5V"/>
<wire x1="53.34" y1="86.36" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM11" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY38" gate="+5V" pin="+5V"/>
<wire x1="12.7" y1="86.36" x2="12.7" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM14" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY36" gate="+5V" pin="+5V"/>
<wire x1="142.24" y1="86.36" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ASM15" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY41" gate="+5V" pin="+5V"/>
<wire x1="187.96" y1="86.36" x2="187.96" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="10-GPIO15" class="0">
<segment>
<pinref part="ASM8" gate="G$1" pin="TXA"/>
<wire x1="86.36" y1="154.94" x2="86.36" y2="144.78" width="0.1524" layer="91"/>
<label x="86.36" y="144.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM9" gate="G$1" pin="TX"/>
<wire x1="137.16" y1="154.94" x2="137.16" y2="144.78" width="0.1524" layer="91"/>
<label x="137.16" y="144.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="08-GPIO14" class="0">
<segment>
<pinref part="ASM8" gate="G$1" pin="RXA"/>
<wire x1="91.44" y1="154.94" x2="91.44" y2="144.78" width="0.1524" layer="91"/>
<label x="91.44" y="144.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM9" gate="G$1" pin="RX"/>
<wire x1="142.24" y1="154.94" x2="142.24" y2="144.78" width="0.1524" layer="91"/>
<label x="142.24" y="144.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="ASM9" gate="G$1" pin="EN"/>
<wire x1="121.92" y1="154.94" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<pinref part="ASM8" gate="G$1" pin="PWR_CTRL"/>
<wire x1="121.92" y1="152.4" x2="101.6" y2="152.4" width="0.1524" layer="91"/>
<wire x1="101.6" y1="152.4" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="ASM9" gate="G$1" pin="VBAT"/>
<wire x1="127" y1="154.94" x2="127" y2="149.86" width="0.1524" layer="91"/>
<pinref part="ASM8" gate="G$1" pin="V_BAT"/>
<wire x1="127" y1="149.86" x2="96.52" y2="149.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="149.86" x2="96.52" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="05-GPIO03" class="0">
<segment>
<pinref part="ASM10" gate="G$1" pin="SCL"/>
<wire x1="198.12" y1="154.94" x2="198.12" y2="144.78" width="0.1524" layer="91"/>
<label x="198.12" y="144.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM11" gate="G$1" pin="SCL"/>
<wire x1="38.1" y1="86.36" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
<label x="38.1" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM12" gate="G$1" pin="SCL"/>
<wire x1="63.5" y1="86.36" x2="63.5" y2="78.74" width="0.1524" layer="91"/>
<label x="63.5" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM13" gate="G$1" pin="SCK"/>
<wire x1="111.76" y1="86.36" x2="111.76" y2="78.74" width="0.1524" layer="91"/>
<label x="111.76" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM14" gate="G$1" pin="SCL"/>
<wire x1="167.64" y1="86.36" x2="167.64" y2="78.74" width="0.1524" layer="91"/>
<label x="167.64" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM15" gate="G$1" pin="SCL"/>
<wire x1="203.2" y1="86.36" x2="203.2" y2="78.74" width="0.1524" layer="91"/>
<label x="203.2" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="03-GPIO02" class="0">
<segment>
<pinref part="ASM10" gate="G$1" pin="SDA"/>
<wire x1="203.2" y1="154.94" x2="203.2" y2="144.78" width="0.1524" layer="91"/>
<label x="203.2" y="144.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM11" gate="G$1" pin="SDA"/>
<wire x1="33.02" y1="86.36" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<label x="33.02" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM12" gate="G$1" pin="SDA"/>
<wire x1="68.58" y1="86.36" x2="68.58" y2="78.74" width="0.1524" layer="91"/>
<label x="68.58" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ASM13" gate="G$1" pin="SDI/MOSI"/>
<wire x1="121.92" y1="86.36" x2="121.92" y2="78.74" width="0.1524" layer="91"/>
<label x="121.92" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="172.72" y1="86.36" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<label x="172.72" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="ASM14" gate="G$1" pin="SDA"/>
</segment>
<segment>
<pinref part="ASM15" gate="G$1" pin="SDA"/>
<wire x1="208.28" y1="86.36" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<label x="208.28" y="78.74" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Audio</description>
<plain>
<text x="149.86" y="27.94" size="2.54" layer="94">Audio</text>
<text x="149.86" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_NUMBER</text>
<text x="241.3" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_REV</text>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
<instance part="FRAME4" gate="G$2" x="147.32" y="0"/>
<instance part="ASM4" gate="G$1" x="116.84" y="101.6"/>
<instance part="SUPPLY18" gate="GND" x="40.64" y="101.6"/>
<instance part="SUPPLY19" gate="GND" x="116.84" y="86.36"/>
<instance part="SUPPLY20" gate="GND" x="193.04" y="101.6"/>
<instance part="SUPPLY21" gate="GND" x="193.04" y="142.24"/>
<instance part="ASM5" gate="G$1" x="210.82" y="144.78" rot="R270"/>
<instance part="ASM6" gate="G$1" x="210.82" y="104.14" rot="R270"/>
<instance part="SUPPLY22" gate="+5V" x="200.66" y="160.02"/>
<instance part="SUPPLY23" gate="+5V" x="200.66" y="119.38"/>
<instance part="R2" gate="G$1" x="68.58" y="121.92"/>
<instance part="R3" gate="G$1" x="68.58" y="111.76"/>
<instance part="R1" gate="G$1" x="53.34" y="106.68"/>
<instance part="JP2" gate="G$1" x="116.84" y="149.86"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="40.64" y1="106.68" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
<wire x1="48.26" y1="106.68" x2="40.64" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ASM4" gate="G$1" pin="SPK_GND2"/>
<wire x1="86.36" y1="106.68" x2="81.28" y2="106.68" width="0.1524" layer="91"/>
<wire x1="81.28" y1="106.68" x2="81.28" y2="101.6" width="0.1524" layer="91"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="81.28" y1="101.6" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<wire x1="81.28" y1="91.44" x2="116.84" y2="91.44" width="0.1524" layer="91"/>
<wire x1="116.84" y1="91.44" x2="116.84" y2="88.9" width="0.1524" layer="91"/>
<pinref part="ASM4" gate="G$1" pin="SPK_GND1"/>
<wire x1="86.36" y1="101.6" x2="81.28" y2="101.6" width="0.1524" layer="91"/>
<junction x="81.28" y="101.6"/>
<pinref part="ASM4" gate="G$1" pin="MIC_GND2"/>
<wire x1="147.32" y1="106.68" x2="154.94" y2="106.68" width="0.1524" layer="91"/>
<wire x1="154.94" y1="106.68" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="154.94" y1="101.6" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
<wire x1="154.94" y1="91.44" x2="116.84" y2="91.44" width="0.1524" layer="91"/>
<junction x="116.84" y="91.44"/>
<pinref part="ASM4" gate="G$1" pin="MIC_GND1"/>
<wire x1="147.32" y1="101.6" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<junction x="154.94" y="101.6"/>
</segment>
<segment>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<pinref part="ASM5" gate="G$1" pin="GND"/>
<wire x1="193.04" y1="144.78" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<pinref part="ASM6" gate="G$1" pin="GND"/>
<wire x1="193.04" y1="104.14" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="ASM4" gate="G$1" pin="SPK_L2"/>
<wire x1="73.66" y1="121.92" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
<pinref part="ASM4" gate="G$1" pin="SPK_L1"/>
<wire x1="78.74" y1="121.92" x2="86.36" y2="121.92" width="0.1524" layer="91"/>
<wire x1="86.36" y1="116.84" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
<wire x1="78.74" y1="116.84" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
<junction x="78.74" y="121.92"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="ASM4" gate="G$1" pin="SPK_R"/>
<wire x1="86.36" y1="111.76" x2="73.66" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="AUDIO+" class="0">
<segment>
<wire x1="48.26" y1="132.08" x2="63.5" y2="132.08" width="0.1524" layer="91"/>
<wire x1="63.5" y1="106.68" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<wire x1="63.5" y1="111.76" x2="63.5" y2="121.92" width="0.1524" layer="91"/>
<wire x1="63.5" y1="121.92" x2="63.5" y2="132.08" width="0.1524" layer="91"/>
<wire x1="58.42" y1="106.68" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<label x="48.26" y="132.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<junction x="63.5" y="121.92"/>
<pinref part="R3" gate="G$1" pin="1"/>
<junction x="63.5" y="111.76"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="SUPPLY22" gate="+5V" pin="+5V"/>
<wire x1="200.66" y1="157.48" x2="200.66" y2="154.94" width="0.1524" layer="91"/>
<pinref part="ASM5" gate="G$1" pin="VIN"/>
<wire x1="200.66" y1="154.94" x2="205.74" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY23" gate="+5V" pin="+5V"/>
<wire x1="200.66" y1="116.84" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
<pinref part="ASM6" gate="G$1" pin="VIN"/>
<wire x1="200.66" y1="114.3" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="ASM6" gate="G$1" pin="DC"/>
<wire x1="205.74" y1="93.98" x2="167.64" y2="93.98" width="0.1524" layer="91"/>
<wire x1="167.64" y1="93.98" x2="167.64" y2="111.76" width="0.1524" layer="91"/>
<pinref part="ASM4" gate="G$1" pin="MIC_R"/>
<wire x1="147.32" y1="111.76" x2="167.64" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="ASM4" gate="G$1" pin="MIC_L2"/>
<wire x1="147.32" y1="121.92" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="167.64" y1="121.92" x2="167.64" y2="134.62" width="0.1524" layer="91"/>
<wire x1="167.64" y1="134.62" x2="205.74" y2="134.62" width="0.1524" layer="91"/>
<pinref part="ASM5" gate="G$1" pin="DC"/>
<pinref part="ASM4" gate="G$1" pin="MIC_L1"/>
<wire x1="147.32" y1="116.84" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
<wire x1="167.64" y1="116.84" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<junction x="167.64" y="121.92"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="VCC"/>
<pinref part="ASM4" gate="G$1" pin="5V"/>
<wire x1="109.22" y1="149.86" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="D-"/>
<pinref part="ASM4" gate="G$1" pin="D-"/>
<wire x1="114.3" y1="149.86" x2="114.3" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="D+"/>
<pinref part="ASM4" gate="G$1" pin="D+"/>
<wire x1="119.38" y1="149.86" x2="119.38" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="GND"/>
<pinref part="ASM4" gate="G$1" pin="GND"/>
<wire x1="124.46" y1="149.86" x2="124.46" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Head Unit Connections</description>
<plain>
<text x="149.86" y="27.94" size="2.54" layer="94">Head Connections</text>
<text x="149.86" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_NUMBER</text>
<text x="241.3" y="7.62" size="2.54" layer="94">&gt;DOCUMENT_REV</text>
</plain>
<instances>
<instance part="FRAME5" gate="G$1" x="0" y="0"/>
<instance part="FRAME5" gate="G$2" x="147.32" y="0"/>
<instance part="JP1" gate="G$1" x="71.12" y="66.04" rot="R90"/>
<instance part="SUPPLY17" gate="GND" x="81.28" y="63.5"/>
<instance part="SUPPLY42" gate="+5V" x="83.82" y="73.66" rot="R270"/>
<instance part="SUPPLY43" gate="GND" x="104.14" y="147.32"/>
<instance part="U$1" gate="G$1" x="154.94" y="111.76"/>
</instances>
<busses>
</busses>
<nets>
<net name="AUDIO+" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="SPK+"/>
<wire x1="73.66" y1="144.78" x2="81.28" y2="144.78" width="0.1524" layer="91"/>
<label x="81.28" y="144.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="33-GPIO13" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="NEOPIXELS"/>
<wire x1="73.66" y1="139.7" x2="81.28" y2="139.7" width="0.1524" layer="91"/>
<label x="81.28" y="139.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="38-GPIO20" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="SW3"/>
<wire x1="73.66" y1="134.62" x2="81.28" y2="134.62" width="0.1524" layer="91"/>
<label x="81.28" y="134.62" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="37-GPIO26" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="SW2"/>
<wire x1="73.66" y1="129.54" x2="81.28" y2="129.54" width="0.1524" layer="91"/>
<label x="81.28" y="129.54" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="36-GPIO16" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="SW1"/>
<wire x1="73.66" y1="124.46" x2="81.28" y2="124.46" width="0.1524" layer="91"/>
<label x="81.28" y="124.46" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="35-GPIO19" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="HEAD_SW"/>
<wire x1="73.66" y1="119.38" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<label x="81.28" y="119.38" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="26-GPIO07" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_SD_CS"/>
<wire x1="73.66" y1="114.3" x2="81.28" y2="114.3" width="0.1524" layer="91"/>
<label x="81.28" y="114.3" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="24-GPIO08" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_LCD_CS"/>
<wire x1="73.66" y1="109.22" x2="81.28" y2="109.22" width="0.1524" layer="91"/>
<label x="81.28" y="109.22" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="23-GPIO11" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_SCK"/>
<wire x1="73.66" y1="104.14" x2="81.28" y2="104.14" width="0.1524" layer="91"/>
<label x="81.28" y="104.14" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="22-GPIO25" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_RESET"/>
<wire x1="73.66" y1="99.06" x2="81.28" y2="99.06" width="0.1524" layer="91"/>
<label x="81.28" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="21-GPIO09" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_MISO"/>
<wire x1="73.66" y1="93.98" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
<label x="81.28" y="93.98" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="19-GPIO10" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_MOSI"/>
<wire x1="73.66" y1="88.9" x2="81.28" y2="88.9" width="0.1524" layer="91"/>
<label x="81.28" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="18-GPIO24" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_DC"/>
<wire x1="73.66" y1="83.82" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<label x="81.28" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="12-GPIO18" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="DISP_BACKLIGHT"/>
<wire x1="73.66" y1="78.74" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<label x="81.28" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="5V"/>
<pinref part="SUPPLY42" gate="+5V" pin="+5V"/>
<wire x1="73.66" y1="73.66" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="GND"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="73.66" y1="68.58" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<wire x1="81.28" y1="68.58" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="SPK-"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="73.66" y1="149.86" x2="104.14" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
