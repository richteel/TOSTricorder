#!/bin/bash

# --- Tricorder Services ---
###########################################################################
P_TITLE="Tricorder Service"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/bin/tricorder.sh ] || [ ! -f /lib/systemd/system/tricorder.service ] || [ "$P_UPDATE_ALL" == "y" ] || [ "$P_UPDATE_SERVICES" == "y" ]; then
    echo
    echo "------------------------------------------------------"
    echo "${P_COUNTER} - Installing ${P_TITLE}"
    echo "------------------------------------------------------"

    cp tricorder.sh /usr/bin
    cp tricorder.service /lib/systemd/system
    systemctl enable /lib/systemd/system/tricorder.service

    echo
    echo "Done"
    CHANGEMADE="Yes"
else
    echo
    echo "${P_COUNTER} - ${P_TITLE} already installed"
fi


###########################################################################
P_TITLE="Tricorder-Buttons Service"
P_COUNTER=$((P_COUNTER+1))
###########################################################################
if [ ! -f /usr/bin/tricorder-buttons.sh ] || [ ! -f /lib/systemd/system/tricorder-buttons.service ] || [ "$P_UPDATE_ALL" == "y" ] || [ "$P_UPDATE_SERVICES" == "y" ]; then
    echo
    echo "------------------------------------------------------"
    echo "${P_COUNTER} - Installing ${P_TITLE}"
    echo "------------------------------------------------------"

    cp tricorder-buttons.sh /usr/bin
    cp tricorder-buttons.service /lib/systemd/system
    systemctl enable /lib/systemd/system/tricorder-buttons.service

    echo
    echo "Done"
    CHANGEMADE="Yes"
else
    echo
    echo "${P_COUNTER} - ${P_TITLE} already installed"
fi